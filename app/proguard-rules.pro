# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/vivek.varun/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

#-dontobfuscate
-ignorewarnings
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.FieldNamingStrategy { *; }

#Facebook
-keep class com.facebook.** { *; }
#-keep class com.facebook.sdk.ApplicationId


#ad
#-keep public class com.google.android.gms.ads.** {
#   public protected *;
#}

-keep public class com.google.ads.** {
  public protected *;
}

-keep class rx.internal.util.unsafe.** { *; }

#play services
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient

#gcm
#-keep public class * extends com.google.android.gcm.*

#apsalar
-keep class com.apsalar.sdk.** { *; }

#Keep classes that are referenced on the AndroidManifest
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.preference.Preference
-keep public class * extends com.library.basemodels.BusinessObject
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}
-keepclassmembers class com.toi.reader.model.* {
        private <fields>;
}
#support libs(cab be used more wisely
-keep class android.support.v4.** { *; }
-keep class android.support.v7.** { *; }
-keep public class * extends android.support.v4.**
-keep public class * extends android.support.v7.**
-keep class android.support.design.widget.** { *; }

-keep interface android.support.design.widget.** { *; }
-dontwarn android.support.design.**
#jsoup
-keep class org.jsoup.**{ *; }

#Faragments
-keep public class * extends android.app.Fragment

#Colombia Change
-keep class com.til.colombia.** { *; }
#Colombia Change END
-keep class com.google.android.gms.ads.** { *; }
-keep class com.til.colombia.dmp.android.**{*;}

-keep class com.google.ads.interactivemedia.** { *; }
-keep interface com.google.ads.interactivemedia.** { *; }

## Required even for GCM only apps
-dontwarn com.amazon.device.messaging.**

## Required for the Javascript Interface
-keepclassmembers class com.urbanairship.js.UAJavascriptInterface {
   public *;
}

## Autopilot
-keep public class * extends com.urbanairship.Autopilot

#lotame
-keep class com.lotame.**{*; }
-dontwarn com.lotame.**
-keep class org.json.**{*; }

#UrbanAirship Changes START
-keep class com.urbanairship.** {*; }
-keepnames class * implements android.os.Parcelable {
  public static final ** CREATOR;
}
-dontwarn com.amazon.device.messaging.**
-dontwarn com.urbanairship.**
#UrbanAirship Changes END

#Twitter start rule
-dontwarn com.squareup.okhttp.**
-dontwarn com.google.appengine.api.urlfetch.**
-dontwarn rx.**
-dontwarn retrofit.**
-keepattributes Signature
-keepattributes *Annotation*
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}
#Twitter End rule

#google play services
-dontwarn com.google.android.gms.**

#Progurad rule for Comscore
-keep class com.comscore.** { *; }
-dontwarn com.comscore.**

-dontwarn org.apache.commons.**
-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**

-dontwarn okio.**
-dontwarn retrofit2.Platform$Java8

#Applications all class for saftey
-keep class com.et.**{ *; }
-keep class com.et.reader.**{ *; }
-keep class com.et.widget.**{ *; }
-keep class com.ext.services.**{*; }
-keep class org.json.**{*; }

-keep class com.library.basemodels.*
-keep class com.sso.library.** { *; }

-keepnames class com.library.basemodels.*
-keepnames class com.sso.library.*
-keepnames class com.et.reader.models.*

-dontwarn net.hockeyapp.android.**
-dontwarn com.sso.library.**
-keep class com.sso.library.** { *; }
-keep class net.hockeyapp.android.** { *; }

-keepclassmembers class com.sso.library.models.User {
static final long serialVersionUID;
}

-keepattributes SourceFile,LineNumberTable
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**




 -keep public class * implements com.bumptech.glide.module.GlideModule
 -keep public class * extends com.bumptech.glide.module.AppGlideModule
 -keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
   **[] $VALUES;
   public *;
 }

 -keep class com.podcastlib.**{ *; }

 # Retrofit
 -keep class com.google.gson.** { *; }
 -keep public class com.google.gson.** {public private protected *;}
 -keep class com.google.inject.** { *; }
 -keep class org.apache.http.** { *; }
 -keep class org.apache.james.mime4j.** { *; }
 -keep class javax.inject.** { *; }
 -keep class javax.xml.stream.** { *; }
 -keep class retrofit.** { *; }
 -keep class com.google.appengine.** { *; }
 -keepattributes *Annotation*
 -keepattributes Signature
 -dontwarn com.squareup.okhttp.*
 -dontwarn rx.**
 -dontwarn javax.xml.stream.**
 -dontwarn com.google.appengine.**
 -dontwarn java.nio.file.**
 -dontwarn org.codehaus.**



 -dontwarn retrofit2.**
 -dontwarn org.codehaus.mojo.**
 -keep class retrofit2.** { *; }
 -keepattributes Exceptions
 -keepattributes RuntimeVisibleAnnotations
 -keepattributes RuntimeInvisibleAnnotations
 -keepattributes RuntimeVisibleParameterAnnotations
 -keepattributes RuntimeInvisibleParameterAnnotations

 -keepattributes EnclosingMethod
 -keepclasseswithmembers class * {
     @retrofit2.http.* <methods>;
 }

 -keepclasseswithmembers class * {
      @retrofit2.https.* <methods>;
  }
 -keepclasseswithmembers interface * {
     @retrofit2.* <methods>;
 }
 # Platform calls Class.forName on types which do not exist on Android to determine platform.
 -dontnote retrofit2.Platform
 # Platform used when running on RoboVM on iOS. Will not be used at runtime.
 -dontnote retrofit2.Platform$IOS$MainThreadExecutor
 # Platform used when running on Java 8 VMs. Will not be used at runtime.
 -dontwarn retrofit2.Platform$Java8
 # Retain generic type information for use by reflection by converters and adapters.
 -keepattributes Signature
 # Retain declared checked exceptions for use by a Proxy instance.
 -keepattributes Exceptions

# Add any classes the interact with gson
# the following line is for illustration purposes
-keep class fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.** { *; }


#App See
-keep class com.appsee.** { *; }
-dontwarn com.appsee.**
-keep class android.support.** { *; }
-keep interface android.support.** { *; }
-keep class androidx.** { *; }
-keep interface androidx.** { *; }
-keepattributes SourceFile,LineNumberTable

#UXCam
-keep class com.uxcam.** { *; }
-dontwarn com.uxcam.**

