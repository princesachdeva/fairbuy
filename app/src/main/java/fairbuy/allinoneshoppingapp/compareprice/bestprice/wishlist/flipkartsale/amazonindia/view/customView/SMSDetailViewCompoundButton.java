package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;


public class SMSDetailViewCompoundButton extends LinearLayout {

    private Context mContext = null;
    private LayoutInflater mInflater = null;
    private TextView nseTag = null;
    private TextView bseTag = null;
    private View mViewReference = null;
    private LinearLayout parentPanel = null;
    private View view = null;

    private int selectedIndex = 0;

    public SMSDetailViewCompoundButton(Context context) {
        this(context, null);
    }

    public SMSDetailViewCompoundButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        init();
    }

    private void init() {
        mViewReference = mInflater.inflate(R.layout.view_ll_container, this, true);
        parentPanel = (LinearLayout) mViewReference.findViewById(R.id.parentPanel);

        view = mInflater.inflate(R.layout.view_cb_item, null, false);
        nseTag = (TextView) view.findViewById(R.id.tagButton);
        nseTag.setBackgroundResource(R.drawable.selector_cb_bg);
        nseTag.setSelected(true);
        nseTag.setText("SMS");
        nseTag.setTag(0);
        //nseTag.setId(R.id.nseTag);


        view = mInflater.inflate(R.layout.view_cb_item, null, false);
        bseTag = (TextView) view.findViewById(R.id.tagButton);
        LinearLayout.LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(-1, 0, 0, 0);
        bseTag.setLayoutParams(layoutParams);
        bseTag.setBackgroundResource(R.drawable.selector_cb_bg);
        bseTag.setSelected(false);
        bseTag.setText("DETAIL VIEW");
        bseTag.setTag(1);
        //nseTag.setId(R.id.bseTag);

        parentPanel.addView(nseTag);
        parentPanel.addView(bseTag);


       /* nseTag.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedIndex = 0;
                changeSelection();
            }
        });
        bseTag.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedIndex = 1;
                changeSelection();
            }
        });*/
    }


}