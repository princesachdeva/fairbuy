package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper;

/**
 * Created by aditya.amartya on 07/12/18
 */
public interface Constants {
    String HOME = "home";
    String WISHLIST = "wishlist";
    String MY_ORDERS = "my_orders";
    String SEARCH = "search";
    String RECENTLY_VIEWED = "my_orders";
    int MAX_VIEW_ALLOWED_COUNT = 1;
}
