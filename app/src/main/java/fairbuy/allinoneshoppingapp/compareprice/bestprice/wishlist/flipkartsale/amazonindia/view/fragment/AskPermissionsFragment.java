package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.accessibility.AccessibilityManager;
import android.widget.Toast;

import com.crashlytics.android.answers.FirebaseAnalyticsEvent;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceKeys;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.FairBuyActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.listeners.OnViewAllClickListener;

/**
 * Created by prince.sachdeva on 28/12/18.
 */

public class AskPermissionsFragment extends DialogFragment implements View.OnClickListener {
    private static final int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 101;
    ViewDataBinding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.ask_permission_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAccessibilityPermissionChangeListener();
        binding.setVariable(BR.clickListener, this);
        PreferenceManager.writeLOngToPreference(PreferenceKeys.LAST_TIME_ASK_PERMISSION_SHOWN, System.currentTimeMillis());
        FirebaseAnalyticsManager.getInstance().setScreen(getActivity(), "Permission");
    }

    private void updateUI() {
        binding.setVariable(BR.isAccessibilityPermissionGranted, ShoppingUtils.isAccessibilitySettingsOn(getContext().getApplicationContext()));
        binding.setVariable(BR.isDrawOverOtherAppsPermissionGranted, Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getContext()));
        binding.executePendingBindings();
    }

    private void askAccessibilityPermission() {
        boolean isAccessibilityServiceOn = ShoppingUtils.isAccessibilitySettingsOn(getContext());
        if (!isAccessibilityServiceOn) {
            startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
        } else {
            drawOverOtherAppPermission();
        }
    }

    private void setAccessibilityPermissionChangeListener() {
        AccessibilityManager accessibilityManager = (AccessibilityManager) getContext().getSystemService(Context.ACCESSIBILITY_SERVICE);
        accessibilityManager.addAccessibilityStateChangeListener(new AccessibilityManager.AccessibilityStateChangeListener() {
            @Override
            public void onAccessibilityStateChanged(boolean enabled) {
                if (enabled && isAdded()) {
                    drawOverOtherAppPermission();
                }
            }
        });
    }

    AccessibilityManager.AccessibilityStateChangeListener mAccessibilityStateChangeListener = new AccessibilityManager.AccessibilityStateChangeListener() {
        @Override
        public void onAccessibilityStateChanged(boolean enabled) {
            if (enabled) {
                drawOverOtherAppPermission();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
        if (FairBuyActivity.isMandatoryPermissionsGranted()) {
            showHome();
        }
    }

    private void drawOverOtherAppPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(ShoppingAssistantApplication.getContext())) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + ShoppingAssistantApplication.getContext().getPackageName()));
            startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
        } else if (FairBuyActivity.isMandatoryPermissionsGranted()) {
            showHome();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE) {
            /*if (FairBuyActivity.isMandatoryPermissionsGranted()) {
                showHome();
            }*/
        }
    }


    private void showHome() {
        Bundle bundle = new Bundle();
        bundle.putInt("status", 2);

        FirebaseAnalyticsManager.getInstance().sendEvent(AnalyticsConstants.FirebaseEvents.MANDATORY_PERMISSIONS, bundle);
        dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibDismiss:
                dismiss();
                break;
            default:
                askAccessibilityPermission();
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
            InsetDrawable inset = new InsetDrawable(back, 40);

            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(inset);
        }
    }
}
