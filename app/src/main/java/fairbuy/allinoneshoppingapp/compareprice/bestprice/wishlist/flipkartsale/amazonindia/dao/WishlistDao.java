package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface WishlistDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(WishlistItem wishlist);

    @Query("Select * from wish_list Order by last_modified_time desc LIMIT 25")
    LiveData<List<WishlistItem>> getWishList();

    @Query("Select * from wish_list Order by last_modified_time desc LIMIT 5")
    LiveData<List<WishlistItem>> getWishListHome();

    @Query("SELECT COUNT(product_url) FROM wish_list WHERE product_url = :productUrl")
    int isWishListedUiThread(String productUrl);

    @Query("SELECT COUNT(product_url) FROM wish_list WHERE product_url = :productUrl")
    LiveData<Integer> isWishListed(String productUrl);

    @Delete
    void deleteWishListItem(WishlistItem wishlistItem);

    @Query("DELETE from wish_list WHERE product_url = :productUrl")
    void deleteByProductUrl(String productUrl);

    @Query("SELECT COUNT(*) FROM wish_list")
    LiveData<Integer> getCount();

}
