package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.SearchHistory;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.GoogleSearch;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.ApiCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.ShoppingRepository;

import java.util.List;

/**
 * Created by aditya.amartya on 28/09/18
 */
public class ShoppingViewModel extends BaseViewModel {
    private ShoppingRepository shoppingRepository;
    private MutableLiveData<GoogleSearch> mutableLiveData;

    public ShoppingViewModel(@NonNull Application application) {
        super(application);
        shoppingRepository = new ShoppingRepository(application);
    }

    public LiveData<List<SearchHistory>> getLiveData(int count) {
        return shoppingRepository.getShoppingHistory(count);
    }

    public void insertItem(SearchHistory searchHistory) {
        shoppingRepository.insertItem(searchHistory);
    }

    public void getSearchResult(String url) {
        if (mutableLiveData == null) {
            mutableLiveData = new MutableLiveData<>();
        }
        shoppingRepository.getSearchResults(url, new ApiCallBack<GoogleSearch>() {
            @Override
            public void onSuccess(GoogleSearch data) {
                mutableLiveData.setValue(data);
            }

            @Override
            public void onFail(Throwable throwable) {
                mutableLiveData.setValue(null);
            }
        });
    }

    public MutableLiveData<GoogleSearch> getSearchLiveData() {
        return mutableLiveData;
    }
}
