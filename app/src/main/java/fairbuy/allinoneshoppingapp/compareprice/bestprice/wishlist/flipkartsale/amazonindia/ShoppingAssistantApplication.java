package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.lrucache.LRUCache;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.worker.AccessibilityCheckJob;


/**
 * Created by prince.sachdeva on 04/10/18.
 */

public class ShoppingAssistantApplication extends Application {

    public static String channaleID = "";
    public static String channale_name = "Fairbuy";
    private static Application instance;
    private LRUCache<String, Product> mLRUCache;
    private LRUCache<String, ArrayList<Product>> mLRUCacheProducts;
    public static boolean showAskPermissionDialog;

    public JSONObject getBestSellersData() {
        return bestSellersData;
    }

    private JSONObject bestSellersData;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        // Load Best Sellers Data From Assets
        loadBestSellersData();

        //AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);


        registerChannelID();

        initLRUCache();
        JobManager.create(this).addJobCreator(new DemoJobCreator());
        schedulePeriodicJob();
    }

    private void initLRUCache() {
        mLRUCache = new LRUCache<>(30);
        mLRUCacheProducts = new LRUCache<>(20);
    }

    public void insertInCache(String url, Product product) {
        mLRUCache.set(url, product);
    }

    public Product getDataFromCache(String url) {
        return mLRUCache.get(url);
    }

    public void insertInCache(String searchURL, ArrayList<Product> products) {
        mLRUCacheProducts.set(searchURL, products);
    }

    public ArrayList<Product> getProductsListDataFromCache(String searchURL) {
        ArrayList<Product> products = mLRUCacheProducts.get(searchURL);
        if (products == null) {
            products = new ArrayList<Product>();
        }
        return products;
    }

    private void registerChannelID() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channaleID = getString(R.string.channel_id);
            NotificationChannel channel = new NotificationChannel(channaleID, channale_name, importance);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static Context getContext() {
        return instance != null ? instance.getApplicationContext() : null;
    }

    public static Application getInstance() {
        return instance;
    }

    public class DemoJobCreator implements JobCreator {
        @Nullable
        @Override
        public Job create(@NonNull String tag) {
            switch (tag) {
                case AccessibilityCheckJob.TAG:
                    return new AccessibilityCheckJob();
                default:
                    return null;
            }
        }
    }

    private void schedulePeriodicJob() {
        Set<JobRequest> jobRequests = JobManager.create(this).getAllJobRequestsForTag(AccessibilityCheckJob.TAG);
        if (jobRequests != null && jobRequests.size() == 0) {
            int jobId = new JobRequest.Builder(AccessibilityCheckJob.TAG)
                    .setPeriodic(TimeUnit.HOURS.toMillis(6), TimeUnit.MINUTES.toMillis(5))
                    .setUpdateCurrent(true)
                    .build()
                    .schedule();
        }
    }

    private void loadBestSellersData() {
        String assetJSONFile = getJSONDataFromAssets("bestseller/data.json");
        if (!TextUtils.isEmpty(assetJSONFile)) {
            try {
                bestSellersData = new JSONObject(assetJSONFile);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getJSONDataFromAssets(String filename) {
        AssetManager manager = getContext().getAssets();
        try {
            InputStream file = manager.open(filename);
            byte[] formArray = new byte[file.available()];

            file.read(formArray);
            file.close();
            return new String(formArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
