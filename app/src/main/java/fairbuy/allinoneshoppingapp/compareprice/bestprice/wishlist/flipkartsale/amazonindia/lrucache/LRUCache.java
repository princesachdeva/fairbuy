package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.lrucache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LRUCache<Key,Value> {

    int capacity;
    HashMap<Key, Node<Key,Value>> map = new HashMap<>();

    Node<Key,Value> head;
    Node<Key,Value> end;

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    public Value get(Key key) {
        if(map.containsKey(key)){
            Node<Key,Value> n = map.get(key);
            remove(n);
            setHead(n);
            return n.value;
        }

        return null;
    }

    public void set(Key key, Value value) {
        if(map.containsKey(key)){
            Node old = map.get(key);
            old.value = value;
            remove(old);
            setHead(old);
        }else{
            Node created = new Node(key, value);
            if(map.size()>=capacity){
                map.remove(end.key);
                remove(end);
                setHead(created);

            }else{
                setHead(created);
            }

            map.put(key, created);
        }
    }

    public boolean contains(Key msid) {
        return map.containsKey(msid);
    }

    private void remove(Node n){
        if(n.pre!=null){
            n.pre.next = n.next;
        }else{
            head = n.next;
        }

        if(n.next!=null){
            n.next.pre = n.pre;
        }else{
            end = n.pre;
        }

    }

    private void setHead(Node n){
        n.next = head;
        n.pre = null;

        if(head!=null)
            head.pre = n;

        head = n;

        if(end ==null)
            end = head;
    }


    public ArrayList<Value> getNodeValueList() {
        ArrayList<Value> list = new ArrayList<>();
        for (Map.Entry<Key, Node<Key,Value>> entry : map.entrySet()){
            if (entry.getValue().value != null){
                list.add(entry.getValue().value);
            }
        }
        return list;
    }


}
