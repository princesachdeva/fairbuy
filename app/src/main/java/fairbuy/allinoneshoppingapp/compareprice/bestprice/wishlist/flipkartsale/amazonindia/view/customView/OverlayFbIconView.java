package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView;

import android.app.Application;
import android.view.View;

public class OverlayFbIconView extends BaseOverlayView {
    public OverlayFbIconView(Application application, int layoutId) {
        super(application, layoutId);
    }

    public View getOverlayIconView() {
        return myLayout;
    }
}
