package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by aditya.amartya on 21/11/18
 */

@Dao
public interface SmsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SmsProductData smsData);

    @Query("SELECT * FROM sms_data Order by last_modified_time")
    LiveData<List<SmsProductData>> getSmsData();

    @Query("SELECT * FROM sms_data Order by last_modified_time LIMIT :count")
    LiveData<List<SmsProductData>> getSmsData(int count);

    @Query("SELECT * FROM sms_data WHERE product_name = :productName  AND source =  :source")
    LiveData<SmsProductData> getDataByProductNameAndSource(String productName, String source);

    @Query("SELECT * FROM sms_data WHERE source = :source")
    LiveData<List<SmsProductData>> getSmsDataBySource(int source);

    @Query("SELECT COUNT(*) FROM sms_data")
    LiveData<Integer> getCount();
}
