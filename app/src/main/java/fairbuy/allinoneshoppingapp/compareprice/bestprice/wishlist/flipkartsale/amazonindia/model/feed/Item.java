package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by aditya.amartya on 24/10/18
 */

public class Item extends BaseModel implements Serializable {

    @SerializedName("kind")
    private String kind;

    @SerializedName("title")
    private String title;

    @SerializedName("htmlTitle")
    private String htmlTitle;

    @SerializedName("link")
    private String link;

    @SerializedName("displayLink")
    private String displayLink;

    @SerializedName("snippet")
    private String snippet;
    @SerializedName("htmlSnippet")
    private String htmlSnippet;

    @SerializedName("cacheId")
    private String cacheId;

    @SerializedName("formattedUrl")
    private String formattedUrl;

    @SerializedName("htmlFormattedUrl")
    private String htmlFormattedUrl;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHtmlTitle() {
        return htmlTitle;
    }

    public void setHtmlTitle(String htmlTitle) {
        this.htmlTitle = htmlTitle;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDisplayLink() {
        return displayLink;
    }

    public void setDisplayLink(String displayLink) {
        this.displayLink = displayLink;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getHtmlSnippet() {
        return htmlSnippet;
    }

    public void setHtmlSnippet(String htmlSnippet) {
        this.htmlSnippet = htmlSnippet;
    }

    public String getCacheId() {
        return cacheId;
    }

    public void setCacheId(String cacheId) {
        this.cacheId = cacheId;
    }

    public String getFormattedUrl() {
        return formattedUrl;
    }

    public void setFormattedUrl(String formattedUrl) {
        this.formattedUrl = formattedUrl;
    }

    public String getHtmlFormattedUrl() {
        return htmlFormattedUrl;
    }

    public void setHtmlFormattedUrl(String htmlFormattedUrl) {
        this.htmlFormattedUrl = htmlFormattedUrl;
    }

}
