package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.converter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.Status;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by prince.sachdeva on 22/11/18.
 */

public class StatusConverter {

    @TypeConverter
    public static ArrayList<Status> fromString(String value) {
        Type listType = new TypeToken<ArrayList<Status>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(ArrayList<Status> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }
}
