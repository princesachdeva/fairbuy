package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;

import com.appsee.Appsee;
import com.uxcam.UXCam;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.LinkingManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseActivity;

/**
 * Created by prince.sachdeva on 12/02/19.
 */

public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*try {
            //VideoView videoHolder = new VideoView(this);
            //setContentView(videoHolder);
            final ActivitySplashVideoBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_video);
            binding.setVariable(BR.clickListener, onClickListener);
            Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash);
            binding.videoHolder.setVideoURI(video);

            binding.videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    binding.setVariable(BR.isVideoFinished, true);
                    //jump();
                }
            });
            binding.videoHolder.start();
        } catch (Exception ex) {
            jump();
        }*/
        UXCam.startWithKey("3lsytrcdulb9ul2");
        Appsee.start();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new LinkingManager().handleDeepLinkingSupport(SplashActivity.this, getIntent());
                SplashActivity.this.finish();
            }
        }, 2000);
        ShoppingAssistantApplication.showAskPermissionDialog = false;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            jump();
        }
    };

    private void jump() {
        if (isFinishing()) {
            return;
        }
        startActivity(new Intent(this, FairBuyActivity.class));
        finish();
    }
}
