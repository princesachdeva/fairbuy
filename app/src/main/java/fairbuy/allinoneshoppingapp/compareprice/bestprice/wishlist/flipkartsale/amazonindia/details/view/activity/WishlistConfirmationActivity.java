package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistItem;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ActivityWishlistConfirmationBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.adapter.WishlistConfirmationAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.DbCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.WishListRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;

public class WishlistConfirmationActivity extends Activity {
    private ActivityWishlistConfirmationBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), android.R.color.transparent, null));
        /*WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = Utils.getScreenHeight() / 2;
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);*/
        binding = DataBindingUtil.setContentView(this, R.layout.activity_wishlist_confirmation);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPx(375, this));
        window.setGravity(Gravity.BOTTOM);
        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        if (intent != null) {
            ArrayList<Product> products = intent.getParcelableArrayListExtra(Args.PRODUCTS);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            binding.rvWishListItems.setLayoutManager(layoutManager);
            binding.rvWishListItems.setAdapter(new WishlistConfirmationAdapter(products, onClickListener));
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            switch (view.getId()) {
                case R.id.cardWishlistConfirmation:
                    ShoppingUtils.launchProductPage(view.getContext(), view.getTag().toString());
                    WishlistConfirmationActivity.this.finish();
                    break;
                case R.id.ivWishlist:
                    Product product = (Product) view.getTag(R.string.product_tag);
                    final int position = (int) view.getTag(R.string.position_tag);
                    if (product.isWishlisted) {
                        WishListRepository.getInstance(WishlistConfirmationActivity.this.getApplication()).deleteWishListItem(product.productURL, new DbCallBack() {
                            @Override
                            public void onQueryProcessed(final boolean isSuccess) {
                                view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isSuccess) {
                                            if (binding != null && binding.rvWishListItems.getAdapter() != null) {
                                                ((WishlistConfirmationAdapter) binding.rvWishListItems.getAdapter()).updateProductWishlistState(position, false);
                                            }
                                        }
                                    }
                                });
                            }
                        });

                    } else {
                        WishlistItem wishlistItem = new WishlistItem();
                        wishlistItem.setItem(product.productName);
                        wishlistItem.setImageUrl(product.productImage);
                        wishlistItem.setProductUrl(product.productURL);
                        wishlistItem.setShoppingSite(product.webSiteName);
                        wishlistItem.setLastModifiedTime(System.currentTimeMillis());
                        WishListRepository.getInstance(WishlistConfirmationActivity.this.getApplication()).insertWishListItem(wishlistItem, new DbCallBack() {
                            @Override
                            public void onQueryProcessed(final boolean isSuccess) {
                                view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isSuccess) {
                                            if (binding != null && binding.rvWishListItems.getAdapter() != null) {
                                                ((WishlistConfirmationAdapter) binding.rvWishListItems.getAdapter()).updateProductWishlistState(position, true);
                                                Toast.makeText(WishlistConfirmationActivity.this, "You have successfully wishlisted this item.", Toast.LENGTH_SHORT).show();
                                                WishlistConfirmationActivity.this.finish();
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                    break;
            }
        }
    };

    public interface Args {
        String PRODUCTS = "products";
    }
}
