package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import android.Manifest;
import android.app.Application;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.MyOrdersAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentMyOrdersBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.SMSParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.viewmodel.SmsViewModel;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceKeys;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;


public class MyOrdersFragment extends BaseFragment implements View.OnClickListener {
    private MyOrdersAdapter adapter;
    private RequestPermissionAction onPermissionCallBack;
    private final static int REQUEST_READ_SMS_PERMISSION = 3004;
    private SmsViewModel smsViewModel;

    @Override
    protected int getResourceId() {
        return R.layout.fragment_my_orders;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());

        ((FragmentMyOrdersBinding) binding).recylerViewMyOrders.setLayoutManager(mLayoutManager);
        ((FragmentMyOrdersBinding) binding).recylerViewMyOrders.setItemAnimator(new DefaultItemAnimator());

        adapter = new MyOrdersAdapter();
        adapter.setAskPermissionClickListener(this);
        ((FragmentMyOrdersBinding) binding).recylerViewMyOrders.setAdapter(adapter);

        smsViewModel = ViewModelProviders.of(this).get(SmsViewModel.class);
        smsViewModel.getLiveData().observe(this, new Observer<List<SmsProductData>>() {
            @Override
            public void onChanged(@Nullable List<SmsProductData> smsProductData) {
                if (smsProductData != null && smsProductData.size() > 0) {
                    adapter.setViewType(MyOrdersAdapter.VIEW_TYPE_DISPLAY_DATA);
                    adapter.updateData(smsProductData);
                }
            }
        });

        if (!checkReadSMSPermission()) {
            adapter.setViewType(MyOrdersAdapter.VIEW_TYPE_ASK_PERMISSION);
            adapter.notifyDataSetChanged();
        } else {
            handleEmptyViewCase();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (checkReadSMSPermission()) {
            getPreviousMessages();
        }
    }

    private void askSMSPermission() {
        if (PreferenceManager.get(PreferenceKeys.IS_SMS_PERMISSION_NEVER_ASK_CLICKED, false)) {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);
        } else {
            getReadSMSPermission(new RequestPermissionAction() {
                @Override
                public void permissionDenied() {
                    Toast.makeText(getContext(), "Denied Permission", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void permissionGranted() {
                    getPreviousMessages();
                }
            });
        }
    }


    public void getReadSMSPermission(RequestPermissionAction onPermissionCallBack) {
        this.onPermissionCallBack = onPermissionCallBack;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkReadSMSPermission()) {
                requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS}, REQUEST_READ_SMS_PERMISSION);
                return;
            } else {
                onPermissionCallBack.permissionGranted();
            }
        } else {
            onPermissionCallBack.permissionGranted();
        }
    }

    private boolean checkReadSMSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {
        askSMSPermission();
    }

    public interface RequestPermissionAction {
        void permissionDenied();

        void permissionGranted();

    }

    public void getPreviousMessages() {

        if (!PreferenceManager.get(PreferenceKeys.IS_PREVIOUS_MESSAGE_PARSED, false)) {

            adapter.setViewType(MyOrdersAdapter.VIEW_TYPE_LOADING);
            adapter.notifyDataSetChanged();
            final Handler handler = new Handler();

            PreferenceManager.save(PreferenceKeys.IS_PREVIOUS_MESSAGE_PARSED, true);

            final Runnable runnableEmptyView = new Runnable() {
                @Override
                public void run() {
                    handleEmptyViewCase();
                }
            };
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    SMSParser.parsePreviousMessages((Application) getContext().getApplicationContext(), new SMSParser.OnTaskCompletionListener() {
                        @Override
                        public void onTaskCompleted() {
                            handler.postDelayed(runnableEmptyView, 1000);
                        }
                    });
                }
            };


            ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.execute(runnable);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_READ_SMS_PERMISSION) {
            if (grantResults.length > 0) {
                boolean smsPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                if (smsPermission) {
                    if (onPermissionCallBack != null) {
                        onPermissionCallBack.permissionGranted();
                    }
                } else {
                    if (onPermissionCallBack != null) {
                        onPermissionCallBack.permissionDenied();
                    }
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_SMS)) {
                        PreferenceManager.save(PreferenceKeys.IS_SMS_PERMISSION_NEVER_ASK_CLICKED, true);
                    } else {

                    }
                }
            }
        }
    }

    private void handleEmptyViewCase() {
        smsViewModel.getCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null && integer == 0) {
                    Log.e("TAG", "count == 0");
                    adapter.setViewType(MyOrdersAdapter.VIEW_TYPE_EMPTY_LIST);
                    adapter.notifyDataSetChanged();
                    getPreviousMessages();
                }
                smsViewModel.getCount().removeObserver(this);
            }
        });
    }
}
