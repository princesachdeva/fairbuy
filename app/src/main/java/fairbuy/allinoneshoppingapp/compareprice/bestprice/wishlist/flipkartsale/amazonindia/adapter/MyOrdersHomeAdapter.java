package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ItemHomeRvBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.Status;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView.TimelineView;

/**
 * Created by prince.sachdeva on 19/12/18.
 */

public class MyOrdersHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Status> mProductDataArrayList;

    public void updateData(List<Status> smsProductData) {
        if (this.mProductDataArrayList == null) {
            this.mProductDataArrayList = new ArrayList<>();
        } else {
            this.mProductDataArrayList.clear();
        }
        if (smsProductData == null || smsProductData.size() == 0) {
            return;
        }
        int start = getItemCount();

        this.mProductDataArrayList.addAll(smsProductData);
        notifyItemRangeChanged(start, smsProductData.size());
    }

    public void clearData() {
        if (mProductDataArrayList != null) {
            mProductDataArrayList.clear();
        }
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ItemHomeRvBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_home_rv, viewGroup, false);
        binding.timeLineView.initLine(viewType);
        return new MyOrdersHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof MyOrdersHolder) {
            MyOrdersHolder holder = (MyOrdersHolder) viewHolder;
            Status status = getItem(i);
            holder.binding.setVariable(BR.status, status);
            ((ItemHomeRvBinding) holder.binding).timeLineView.setMarker(
                    Utils.getDrawable(((MyOrdersHolder) viewHolder).binding.getRoot().getContext(), R.drawable.ic_marker, R.color.color_009B2C));
            holder.binding.executePendingBindings();
        }
    }

    private Status getItem(int position) {
        int size = getItemCount();
        if (size > mProductDataArrayList.size()) {
            size = mProductDataArrayList.size();
        }
        if (position < size) {
            return mProductDataArrayList.get(size - position - 1);
        }
        Status status = new Status();
        status.setStatus("Upcoming Status");
        return status;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public int getItemCount() {
        return mProductDataArrayList != null ? mProductDataArrayList.size() == 1 ? 2 : mProductDataArrayList.size() > 3 ? 4 : mProductDataArrayList.size() : 0;
    }

    public static class MyOrdersHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public MyOrdersHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
