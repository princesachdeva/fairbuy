package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.SMSParser.SOURCE_AMAZON;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.SMSParser.SOURCE_FLIPKART;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.SMSParser.SOURCE_MYNTRA;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.text.TextUtils;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.DateTimeUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by aditya.amartya on 21/11/18
 */
@Entity(tableName = "sms_data")
public class SmsProductData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo
    private int id;

    @ColumnInfo(name = "product_name")
    private String productName;

    @ColumnInfo
    private int source;

    @ColumnInfo(name = "order_id")
    private String orderId;

    @ColumnInfo(name = "track_id")
    private String trackId;

    @ColumnInfo(name = "status")
    private ArrayList<Status> status;

    @ColumnInfo(name = "last_modified_time")
    private long lastModifiedTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ArrayList<Status> getStatus() {
        return status;
    }

    public void setStatus(ArrayList<Status> status) {
        this.status = status;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }


    @Override
    public boolean equals(Object obj) {

        if (obj == null) return false;
        if (obj == this) return true; //if both pointing towards same object on heap
        SmsProductData smsProductData = (SmsProductData) obj;

        if (this.getSource() != smsProductData.getSource()) {
            return false;
        }

        String productName = this.getLogicalProductName();
        String productName2 = smsProductData.getLogicalProductName();

        if (productName.contains(productName2)) {
            return true;
        } else if (productName2.contains(productName)) {
            return true;
        } else if (ShoppingUtils.matchedTextsPercentage(productName, productName2) > 60) {
            return true;
        } else {
            return false;
        }
    }


   /* @Override
    public boolean equals(Object obj) {

        if (obj == null) return false;
        if (obj == this) return true; //if both pointing towards same object on heap
        SmsProductData smsProductData = (SmsProductData) obj;

        if (this.getSource() != smsProductData.getSource()) {
            return false;
        }

        String orderId = this.getOrderId();
        String orderrId2 = smsProductData.getOrderId();

        if (!TextUtils.isEmpty(orderId) && orderId.equalsIgnoreCase(orderrId2)) {
            return true;
        }

        String productName = this.getLogicalProductName();
        String productName2 = smsProductData.getLogicalProductName();

        if (productName.contains(productName2)) {
            return true;
        } else if (productName2.contains(productName)) {
            return true;
        } else if (ShoppingUtils.matchedTextsPercentage(productName, productName2) > 60) {
            return true;
        } else {
            return false;
        }
    }*/

    public String getLogicalProductName() {
        if (!TextUtils.isEmpty(productName)) {
            productName = productName.replace(".", "").replace(",", "").trim();
        }
        return productName;
    }

    public void appendStatus(Status status) {
        if (status == null) {
            return;
        }

        this.lastModifiedTime = System.currentTimeMillis();

        if (this.status == null) {
            this.status = new ArrayList<Status>();
        }

        this.status.add(status);
    }

    public void appendStatusAtZero(Status status) {
        if (status == null) {
            return;
        }

        this.lastModifiedTime = System.currentTimeMillis();

        if (this.status == null) {
            this.status = new ArrayList<Status>();
        }

        this.status.add(0, status);
    }

    @Override
    public String toString() {
        return productName + "  " + source + "  " + status.toString();
    }

    public long getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(long lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public String getListingDisplayStatus() {
        StringBuilder stringBuilder = new StringBuilder();
        if (status != null && status.size() > 0) {
            Status status = this.status.get(0);
            if (status != null && !TextUtils.isEmpty(status.getStatus())) {
                stringBuilder.append(status.getStatus());
                if (status.getStatusDate() != 0) {
                    stringBuilder.append(" on ").append(DateTimeUtils.getFormattedDate(status.getStatusDate(), DateTimeUtils.DATE_FORMAT_ORDER_DETAIL_PAGE));
                }
            }
        }

        return stringBuilder.toString();
    }

    public int getAppIcon() {
        if (SOURCE_AMAZON == source) {
            return R.drawable.amazon;
        } else if (SOURCE_FLIPKART == source) {
            return R.drawable.flipkart;
        } else if (SOURCE_MYNTRA == source) {
            return R.drawable.myntra;
        }
        return R.drawable.flipkart;
    }
}