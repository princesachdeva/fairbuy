package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.bumptech.glide.Glide;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.WebSearchFragment;

import java.lang.ref.WeakReference;

/**
 * Created by aditya.amartya on 04/10/18
 */
public class FetchBitmap extends AsyncTask<String, Integer, Bitmap> {
    private WeakReference<BitmapListener> bitmapListener;

    public FetchBitmap(BitmapListener bitmapListener) {
        this.bitmapListener = new WeakReference<>(bitmapListener);
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        if (bitmapListener != null && bitmapListener.get() != null) {
            WebSearchFragment context = (WebSearchFragment) bitmapListener.get();
            try {
                return Glide
                        .with(context)
                        .asBitmap()
                        .load(strings[0])
                        .submit()
                        .get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (bitmapListener != null && bitmapListener.get() != null) {
            bitmapListener.get().setBitmap(bitmap);
        }
    }

    public interface BitmapListener {
        void setBitmap(Bitmap bitmap);
    }
}
