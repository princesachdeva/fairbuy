package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.WishListRepository;

/**
 * Created by prince.sachdeva on 29/10/18.
 */

public class ChromeParser {

    public interface OnProductPageListener {
        void onProductPage(Product product);
    }

    public interface OnProductSearchPageListener {
        void onProductSearch(ArrayList<Product> products);
    }

    public interface OnProductSuggestionsListener {
        void onProductsList(ArrayList<Product> product);
    }

    public static void parseURL(final String url, final OnProductPageListener onProductPageListener) {
        final ShoppingUtils.WEBSITE website = getWebSiteType(url);
        if (website == null) {
            // NOT A PRDUCT DETAIL PAGE
            onProductPageListener.onProductPage(null);
            return;
        }

        if (url.contains("/p/") || url.contains("/dp/") || url.contains("/gp/") || url.contains("/buy") || url.contains("/product/") || url.contains("-pdp") || (url.contains(".htm")
                && url.contains("jabong"))) {

            ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    getDataFrmChromeApp(url, website, onProductPageListener);
                }
            });
        } else {
            onProductPageListener.onProductPage(null);
            return;
        }
    }

    private static void getDataFrmChromeApp(String url, ShoppingUtils.WEBSITE website, final OnProductPageListener onProductPageListener) {
        Product cachedProduct = ((ShoppingAssistantApplication) ShoppingAssistantApplication.getContext()).getDataFromCache(url);
        if (cachedProduct != null) {
            // TEST THIS FUNCTIONALITY
            postResponseToMainThread(onProductPageListener, cachedProduct);
            return;
        }

        Document document = null;
        try {
            document = Jsoup.connect(url).userAgent("Chrome").get();

            if (document == null) {
                postResponseToMainThread(onProductPageListener, null);
                return;
            }
            final Product product = new Product();
            if (website == ShoppingUtils.WEBSITE.JABONG) {
                product.productName = document.getElementsByClass("product-title").text();
                product.breadCrumb = getBreadCrumbFromJabong(document);
                product.productPrice = getPriceFromJabong(document);
                product.productBrand = getBrandFromJabong(document);
                product.productImage = document.getElementsByClass("carousal loaded").attr("src");
            } else if (website == ShoppingUtils.WEBSITE.FLIPKART) {
                product.productName = document.getElementsByClass("_35KyD6").text();
                product.breadCrumb = getBreadCrumbFromFlipkart(document);
                product.productPrice = getPriceFromFlipkart(document);
                product.productImage = getProductImageFromFlipkart(document);
            } else if (website == ShoppingUtils.WEBSITE.AMAZON) {
                product.productName = document.getElementById("title").text();
                product.breadCrumb = getBreadCrumbFromAmazon(document);
                product.productPrice = getPriceFromAmazon(document);
                product.productBrand = getBrandFromAmazon(document);
                product.productImage = getProductImageFromAmazon(document);
            } else if (website == ShoppingUtils.WEBSITE.MYNTRA) {
                product.productName = getTitleFromMyntra(document.title());
                product.productPrice = getPriceFromMyntra(document);
                product.productBrand = getBrandFromMyntra(document);
                product.productImage = getProductImageFromMyntra(document);
            } else if (website == ShoppingUtils.WEBSITE.SNAPDEAL) {
                product.productName = document.getElementById("productNamePDP").attr("value");
                product.breadCrumb = getBreadCrumbFromSnapdeal(document);
                product.productPrice = getPriceFromSnapdeal(document);
                product.productImage = getProductImageFromSnapdeal(document);
            } else if (website == ShoppingUtils.WEBSITE.PAYTM) {
                product.productName = document.getElementsByClass("NZJI").text();
                product.breadCrumb = getBreadCrumbFromPaytm(document);
                product.productPrice = getPriceFromPAYTM(document);
                product.productImage = getProductImageFromPAYTM(document);
            } else if (website == ShoppingUtils.WEBSITE.AJIO) {

            }

            //If product is not available, then return null
            if (TextUtils.isEmpty(product.productName)) {
                postResponseToMainThread(onProductPageListener, null);
                return;
            }
            product.productURL = url;
            product.webSiteName = website.toString();
            product.website = website;
            if (TextUtils.isEmpty(product.productBrand)) {
                product.productBrand = product.deducedBrand = ShoppingUtils.getBrandFromTitle(ShoppingAssistantApplication.getContext(), product.productName);
                product.isWishlisted = WishListRepository.getInstance((Application) ShoppingAssistantApplication.getContext()).isWishlisted(url);
            }
            postResponseToMainThread(onProductPageListener, product);

        } catch (Exception e) {
            postResponseToMainThread(onProductPageListener, null);
            e.printStackTrace();
        }
    }

    private static void postResponseToMainThread(final OnProductPageListener onProductPageListener, final Product product) {
        if (product != null && !TextUtils.isEmpty(product.productURL)) {
            ((ShoppingAssistantApplication) ShoppingAssistantApplication.getContext()).insertInCache(product.productURL, product);
        }
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                onProductPageListener.onProductPage(product);
            }
        });
    }

    private static void postResponseToMainThread(final OnProductSearchPageListener onProductPageListener, final ArrayList<Product> products, String searchURL) {
        if (products != null && products.size() > 0) {
            ((ShoppingAssistantApplication) ShoppingAssistantApplication.getContext()).insertInCache(searchURL, products);
        }
        Log.e("TAG", "postResponseToMainThread");
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                onProductPageListener.onProductSearch(products);
            }
        });
    }

    public static ShoppingUtils.WEBSITE getWebSiteType(String url) {
        if (url.contains(".htm") && url.contains("jabong")) {
            return ShoppingUtils.WEBSITE.JABONG;
        }

        if (url.contains("/p/") && url.contains("flipkart")) {
            return ShoppingUtils.WEBSITE.FLIPKART;
        }
        if ((url.contains("/p/") || url.contains("/s/")) && url.contains("ajio")) {
            return ShoppingUtils.WEBSITE.AJIO;
        }

        if ((url.contains("/dp/") || url.contains("/gp/")) && url.contains("amazon.in")) {
            return ShoppingUtils.WEBSITE.AMAZON;
        }

        if (url.contains("/buy") && url.contains("myntra")) {
            return ShoppingUtils.WEBSITE.MYNTRA;
        }

        if (url.contains("/product/") && url.contains("snapdeal")) {
            return ShoppingUtils.WEBSITE.SNAPDEAL;
        }

        if ((url.contains("-pdp") || url.contains("/p/")) && url.contains("paytm")) {
            return ShoppingUtils.WEBSITE.PAYTM;
        }
        return null;
    }


    private static String getBreadCrumbFromJabong(Document document) {

        if (document == null) {
            return "";
        }
        StringBuilder breadCrumb = new StringBuilder();
        Elements elementsByClass = document.getElementsByClass("brand");
        if (elementsByClass != null) {
            breadCrumb.append(elementsByClass.text());
            elementsByClass = document.getElementsByClass("product-title");
            if (elementsByClass != null) {
                String value = elementsByClass.text();
                if (!TextUtils.isEmpty(value)) {
                    breadCrumb.append("/").append(value);
                }
            }
        }
        return breadCrumb.toString();
    }

    private static String getBreadCrumbFromFlipkart(Document document) {

        if (document == null) {
            return "";
        }
        StringBuilder breadCrumb = new StringBuilder();
        Elements elementsByClass = document.getElementsByClass("_1HEvv0");
        int i = 0;
        for (Element e : elementsByClass) {
            if (i != 0) {
                breadCrumb.append("/");
            }
            breadCrumb.append(e.text());
            i++;
        }
        return breadCrumb.toString();
    }

    private static String getBreadCrumbFromPaytm(Document document) {

        if (document == null) {
            return "";
        }

        StringBuilder breadCrumb = new StringBuilder();
        Elements elementsByClass = document.getElementsByClass("Tk9i");
        int i = 0;
        for (Element e : elementsByClass) {
            if (i != 0) {
                breadCrumb.append("/");
            }
            breadCrumb.append(e.text());
            i++;
        }
        return breadCrumb.toString();
    }

    private static String getBreadCrumbFromAmazon(Document document) {

        if (document == null) {
            return "";
        }
        StringBuilder breadCrumb = new StringBuilder();
        Elements elementsByClass = document.getElementsByClass("a-link-normal a-color-tertiary");
        int i = 0;
        for (Element e : elementsByClass) {
            if (i != 0) {
                breadCrumb.append("/");
            }
            breadCrumb.append(e.text());
            i++;
        }
        Element title = document.getElementById("title");
        if (title != null) {
            breadCrumb.append("/").append(title.text());
        }
        return breadCrumb.toString();
    }


    private static String getBreadCrumbFromSnapdeal(Document document) {
        if (document == null) {
            return "";
        }
        StringBuilder breadCrumb = new StringBuilder();
        Elements elementsByClass = document.getElementsByClass("bCrumbOmniTrack");
        int i = 0;
        for (Element e : elementsByClass) {
            if (i != 0) {
                breadCrumb.append("/");
            }
            breadCrumb.append(e.text());
            i++;
        }
        Element title = document.getElementById("productNamePDP");
        if (title != null) {
            String value = title.attr("value");
            if (!TextUtils.isEmpty(value)) {
                breadCrumb.append("/").append(value);
            }
        }
        return breadCrumb.toString();
    }

    private static String getTitleFromMyntra(String title) {
        try {
            if (title.contains("-")) {
                String[] split = title.split("-");
                if (split.length > 0) {
                    title = split[0];
                    if (title.contains("Buy")) {
                        title = title.replace("Buy", "");
                    }
                }
            }

        } catch (Exception e) {
        }
        return title;
    }

    private static String getPriceFromJabong(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementsByClass("actual-price").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getBrandFromJabong(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementsByClass("brand").text();
        } catch (Exception e) {
            return null;
        }
    }


    private static String getPriceFromFlipkart(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementsByClass("_1vC4OE _3qQ9m1").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getProductImageFromFlipkart(Document document) {
        if (document == null) {
            return null;
        }
        try {


            Elements elementsByClass = document.getElementsByClass("_1k8TbK");
            if (elementsByClass != null && elementsByClass.size() > 0) {
                String src = elementsByClass.attr("src");
                if (!TextUtils.isEmpty(src)) {
                    return getHigherResolutionImage(src);
                }
            }

            elementsByClass = document.getElementsByClass("_2_AcLJ");
            if (elementsByClass != null && elementsByClass.size() > 0) {
                Element element = elementsByClass.get(0);
                if (element != null) {
                    String style = element.attr("style");
                    if (!TextUtils.isEmpty(style)) {
                        String url = style.replace("background-image:url(", "").replace(")", "");
                        return getHigherResolutionImage(url);
                    }
                }
            }

            Elements script = document.getElementsByTag("script");
            for (Element element : script) {
                if (element != null) {
                    String data = element.data();
                    if (!TextUtils.isEmpty(data)) {
                        try {
                            JSONArray jsonArray = new JSONArray(data);
                            if (jsonArray != null) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    if (jsonObject != null) {
                                        String image = jsonObject.optString("image");
                                        if (!TextUtils.isEmpty(image)) {
                                            return getHigherResolutionImage(image);
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {

                        }
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getHigherResolutionImage(String url) {
        if (!TextUtils.isEmpty(url)) {
            url = url.replace("/144/144/", "/256/256/");
            url = url.replace("/128/128/", "/256/256/");
        }
        return url;
    }

    private static String getPriceFromSnapdeal(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementsByClass("payBlkBig").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getProductImageFromSnapdeal(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementsByClass("tileImg lazy-load").get(0).attr("data-src");
        } catch (Exception e) {
            return null;
        }
    }

    private static String getPriceFromMyntra(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementsByClass("pdp-price").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getBrandFromMyntra(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementsByClass("pdp-title").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getProductImageFromMyntra(Document document) {
        if (document == null) {
            return null;
        }
        try {
            Elements elements = document.getElementsByTag("meta");
            for (Element e : elements) {
                String name = e.attr("name");
                if ("twitter:image".equalsIgnoreCase(name)) {
                    return e.attr("content");
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    private static String getPriceFromAmazon(Document document) {
        if (document == null) {
            return null;
        }
        try {
            String dealprice = null;
            Element element = document.getElementById("priceblock_dealprice");
            if (element != null) {
                dealprice = element.text();
            }
            if (TextUtils.isEmpty(dealprice)) {
                element = document.getElementById("priceblock_ourprice");
                if (element != null) {
                    dealprice = element.text();
                }
            }

            if (TextUtils.isEmpty(dealprice)) {
                return document.getElementsByClass("a-size-base a-color-price a-color-price").text();
            }
            return dealprice;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getBrandFromAmazon(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementById("bylineInfo").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getProductImageFromAmazon(Document document) {
        if (document == null) {
            return null;
        }
        try {
            String image = null;
            Element elementById = document.getElementById("imgTagWrapperId");
            if (elementById == null) {
                elementById = document.getElementById("img-wrapper");
            }
            if (elementById != null) {
                Elements img = elementById.getElementsByTag("img");
                if (img != null) {
                    image = img.attr("data-old-hires");
                    if (TextUtils.isEmpty(image)) {
                        String imgArray = img.attr("data-a-dynamic-image");
                        if (!TextUtils.isEmpty(imgArray)) {
                            image = new JSONObject(imgArray).keys().next();
                        }
                    }
                }
            }

            return image;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getPriceFromPAYTM(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementsByClass("_1V3w").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getProductImageFromPAYTM(Document document) {
        if (document == null) {
            return null;
        }
        try {
            return document.getElementsByClass("_3v_O").attr("src");
        } catch (Exception e) {
            return null;
        }
    }

    public static void getSuggestionsFromGoogle(String title, final OnProductSuggestionsListener onProductSuggestionsListener) {
        title = title + " site:amazon.in OR site:snapdeal.com OR site:flipkart.com OR site:paytm.com OR site:myntra.com";
        String query;
        try {
            query = URLEncoder.encode(title, "utf-8");
        } catch (UnsupportedEncodingException e) {
            query = title;
            e.printStackTrace();
        }
        final String url = "https://www.google.co.in/search?q=" + query;

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                hitSearchQuery(url, onProductSuggestionsListener);
            }
        });
    }

    private static void hitSearchQuery(final String url, final OnProductSuggestionsListener onProductSuggestionsListener) {
        final ArrayList<String> productsURL = new ArrayList<>();
        final ArrayList<Product> products = new ArrayList<>();
        try {
            Document document = Jsoup.connect(url).userAgent("Chrome").get();
            if (document == null) {
                onProductSuggestionsListener.onProductsList(products);
                return;
            }

            Elements elements = document.getElementsByClass("g");

            if (elements == null) {
                onProductSuggestionsListener.onProductsList(products);
                return;
            }

            for (Element e : elements) {
                Elements elementsByClass = e.getElementsByClass("r");
                if (elementsByClass != null && elementsByClass.get(0) != null) {
                    Elements a = elementsByClass.get(0).getElementsByTag("a");
                    if (a != null && a.get(0) != null) {
                        String productUrl = a.get(0).attr("href");
                        if (!TextUtils.isEmpty(productUrl)) {
                            if (productUrl.contains("flipkart") || productUrl.contains("snapdeal") || productUrl.contains("amazon") || productUrl.contains("paytm")) {
                                String[] splitBySA = productUrl.split("&sa");
                                if (splitBySA != null && splitBySA.length > 0) {
                                    productUrl = splitBySA[0];
                                    if (!TextUtils.isEmpty(productUrl) && productUrl.contains("/url?q=")) {
                                        productUrl = productUrl.replace("/url?q=", "");
                                        productsURL.add(productUrl);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            int size = productsURL.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    String productURL = productsURL.get(i);
                    if (!TextUtils.isEmpty(productURL)) {
                        parseURL(productURL, new OnProductPageListener() {
                            @Override
                            public void onProductPage(Product product) {
                                products.add(product);
                                if (products.size() == productsURL.size()) {
                                    postResponseToMainThread(onProductSuggestionsListener, products);
                                }
                            }
                        });
                    }
                }
            } else {
                postResponseToMainThread(onProductSuggestionsListener, products);
            }
        } catch (Exception e) {
            e.printStackTrace();
            postResponseToMainThread(onProductSuggestionsListener, products);
        }
    }

    private static void postResponseToMainThread(final OnProductSuggestionsListener onProductSuggestionsListener, final ArrayList<Product> products) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                onProductSuggestionsListener.onProductsList(products);
            }
        });
    }

    public static void getProductDetails(final String title, final String price, final String ratingCount, final ShoppingUtils.WEBSITE websiteName,
            final OnProductSearchPageListener onProductSearchPageListener) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                getProductDetailsFromTitle(title, price, ratingCount, websiteName, onProductSearchPageListener);
            }
        });
    }

    private static void getProductDetailsFromTitle(final String title, String price, String ratingCount, ShoppingUtils.WEBSITE websiteName,
            final OnProductSearchPageListener onProductSearchPageListener) {
        final String searchURL = websiteName.getSearchQuery() + title;
        final ShoppingAssistantApplication shoppingAssistantApplication = (ShoppingAssistantApplication) ShoppingAssistantApplication.getContext();
        final ArrayList<Product> products = shoppingAssistantApplication.getProductsListDataFromCache(searchURL);
        if (products != null && products.size() > 0) {
            Log.e("TAG", "cached Product");
            postResponseToMainThread(onProductSearchPageListener, products, searchURL);
            return;
        }

        if (TextUtils.isEmpty(price)) {
            postResponseToMainThread(onProductSearchPageListener, products, searchURL);
            return;
        }

        double numericPrice = getPriceInNumericFormat(price);
        if (numericPrice == 0) {
            postResponseToMainThread(onProductSearchPageListener, products, searchURL);
            return;
        }


        if (products.size() == 0) {
            try {
                Document document = Jsoup.connect(searchURL).userAgent("Chrome").get();
                if (document == null) {
                    postResponseToMainThread(onProductSearchPageListener, products, searchURL);
                    return;
                }

                if (websiteName == ShoppingUtils.WEBSITE.FLIPKART) {
                    getListingFromFlipkart(numericPrice, ratingCount, products, document);
                } else if (websiteName == ShoppingUtils.WEBSITE.AMAZON) {
                    getListingFromAmazon(numericPrice, ratingCount, products, document);
                } else if (websiteName == ShoppingUtils.WEBSITE.MYNTRA) {
                    getListingFromMyntra(numericPrice, ratingCount, products, document);
                } else if (websiteName == ShoppingUtils.WEBSITE.SNAPDEAL) {
                    getListingFromSnapdeal(numericPrice, ratingCount, products, document);
                } else if (websiteName == ShoppingUtils.WEBSITE.PAYTM) {
                    getListingFromPaytm(numericPrice, ratingCount, products, document);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (products.size() == 0) {
            postResponseToMainThread(onProductSearchPageListener, products, searchURL);
            return;
        }
        final ArrayList<Product> matchedProducts = new ArrayList<>();
        for (int i = products.size() - 1; i >= 0; i--) {
            Product product = products.get(i);
            if (product == null) {
                if (products.size() > 0) {
                    products.remove(products.size() - 1);
                }
                if (products.size() == 0) {
                    Collections.reverse(matchedProducts);
                    postResponseToMainThread(onProductSearchPageListener, matchedProducts, searchURL);
                    break;
                }
                continue;
            }

            String productURL = product.productURL;
            if (!TextUtils.isEmpty(productURL)) {
                final int finalI = i;
                parseURL(productURL, new OnProductPageListener() {
                    @Override
                    public void onProductPage(Product product) {
                        if (products.size() > 0) {
                            products.remove(products.size() - 1);
                        }
                        if (product != null) {
                            if ((!TextUtils.isEmpty(product.productName) && product.productName.replaceAll("\\s", "").equalsIgnoreCase(title.replaceAll("\\s", "")))) {
                                matchedProducts.add(product);
                            }
                        }
                        if (products.size() == 0) {
                            Collections.reverse(matchedProducts);
                            postResponseToMainThread(onProductSearchPageListener, matchedProducts, searchURL);
                        }
                    }
                });
            } else {
                if (products.size() > 0) {
                    products.remove(products.size() - 1);
                }
                if (products.size() == 0) {
                    Collections.reverse(matchedProducts);
                    postResponseToMainThread(onProductSearchPageListener, matchedProducts, searchURL);
                }
            }
        }
    }

    private static void getListingFromFlipkart(double price, String ratingCount, ArrayList<Product> products, Document document) {
        Elements elements = document.getElementsByClass("_3liAhj _1R0K0g");
        if (elements == null || elements.size() == 0) {
            // List Handling
            elements = document.getElementsByClass("_1UoZlX");
        }
        if (elements != null) {
            for (Element element : elements) {
                String listingNameFromFlipkart = getListingNameFromFlipkart(element);
                String listingPrice = getListingPriceFromFlipkart(element);
                double priceInNumericFormat = getPriceInNumericFormat(listingPrice);
                if (priceInNumericFormat != 0) {
                    if (priceInNumericFormat == price) {
                        String listingRatingFromFlipkart = getListingRatingFromFlipkart(element);
                        if (true || TextUtils.isEmpty(ratingCount) || ratingCount.equalsIgnoreCase(listingRatingFromFlipkart)) {
                            String listingLinkFromFlipkart = getListingLinkFromFlipkart(element);
                            Product p = new Product();
                            p.productName = listingNameFromFlipkart;
                            p.productURL = listingLinkFromFlipkart;
                            p.productPrice = listingPrice;
                            p.ratedUsersCount = listingRatingFromFlipkart;
                            if (!TextUtils.isEmpty(listingLinkFromFlipkart)) {
                                products.add(p);
                            }
                        }
                    }
                }
            }
        }
    }

    private static String getListingNameFromFlipkart(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String text = element.getElementsByClass("_2cLu-l").text();
            if (TextUtils.isEmpty(text)) {
                text = element.getElementsByClass("_3wU53n").text();
            }
            return text;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingPriceFromFlipkart(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return element.getElementsByClass("_1vC4OE").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingRatingFromFlipkart(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return element.getElementsByClass("_38sUEc").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingLinkFromFlipkart(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String href = element.getElementsByClass("Zhf2z-").attr("href");
            if (TextUtils.isEmpty(href)) {
                href = element.getElementsByClass("_31qSD5").attr("href");
            }
            if (!href.contains("http")) {
                href = "https://www.flipkart.com" + href;
            }
            return href;
        } catch (Exception e) {
            return null;
        }
    }

    private static void getListingFromAmazon(double price, String ratingCount, ArrayList<Product> products, Document document) {
        Elements elements = document.getElementsByClass("a-fixed-left-grid-inner");
        if (elements != null) {
            for (Element element : elements) {
                String listingName = getListingNameFromAmazon(element);
                String listingPrice = getListingPriceFromAmazon(element);
                double priceInNumericFormat = getPriceInNumericFormat(listingPrice);
                if (priceInNumericFormat != 0) {
                    if (priceInNumericFormat == price) {
                        String listingRating = getListingRatingFromAmazon(element);
                        if (true || TextUtils.isEmpty(ratingCount) || ratingCount.equalsIgnoreCase(listingRating)) {
                            String listingLink = getListingLinkFromAmazon(element);
                            Product p = new Product();
                            p.productName = listingName;
                            p.productURL = listingLink;
                            p.productPrice = listingPrice;
                            p.ratedUsersCount = listingRating;
                            if (!TextUtils.isEmpty(listingLink)) {
                                products.add(p);
                            }
                        }
                    }
                }
            }
        }
    }


    private static String getListingNameFromAmazon(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return element.getElementsByClass("a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal").get(0).attr("title");
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingPriceFromAmazon(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String text = element.getElementsByClass("a-size-base a-color-price s-price a-text-bold").text();
            if (TextUtils.isEmpty(text)) {
                text = element.getElementsByClass(" a-size-base a-color-price a-text-bold").text();
            }
            text = text.replaceAll("(^\\h*)|(\\h*$)", "");
            return text;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingRatingFromAmazon(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return element.getElementsByClass("a-size-small a-link-normal a-text-normal").text();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingLinkFromAmazon(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String href = element.getElementsByClass("a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal").get(0).attr("href");
            ;
            if (!href.contains("http")) {
                href = "https://www.amazon.com" + href;
            }
            return href;
        } catch (Exception e) {
            return null;
        }
    }


    private static void getListingFromMyntra(double price, String ratingCount, ArrayList<Product> products, Document document) {
        Elements elements = document.getElementsByTag("script");
        if (elements != null) {
            for (Element element : elements) {
                if (element == null) {
                    return;
                }
                String data = element.data();
                if (TextUtils.isEmpty(data)) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject == null) {
                        return;
                    }
                    String type = jsonObject.optString("@type", "");
                    if (!TextUtils.isEmpty(type) && type.equalsIgnoreCase("Product")) {
                        String listingPrice = jsonObject.optString("price", "0");
                        double priceInNumericFormat = getPriceInNumericFormat(listingPrice);
                        if (priceInNumericFormat != 0) {
                            if (priceInNumericFormat == price) {
                                String listingRating = getListingRatingFromMyntra(element);
                                if (true || TextUtils.isEmpty(ratingCount) || ratingCount.equalsIgnoreCase(listingRating)) {
                                    String listingLink = getListingLinkFromMyntra(element);
                                    Product p = new Product();
                                    p.productURL = listingLink;
                                    p.productPrice = listingPrice;
                                    p.ratedUsersCount = listingRating;
                                    if (!TextUtils.isEmpty(listingLink)) {
                                        products.add(p);
                                    }
                                }
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }
    }


    private static String getListingNameFromMyntra(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return "";
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingPriceFromMyntra(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String text = element.getElementsByClass("product-discountedPrice").text();
            if (TextUtils.isEmpty(text)) {
                text = element.getElementsByClass("product-price").text();
            }
            text = text.replaceAll("(^\\h*)|(\\h*$)", "");
            return text;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingRatingFromMyntra(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return "";
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingLinkFromMyntra(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String href = element.getElementsByTag("a").get(0).attr("href");
            ;
            if (!href.contains("http")) {
                href = "http://www.myntra.com/" + href;
            }
            return href;
        } catch (Exception e) {
            return null;
        }
    }


    private static void getListingFromSnapdeal(double price, String ratingCount, ArrayList<Product> products, Document document) {
        Elements elements = document.getElementsByClass("disInBlock trackDPClick_h");
        if (elements != null) {
            for (Element element : elements) {
                String listingPrice = getListingPriceFromSnapdeal(element);
                double priceInNumericFormat = getPriceInNumericFormat(listingPrice);
                if (priceInNumericFormat != 0) {
                    if (priceInNumericFormat == price) {
                        String listingRating = getListingRatingFromSnapdeal(element);
                        if (true || TextUtils.isEmpty(ratingCount) || ratingCount.equalsIgnoreCase(listingRating)) {
                            String listingLink = getListingLinkFromSnapdeal(element);
                            Product p = new Product();
                            p.productURL = listingLink;
                            p.productPrice = listingPrice;
                            p.ratedUsersCount = listingRating;
                            if (!TextUtils.isEmpty(listingLink)) {
                                products.add(p);
                            }
                        }
                    }
                }
            }
        }
    }


    private static String getListingNameFromSnapdeal(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return "";
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingPriceFromSnapdeal(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String text = element.attr("data-price");
            // Pending No Sale Case
            if (TextUtils.isEmpty(text)) {
                text = element.getElementsByClass("product-price").text();
            }
            text = text.replaceAll("(^\\h*)|(\\h*$)", "");
            return text;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingRatingFromSnapdeal(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return "";
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingLinkFromSnapdeal(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String href = element.attr("href");
            ;
            if (!href.contains("http")) {
                href = "http://www.snapdeal.com/" + href;
            }
            return href;
        } catch (Exception e) {
            return null;
        }
    }


    private static void getListingFromPaytm(double price, String ratingCount, ArrayList<Product> products, Document document) {
        Elements elements = document.getElementsByClass("_2i1r");
        if (elements != null) {
            for (Element element : elements) {
                String listingPrice = getListingPriceFromPaytm(element);
                double priceInNumericFormat = getPriceInNumericFormat(listingPrice);
                if (priceInNumericFormat != 0) {
                    // For Paytm Traverse all listing
                    if (true || priceInNumericFormat == price) {
                        String listingRating = getListingRatingFromPaytm(element);
                        if (true || TextUtils.isEmpty(ratingCount) || ratingCount.equalsIgnoreCase(listingRating)) {
                            String listingLink = getListingLinkFromPaytm(element);
                            Product p = new Product();
                            p.productURL = listingLink;
                            p.productPrice = listingPrice;
                            p.ratedUsersCount = listingRating;
                            if (!TextUtils.isEmpty(listingLink)) {
                                products.add(p);
                            }
                        }
                    }
                }
            }
        }
    }


    private static String getListingNameFromPaytm(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return "";
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingPriceFromPaytm(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String text = element.getElementsByClass("_1kMS").text();
            // Pending No Sale Case
            if (TextUtils.isEmpty(text)) {
                text = element.getElementsByClass("product-price").text();
            }
            text = text.replaceAll("(^\\h*)|(\\h*$)", "");
            return text;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingRatingFromPaytm(Element element) {
        if (element == null) {
            return null;
        }
        try {
            return "";
        } catch (Exception e) {
            return null;
        }
    }

    private static String getListingLinkFromPaytm(Element element) {
        if (element == null) {
            return null;
        }
        try {
            String href = element.getElementsByTag("a").attr("href");
            ;
            if (!href.contains("http")) {
                href = "https://www.paytmmall.com" + href;
            }
            return href;
        } catch (Exception e) {
            return null;
        }
    }

    public static double getPriceInNumericFormat(String price) {
        if (TextUtils.isEmpty(price)) {
            return 0;
        }

        try {
            if (price.contains("-")) {
                String[] priceArray = price.split("-");
                if (priceArray.length > 0) {
                    price = priceArray[0];
                }
            }
            price = price.replaceAll("(^\\h*)|(\\h*$)", "");
            price = price.replace(",", "").replace(AssistantConstants.RUPEE, "").replace("Rs.", "").replace("Rs", "");
            return Double.parseDouble(price);
        } catch (Exception e) {
            return 0;
        }
    }

    public static String getPriceInStringFormat(String price) {
        double value = getPriceInNumericFormat(price);
        if (value == 0) {
            return price;
        } else {
            return AssistantConstants.RUPEE + " " + value;
        }
    }


}

//<script>
//  (function() {
//    var cx = '006274864137540837670:miaykmfzw6w';
//    var gcse = document.createElement('script');
//    gcse.type = 'text/javascript';
//    gcse.async = true;
//    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
//    var s = document.getElementsByTagName('script')[0];
//    s.parentNode.insertBefore(gcse, s);
//  })();
//</script>
//<gcse:search></gcse:search>
//AIzaSyCXWYW8gqAW98GqnYPf3cpBcgI5_KE2RB0
