package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by aditya.amartya on 05/12/18
 */
public class ArchivoMediumTextView extends AppCompatTextView {
    public ArchivoMediumTextView(Context context) {
        super(context);
        setFonts(context);
    }

    public ArchivoMediumTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setFonts(context);
    }

    private void setFonts(Context ctx) {
        Typeface primeTypeFace = Typeface.createFromAsset(ctx.getAssets(), "fonts/Archivo-Medium.ttf");
        setTypeface(primeTypeFace);
    }
}
