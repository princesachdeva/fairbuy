package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.receiver;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.SMSParser;

/**
 * Created by prince.sachdeva on 21/11/18.
 */

public class SMSReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction() != null && intent.getAction().equals(SMS_RECEIVED)) {
            final Bundle bundle = intent.getExtras();
            try {
                if (bundle != null) {

                    final Object[] pdusObj = (Object[]) bundle.get("pdus");

                    String sender = null;
                    StringBuilder message = new StringBuilder();
                    for (int i = 0; i < pdusObj.length; i++) {
                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        sender = currentMessage.getDisplayOriginatingAddress();
                        message.append(currentMessage.getDisplayMessageBody());
                    }
                    if (!TextUtils.isEmpty(sender) && !TextUtils.isEmpty(message.toString())) {
                        SMSParser.parseIncomingMessage(message.toString(), sender, (Application) context.getApplicationContext());
                    }
                }
            } catch (Exception e) {
                Log.e("######", "Exception smsReceiver" + e);

            }

        }
    }
}
