package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistItem;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.DbCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.WishListRepository;


public class WishlistViewModel extends AndroidViewModel {
    private WishListRepository mWishListRepository;

    public WishlistViewModel(@NonNull Application application) {
        super(application);
        mWishListRepository = WishListRepository.getInstance(application);
    }

    public LiveData<List<WishlistItem>> getLiveData() {
        return mWishListRepository.getWishList();
    }

    public LiveData<List<WishlistItem>> getWishListLiveDataHome() {
        return mWishListRepository.getWishListHome();
    }


    public void insertWishListData(WishlistItem wishlist, DbCallBack dbCallBack) {
        mWishListRepository.insertWishListItem(wishlist, dbCallBack);
    }

    public void deleteWishListData(String productUrl, DbCallBack dbCallBack) {
        mWishListRepository.deleteWishListItem(productUrl, dbCallBack);
    }

    public void isWishListed(String productUrl, DbCallBack dbCallBack) {
        mWishListRepository.isWishlisted(productUrl, dbCallBack);
    }

    public LiveData<Integer> getCount() {
        return mWishListRepository.getCount();
    }
}
