package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper;

/**
 * Created by prince.sachdeva on 13/02/19.
 */

public interface AnalyticsConstants {

    String SEPERATOR = "-";
    String SEPERATOR_UNDERSCORE = "_";

    public interface Category {
        String PURCHASE_GOAL = "PurchaseGoal";
        String WISHLISST_GOAL = "WishlistGoal";
    }

    public interface Action {
        String SEARCH = "Search";
        String SELECT_STORE = "SelectStore";
        String RECENTLY_VIEWED = "RecViewed";
        String BUBBLE = "Bubble";
        String COMPARE = "Compare";
        String WISHLIST = "Wishlist";
        String BESTSELLER = "BestSeller";
        String DEMO = "Demo";
    }

    public interface Label {
        String SEARCH_HOME = "Search-Home";
        String SEARCH_NAV = "Search-Nav";
        String SEARCH_KEYWORD = "SS-Keyword-";
        String SEARCH_TRENDING = "SS-Trending-";
        String SEARCH_RESPONSE_ZERO = "Results-Zero-";
        String SEARCH_RESPONSE_COUNT = "Results-";
        String SEARCH_PRODUCT_VIEWED = "ProductViewed-search-";
        String PRODUCT_VIEWED = "ProductViewed-";


        String CLICK_STORE = "WebView-";

        String RV_HOME = "RV-home";
        String RV_NAV = "RV-nav";

        String BUBBLE_SHOW = "Show-";
        String BUBBLE_CLICK = "Click-";
        String BUBBLE_COMPARE = "Compare-";
        String BUBBLE_WISHLIST = "Wishlist-";
        String BUBBLE_WISHLIST_ERROR = "WishlistError-";


        String COMPARE_SHOW = "Show-";
        String COMPARE_RESULT_COUNT_ZERO = "CompareResults-Zero-";
        String COMPARE_RESULT_COUNT = "CompareResults-";
        String COMPARE_PRODUCT_VIEWED = "compare";
        String COMPARE_CLOSE = "Click-Close";


        String BESTSELLER_HOME = "Bestseller-Home";
        String DEMO_HOME = "Demo-Home";

        String WISHLIST_HOME = "Wishlist-Home";
        String WISHLIST_NAV = "Wishlist-Nav";
        String CHROME_BROWSER = "Chrome Browser";
        String NATIVE = "NATIVE";
        String WEBVIEW = "WEBVIEW";


    }

    public interface FirebaseEvents {
        String SEARCH_NAV = "PurchaseGoal_Search_SearchNav";
        String SEARCH = "PurchaseGoal_Search_SS_Keyword";
        String SEARCH_RESULT = "PurchaseGoal_Search_Results";
        String SEARCH_TERM = "search_term";
        String SEARCH_SELECTED = "PurchaseGoal_Search_Viewed";
        String BESTSERLLER_SELECTED = "PurchaseGoal_BestSeller_ProductViewed";
        String COMPARE_ITEM_SELECTED = "PurchaseGoal_Compare_ProductViewed";
        String WISHLIST_ITEM_SELECTED = "PurchaseGoal_Wishlist_ProductViewed";
        String RECENT_ITEM_SELECTED = "PurchaseGoal_RecViewed_ProductViewed";
        String DEMO_SELECTED = "PurchaseGoal_Demo_ProductViewed";
        ;
        String BUBBLE_WISHLIST_INAPP = "bubble_wishlist_inapp";



        String COMPARE_BASEPRODUCT = "PurchaseGoal_Compare_Show";
        String COMPARE_RESULT = "PurchaseGoal_Compare_Results";
        String SELECT_STORE = "PurchaseGoal_SelectStore_Webview";

        String BUBBLE_SHOW = "PurchaseGoal_Bubble_Show";
        String BUBBLE_CLICK = "PurchaseGoal_Bubble_Click";
        String BUBBLE_COMPARE = "PurchaseGoal_Bubble_Compare";
        String BUBBLE_WISHLIST_ERROR = "WishlistGoal_Bubble_WishlistError";
        String MANDATORY_PERMISSIONS = "Permissions_status";
    }

}
