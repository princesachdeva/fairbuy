package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BuildConfig;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;

/**
 * Created by prince.sachdeva on 13/02/19.
 */

public class AnalyticsManager {

    private static GoogleAnalytics mGoogleAnalytics;
    private static Tracker mTracker;
    private static AnalyticsManager mInstance;

    public static AnalyticsManager getInstance() {
        if (mInstance == null) {
            synchronized (AnalyticsManager.class) {
                if (mInstance == null) {
                    mInstance = new AnalyticsManager();
                }
            }
        }
        initalize();
        return mInstance;
    }

    private AnalyticsManager() {

    }

    public static void initalize() {

        Context context = ShoppingAssistantApplication.getContext();
        if (context == null) {
            return;
        }

        if (mGoogleAnalytics == null) {
            mGoogleAnalytics = GoogleAnalytics.getInstance(context);
            mGoogleAnalytics.setDryRun(false);
        }
        if (mTracker == null) {
            mTracker = mGoogleAnalytics.newTracker(R.xml.global_tracker);
        }

    }

    public void sendEvent(String category, String action, String label) {
        Log.e("TAG", "category---->" + category + "   action ---->" + action + "   label---->" + label);

        if (mTracker != null) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(label)
                    .build());
        }
    }
}
