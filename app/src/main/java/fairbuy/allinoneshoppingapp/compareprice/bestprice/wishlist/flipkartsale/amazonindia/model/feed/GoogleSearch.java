package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed;

import java.util.List;

/**
 * Created by aditya.amartya on 24/10/18
 */


public class GoogleSearch extends BaseModel {

    private List<Item> items;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
