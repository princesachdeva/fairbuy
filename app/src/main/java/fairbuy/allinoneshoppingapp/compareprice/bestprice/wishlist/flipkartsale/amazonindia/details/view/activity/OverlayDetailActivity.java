package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.activity;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.COMPARE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Category.PURCHASE_GOAL;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.COMPARE_BASEPRODUCT;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.COMPARE_RESULT;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.SEARCH_TERM;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.COMPARE_CLOSE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.COMPARE_PRODUCT_VIEWED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.COMPARE_RESULT_COUNT;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.COMPARE_RESULT_COUNT_ZERO;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.PRODUCT_VIEWED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.SEPERATOR;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistItem;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.OverlayDetailsBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.adapter.OverlayViewAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.AnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.GoogleSearch;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Item;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser.ChromeParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.ApiCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.DbCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.ShoppingRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.WishListRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceKeys;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView.AppBarStateChangeListener;

public class OverlayDetailActivity extends Activity implements Observer<GoogleSearch> {
    private OverlayDetailsBinding detailsBinding;
    private String searchString;
    private MutableLiveData<GoogleSearch> liveData;
    private int urlCount;
    private ArrayList<Product> products;
    private Product product;
    private String origin;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int currentViewCount = PreferenceManager.getIntFromPreference(PreferenceKeys.ASSISTANT_DETAIL_PAGE_VIEW_COUNT);
        PreferenceManager.writeIntToPreference(PreferenceKeys.ASSISTANT_DETAIL_PAGE_VIEW_COUNT, currentViewCount + 1);
        Window window = getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), android.R.color.transparent, null));
        detailsBinding = DataBindingUtil.setContentView(this, R.layout.overlay_details);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        if (intent != null) {
            product = intent.getParcelableExtra(Args.PRODUCT);
            searchString = intent.getStringExtra(Args.SEARCH_STRING);
            origin = intent.getStringExtra(Args.ORIGIN);
        }
        double priceInNumericFormat = ChromeParser.getPriceInNumericFormat(product.productPrice);
        AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.COMPARE,
                AnalyticsConstants.Label.COMPARE_SHOW + origin + AnalyticsConstants.SEPERATOR + product.website.toString() + AnalyticsConstants.SEPERATOR + product.productName
                        + AnalyticsConstants.SEPERATOR + priceInNumericFormat);

        Bundle bundle = new Bundle();
        bundle.putString("Store", product.website.toString());
        bundle.putString("Title", product.productName);
        bundle.putString("Placeholder", origin);
        bundle.putInt("Price", (int) priceInNumericFormat);

        FirebaseAnalyticsManager.getInstance().sendEvent(COMPARE_BASEPRODUCT, bundle);


        products = new ArrayList<>();
        if (liveData == null) {
            liveData = new MutableLiveData<>();
        }
        detailsBinding.setVariable(BR.clickListener, clickListener);
        detailsBinding.setVariable(BR.product, product);
        detailsBinding.progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.color_009cff), PorterDuff.Mode.SRC_IN);

        detailsBinding.appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, int state) {
                switch (state) {
                    case State.EXPANDED:
                    case State.IDLE:
                        detailsBinding.setVariable(BR.isToolbarCollapsed, false);
                        break;
                    case State.COLLAPSED:
                        detailsBinding.setVariable(BR.isToolbarCollapsed, true);
                        break;
                }
            }
        });
        if (detailsBinding != null && product != null && !TextUtils.isEmpty(product.getProductName())) {
            detailsBinding.setVariable(BR.status, 1);
            liveData.observeForever(this);
            fetchGoogleSearchResults();
            //ChromeParser.getSuggestionsFromGoogle(title, onProductSuggestionsListener);
        } else {
            Toast.makeText(this, "No products found", Toast.LENGTH_SHORT).show();
            finish();
        }

        FirebaseAnalyticsManager.getInstance().setScreen(this, "Comparison");
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            switch (v.getId()) {
                case R.id.ibDismissOverlay:
                case R.id.ivDismissFromCollapsed:
                    AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, COMPARE, COMPARE_CLOSE);
                    FirebaseAnalyticsManager.getInstance().sendEvent("PurchaseGoal_Compare_Close", null);
                    OverlayDetailActivity.this.finish();
                    break;
                case R.id.cardItem:
                    showProgressBarForDelay();
                    Product product1 = (Product) v.getTag();
                    product1.productOrigin = COMPARE_PRODUCT_VIEWED;
                    ShoppingUtils.launchProductPageInAppSuggestions(v.getContext(), product1, (int) ChromeParser.getPriceInNumericFormat(product.productPrice));
                    break;

                case R.id.ivWishlist:
                    Product product = (Product) v.getTag(R.string.product_tag);
                    final int position = (int) v.getTag(R.string.position_tag);
                    if (product.isWishlisted) {
                        WishListRepository.getInstance(OverlayDetailActivity.this.getApplication()).deleteWishListItem(product.productURL, new DbCallBack() {
                            @Override
                            public void onQueryProcessed(final boolean isSuccess) {
                                v.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isSuccess) {
                                            if (detailsBinding != null && detailsBinding.rvOverlayDetails.getAdapter() != null) {
                                                //detailsBinding.rvOverlayDetails.getAdapter().notifyItemChanged(position);
                                                ((OverlayViewAdapter) detailsBinding.rvOverlayDetails.getAdapter()).updateProductWishlistState(position, false);
                                            }
                                        }
                                    }
                                });
                            }
                        });

                    } else {
                        WishlistItem wishlistItem = new WishlistItem();
                        wishlistItem.setItem(product.productName);
                        wishlistItem.setImageUrl(product.productImage);
                        wishlistItem.setProductUrl(product.productURL);
                        wishlistItem.setShoppingSite(product.webSiteName);
                        wishlistItem.setLastModifiedTime(System.currentTimeMillis());
                        WishListRepository.getInstance(OverlayDetailActivity.this.getApplication()).insertWishListItem(wishlistItem, new DbCallBack() {
                            @Override
                            public void onQueryProcessed(final boolean isSuccess) {
                                v.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isSuccess) {
                                            if (detailsBinding != null && detailsBinding.rvOverlayDetails.getAdapter() != null) {
                                                //detailsBinding.rvOverlayDetails.getAdapter().notifyItemChanged(position);
                                                ((OverlayViewAdapter) detailsBinding.rvOverlayDetails.getAdapter()).updateProductWishlistState(position, true);
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                    break;

                case R.id.tv_facebook:
                    PreferenceManager.writeIntToPreference(PreferenceKeys.ASSISTANT_DETAIL_PAGE_VIEW_COUNT, 0);
                    detailsBinding.setVariable(BR.showBlockView, false);
                    detailsBinding.executePendingBindings();
                    break;
            }
        }
    };

    private void fetchGoogleSearchResults() {
        try {
            //Log.d("GoogleSearch", "https://www.googleapis.com/customsearch/v1?q=" + URLEncoder.encode(searchString, "UTF-8") +
            // "&cx=014691981478614609118:crqlnam1whq&key=AIzaSyCpcK-P7L2Y8gTR5qooCy9DBLBxwLJ5q1A");
            /*Log.d("GoogleSearch", "https://www.googleapis.com/customsearch/v1?q=" + URLEncoder.encode(searchString, "UTF-8") +
            "&cx=006274864137540837670:miaykmfzw6w&key=AIzaSyCXWYW8gqAW98GqnYPf3cpBcgI5_KE2RB0");*/
            new ShoppingRepository(OverlayDetailActivity.this.getApplication()).getSearchResults(
                    "https://www.googleapis.com/customsearch/v1?q=" + URLEncoder.encode(searchString, "UTF-8")
                            + "&cx=006274864137540837670:miaykmfzw6w&key=AIzaSyCXWYW8gqAW98GqnYPf3cpBcgI5_KE2RB0&alt=json", new ApiCallBack<GoogleSearch>() {
                        @Override
                        public void onSuccess(GoogleSearch data) {
                            //detailsBinding.setVariable(BR.status, 0);
                            // detailsBinding.executePendingBindings();
                            if (liveData != null) {
                                liveData.setValue(data);
                                liveData.removeObserver(OverlayDetailActivity.this);
                            }
                        }

                        @Override
                        public void onFail(Throwable throwable) {
                            //detailsBinding.setVariable(BR.status, 0);
                            //detailsBinding.executePendingBindings();
                            if (liveData != null) {
                                liveData.setValue(null);
                                //Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                liveData.removeObserver(OverlayDetailActivity.this);
                            }
                        }
                    });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    private ChromeParser.OnProductPageListener productPageListener = new ChromeParser.OnProductPageListener() {
        int nullProductCount = 0;

        @Override
        public void onProductPage(Product product) {
            if (product == null) {
                nullProductCount++;
            }
            products.add(product);
            //Log.d("googlesearch", product != null ? product.getProductName() : "Product is null");
            if (products.size() == urlCount) {
                if (nullProductCount == urlCount) {
                    AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.COMPARE,
                            COMPARE_RESULT_COUNT_ZERO + OverlayDetailActivity.this.product.productName);
                    detailsBinding.setVariable(BR.errorText, "No results found!");
                    detailsBinding.setVariable(BR.status, 2);
                    sendResponseEvent(0);
                } else {

                    detailsBinding.setVariable(BR.status, 0);
                    products.removeAll(Collections.singleton(null));
                    AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.COMPARE,
                            COMPARE_RESULT_COUNT + products.size() + AnalyticsConstants.SEPERATOR + OverlayDetailActivity.this.product.productName);
                    OverlayViewAdapter overlayViewAdapter = new OverlayViewAdapter(products, clickListener);
                    if (OverlayDetailActivity.this.product != null && !TextUtils.isEmpty(OverlayDetailActivity.this.product.productImage)) {
                        AppBarLayout.LayoutParams layoutParams = (AppBarLayout.LayoutParams) detailsBinding.collapsingToolBar.getLayoutParams();
                        layoutParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED);
                        detailsBinding.collapsingToolBar.setLayoutParams(layoutParams);
                    }
                    detailsBinding.rvOverlayDetails.addItemDecoration(new DividerItemDecoration(OverlayDetailActivity.this, DividerItemDecoration.VERTICAL));
                    detailsBinding.rvOverlayDetails.setAdapter(overlayViewAdapter);
                    sendResponseEvent(products.size());
                }
            }
        }
    };

    @Override
    public void onChanged(@Nullable GoogleSearch googleSearch) {
        if (googleSearch != null && googleSearch.getItems() != null && googleSearch.getItems().size() > 0) {
            //Log.d("GoogleSearch", googleSearch.getItems().size() + "");
            urlCount = googleSearch.getItems().size();
            for (Item item : googleSearch.getItems()) {
                ChromeParser.parseURL(item.getLink(), productPageListener);
            }
        } else {
            //Log.d("GoogleSearch", "No results found");
            if (!Utils.isNetworkAvailable(this)) {
                detailsBinding.setVariable(BR.errorText, "Check your internet connection!");
                detailsBinding.setVariable(BR.status, 2);
            } else {
                AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.COMPARE,
                        COMPARE_RESULT_COUNT_ZERO + product.productName);
                detailsBinding.setVariable(BR.errorText, "No results found!");
                detailsBinding.setVariable(BR.status, 2);
                sendResponseEvent(0);
            }
            detailsBinding.executePendingBindings();
            //Toast.makeText(context, "No results", Toast.LENGTH_SHORT).show();
        }
        /*Uncomment this to show fb block view
        int currentViewCount = PreferenceManager.getIntFromPreference(PreferenceKeys.ASSISTANT_DETAIL_PAGE_VIEW_COUNT);
        if (currentViewCount >= Constants.MAX_VIEW_ALLOWED_COUNT + 1) {
            detailsBinding.setVariable(BR.showBlockView, true);
            detailsBinding.executePendingBindings();
            showTimer();
        }*/
    }


    private void sendResponseEvent(int count) {
        Bundle bundle = new Bundle();
        bundle.putString("Store", product.website.toString());
        bundle.putString("Title", product.productName);
        bundle.putString("Type", "server");
        bundle.putInt("Results", count);

        FirebaseAnalyticsManager.getInstance().sendEvent(COMPARE_RESULT, bundle);
    }

    private void showTimer() {
        new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                detailsBinding.setVariable(BR.counterText, "or wait: " + (millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                detailsBinding.setVariable(BR.showBlockView, false);
            }
        }.start();
    }

    public interface Args {
        String PRODUCT = "product";
        String SEARCH_STRING = "search_string";
        String ORIGIN = "ORIGIN";
    }

    public void showProgressBarForDelay() {
        final ViewGroup viewGroup = ((OverlayDetailsBinding) detailsBinding).progressBarContainer;
        if (viewGroup != null) {
            viewGroup.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (viewGroup != null) {
                        viewGroup.setVisibility(View.GONE);
                        OverlayDetailActivity.this.finish();
                    }
                }
            }, 2000);
        }
    }
}
