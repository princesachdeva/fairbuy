package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ItemSearchBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;

/**
 * Created by aditya.amartya on 13/12/18
 */
public class SearchResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Product> products;
    private View.OnClickListener clickListener;

    public SearchResultAdapter(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setData(ArrayList<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public void clearData() {
        if (products != null) {
            this.products.clear();
            notifyDataSetChanged();
        }
    }

    public void updateProductWishlistState(int position, boolean isWishlist) {
        if (products != null && products.size() > 0) {
            products.get(position).isWishlisted = isWishlist;
            notifyItemChanged(position);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_search, viewGroup, false);
        binding.setVariable(BR.clickListener, clickListener);
        return new SearchViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        SearchViewHolder holder = (SearchViewHolder) viewHolder;
        final Product product = products.get(position);
        if (product != null) {
            product.setProductOrigin("Search");
            setViewHeight(holder, false);
            holder.binding.setVariable(BR.product, product);
            holder.binding.setVariable(BR.drawableId, product.getAppIcon());
            ((ItemSearchBinding) holder.binding).fabWishlistProduct.setTag(R.string.product_tag, product);
            ((ItemSearchBinding) holder.binding).fabWishlistProduct.setTag(R.string.position_tag, position);
            ((ItemSearchBinding) holder.binding).fabWishlistProduct.setImageDrawable(
                    ContextCompat.getDrawable(holder.binding.getRoot().getContext(), product.isWishlisted ? R.drawable.ic_wishlisted : R.drawable.ic_wishlist));
            holder.binding.executePendingBindings();
        } else {
            setViewHeight(holder, true);
        }
    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    private static class SearchViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public SearchViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void setViewHeight(RecyclerView.ViewHolder holder, boolean hide) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) (holder.itemView.getLayoutParams());
        layoutParams.height = hide ? 0 : ViewGroup.LayoutParams.WRAP_CONTENT;
        holder.itemView.setLayoutParams(layoutParams);
    }
}
