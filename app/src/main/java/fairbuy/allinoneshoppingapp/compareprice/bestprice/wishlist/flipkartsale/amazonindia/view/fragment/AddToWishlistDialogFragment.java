package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;


/**
 * Created by prince.sachdeva on 27/12/18.
 */

public class AddToWishlistDialogFragment extends BottomSheetDialogFragment {

    public static AddToWishlistDialogFragment newInstance() {
        return new AddToWishlistDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_to_wishlist_dialog, container,
                false);

        // get the views and attach the listener

        return view;
    }
}
