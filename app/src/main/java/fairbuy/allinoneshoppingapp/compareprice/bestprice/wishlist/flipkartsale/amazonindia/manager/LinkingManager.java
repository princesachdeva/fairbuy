package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.navigation.Navigation;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.AppUpdateActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.FairBuyActivity;

/**
 * Created by prince.sachdeva on 12/02/19.
 */

public class LinkingManager {

    private String mUriPath = null;
    private Intent sourceIntent;
    private Navigation mNavigation;


    public static final String SCHEME_COMPARE = "compare/";
    public static final String SCHEME_SEARCH = "search/";
    public static final String SCHEME_WISHLIST = "wishlist";
    public static final String SCHEME_HISTORY = "history";
    public static final String SCHEME_ASK_PERMISSION = "permission";
    public final static String SCHEME_FAIRBUYAPP = "fairbuyapp://";

    public boolean handleDeepLinkingSupport(Context context, Intent intent) {
        if (mNavigation == null) {
            mNavigation = new Navigation();
        }
        this.mUriPath = null;
        sourceIntent = intent;
        Uri schemeUri = intent.getData();
        Bundle bundle = intent.getExtras();
        if (null != schemeUri) {
            return handleDeepLinkingSupport(context, schemeUri.toString());
        }
        // handleNotificationClick
        String deeplink = intent.getStringExtra("deeplink");
        if (!TextUtils.isEmpty(deeplink)) {
            return handleDeepLinkingSupport(context, deeplink);
        }
        return handleDeepLinkingSupport(context);
    }

    public boolean handleDeepLinkingSupport(Context context, String url) {
        if (mNavigation == null) {
            mNavigation = new Navigation();
        }

        this.mUriPath = url;
        return handleDeepLinkingSupport(context);
    }

    private boolean handleDeepLinkingSupport(Context context) {
        if (mNavigation == null) {
            mNavigation = new Navigation();
        }

        if (mUriPath != null) {
            if (mUriPath.startsWith(SCHEME_FAIRBUYAPP)) {
                if (mUriPath.contains(SCHEME_COMPARE)) {
                    String[] array = mUriPath.split(SCHEME_COMPARE);
                    if (array.length >= 2) {
                        launchHomeScreen(context, R.id.Compare, getDecodedString(array[1]));
                        return true;
                    }
                } else if (mUriPath.contains(SCHEME_SEARCH)) {
                    String[] array = mUriPath.split(SCHEME_SEARCH);
                    if (array != null && array.length >= 2) {
                        launchHomeScreen(context, R.id.Search, getDecodedString(array[1]));
                        return true;
                    } else {
                        launchHomeScreen(context, R.id.Search);
                    }
                } else if (mUriPath.contains(SCHEME_WISHLIST)) {
                    launchHomeScreen(context, R.id.Wishlist);
                } else if (mUriPath.contains(SCHEME_HISTORY)) {
                    launchHomeScreen(context, R.id.History);
                }else if (mUriPath.contains(SCHEME_ASK_PERMISSION)) {
                    launchHomeScreen(context, R.id.Permission);
                }
                else {
                    launchHomeScreen(context, R.id.Home);
                }
            }
        } else {
            launchHomeScreen(context, R.id.Home);
        }
        return true;
    }

    private void launchHomeScreen(Context context, int deepLinkingScreenId) {
        if (mNavigation == null) {
            mNavigation = new Navigation();
        }
        mNavigation.setMenuId(deepLinkingScreenId);
        launchHome(context);
    }

    private void launchHomeScreen(Context context, int deepLinkingScreenId, final String param) {
        if (mNavigation == null) {
            mNavigation = new Navigation();
        }
        mNavigation.setMenuId(deepLinkingScreenId);
        mNavigation.setParams(new ArrayList<String>() {{
            add(param);
        }});
        launchHome(context);
    }


    private void launchHome(Context context) {
        Intent intent = new Intent(context, FairBuyActivity.class);
        intent.putExtra(FairBuyActivity.NAVIAGTION, mNavigation);
        context.startActivity(intent);
    }

    private String getDecodedString(String encodedString) {
        if (!TextUtils.isEmpty(encodedString)) {
            try {
                return URLDecoder.decode(encodedString, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }
        }
        return encodedString;
    }
}
