package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView;

import android.content.Context;
import android.view.KeyEvent;
import android.widget.LinearLayout;

/**
 * Created by aditya.amartya on 03/11/18
 */
public class OverlayLayout extends LinearLayout {

    private BaseOverlayView overlayView;

    public OverlayLayout(Context context, BaseOverlayView overlayView) {
        super(context);
        this.overlayView = overlayView;
        //this.setFocusable(true);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        //Log.d("OverlayFocus", event.getAction() + "");
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0) {
                getKeyDispatcherState().startTracking(event, this);
                return true;

            } else if (event.getAction() == KeyEvent.ACTION_UP) {
                getKeyDispatcherState().handleUpEvent(event);

                if (event.isTracking() && !event.isCanceled()) {
                    // dismiss your window:
                    overlayView.hide();

                    return true;
                }
            }
        }

        return super.dispatchKeyEvent(event);
    }
}
