package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.OverlayDetailsBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.adapter.OverlayViewAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistItem;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.GoogleSearch;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Item;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser.ChromeParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.ApiCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.DbCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.ShoppingRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.WishListRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceKeys;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;

/**
 * Created by aditya.amartya on 03/11/18
 */
public class OverlayWindow extends BaseOverlayView implements Observer<GoogleSearch> {
    //private WindowManager windowManager;
    private WindowManager.LayoutParams layoutParams;
    //private OverlayLayout myLayout;
    //private View view;
    private OverlayDetailsBinding detailsBinding;
    private String searchString;
    private Application context;
    private MutableLiveData<GoogleSearch> liveData;
    private int urlCount;
    private ArrayList<Product> products;
    private Product product;
    private int nullProductCount;

    /**
     * Overlay screen for list page
     *
     * @param context
     * @param product      Product data
     * @param searchString search query string for google search api
     */
    public OverlayWindow(Application context, Product product, String searchString) {
        super(context, R.layout.overlay_details);
        int currentViewCount = PreferenceManager.getIntFromPreference(PreferenceKeys.ASSISTANT_DETAIL_PAGE_VIEW_COUNT);
        PreferenceManager.writeIntToPreference(PreferenceKeys.ASSISTANT_DETAIL_PAGE_VIEW_COUNT, currentViewCount + 1);
        this.context = context;
        this.product = product;
        this.searchString = searchString;
        products = new ArrayList<>();
        if (liveData == null) {
            liveData = new MutableLiveData<>();
        }
        layoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                Build.VERSION.SDK_INT < Build.VERSION_CODES.O ? WindowManager.LayoutParams.TYPE_SYSTEM_ALERT : WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        layoutParams.verticalMargin = -100;
        /*windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //view = inflater.inflate(R.layout.overlay_details, null);


        myLayout = new OverlayLayout(context, this);
        //myLayout.setBackgroundColor(Color.WHITE);

        detailsBinding = DataBindingUtil.inflate(inflater, R.layout.overlay_details, myLayout, true);*/
        if (dataBinding instanceof OverlayDetailsBinding) {
            detailsBinding = (OverlayDetailsBinding) dataBinding;
            detailsBinding.setVariable(BR.clickListener, clickListener);
            detailsBinding.setVariable(BR.product, product);
        }
        //myLayout.addView(detailsBinding.getRoot(), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            switch (v.getId()) {
                case R.id.ibDismissOverlay:
                    hide();
                    break;
                case R.id.cardItem:
                    hide();
                    ShoppingUtils.launchProductPage(context, v.getTag().toString());
                    break;

                case R.id.ivWishlist:
                    Product product = (Product) v.getTag(R.string.product_tag);
                    final int position = (int) v.getTag(R.string.position_tag);
                    if (product.isWishlisted) {
                        WishListRepository.getInstance(context).deleteWishListItem(product.productURL, new DbCallBack() {
                            @Override
                            public void onQueryProcessed(final boolean isSuccess) {
                                v.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isSuccess) {
                                            if (detailsBinding != null && detailsBinding.rvOverlayDetails.getAdapter() != null) {
                                                //detailsBinding.rvOverlayDetails.getAdapter().notifyItemChanged(position);
                                                ((OverlayViewAdapter) detailsBinding.rvOverlayDetails.getAdapter()).updateProductWishlistState(position, false);
                                            }
                                        }
                                    }
                                });
                            }
                        });

                    } else {
                        WishlistItem wishlistItem = new WishlistItem();
                        wishlistItem.setItem(product.productName);
                        wishlistItem.setImageUrl(product.productImage);
                        wishlistItem.setProductUrl(product.productURL);
                        wishlistItem.setShoppingSite(product.webSiteName);
                        wishlistItem.setLastModifiedTime(System.currentTimeMillis());
                        WishListRepository.getInstance(context).insertWishListItem(wishlistItem, new DbCallBack() {
                            @Override
                            public void onQueryProcessed(final boolean isSuccess) {
                                v.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isSuccess) {
                                            if (detailsBinding != null && detailsBinding.rvOverlayDetails.getAdapter() != null) {
                                                //detailsBinding.rvOverlayDetails.getAdapter().notifyItemChanged(position);
                                                ((OverlayViewAdapter) detailsBinding.rvOverlayDetails.getAdapter()).updateProductWishlistState(position, true);
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                    break;

                case R.id.tv_facebook:
                    PreferenceManager.writeIntToPreference(PreferenceKeys.ASSISTANT_DETAIL_PAGE_VIEW_COUNT, 0);
                    detailsBinding.setVariable(BR.showBlockView, false);
                    detailsBinding.executePendingBindings();
                    break;
            }
        }
    };


    public void show() {
        showView(layoutParams);
        if (detailsBinding != null && product != null && !TextUtils.isEmpty(product.getProductName())) {
            detailsBinding.setVariable(BR.status, 1);
            liveData.observeForever(OverlayWindow.this);
            fetchGoogleSearchResults();
            //ChromeParser.getSuggestionsFromGoogle(title, onProductSuggestionsListener);
        } else {
            Toast.makeText(context, "No products found", Toast.LENGTH_SHORT).show();
            hide();
        }
    }

    private void fetchGoogleSearchResults() {
        try {
            //Log.d("GoogleSearch", "https://www.googleapis.com/customsearch/v1?q=" + URLEncoder.encode(searchString, "UTF-8") + "&cx=014691981478614609118:crqlnam1whq&key=AIzaSyCpcK-P7L2Y8gTR5qooCy9DBLBxwLJ5q1A");
            /*Log.d("GoogleSearch", "https://www.googleapis.com/customsearch/v1?q=" + URLEncoder.encode(searchString, "UTF-8") + "&cx=006274864137540837670:miaykmfzw6w&key=AIzaSyCXWYW8gqAW98GqnYPf3cpBcgI5_KE2RB0");*/
            new ShoppingRepository(context).getSearchResults(
                    "https://www.googleapis.com/customsearch/v1?q=" + URLEncoder.encode(searchString, "UTF-8")
                            + "&cx=006274864137540837670:miaykmfzw6w&key=AIzaSyCXWYW8gqAW98GqnYPf3cpBcgI5_KE2RB0&alt=json", new ApiCallBack<GoogleSearch>() {
                        @Override
                        public void onSuccess(GoogleSearch data) {
                            //detailsBinding.setVariable(BR.status, 0);
                            // detailsBinding.executePendingBindings();
                            liveData.setValue(data);
                            liveData.removeObserver(OverlayWindow.this);
                        }

                        @Override
                        public void onFail(Throwable throwable) {
                            //detailsBinding.setVariable(BR.status, 0);
                            //detailsBinding.executePendingBindings();
                            liveData.setValue(null);
                            //Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            liveData.removeObserver(OverlayWindow.this);
                        }
                    });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    private ChromeParser.OnProductPageListener productPageListener = new ChromeParser.OnProductPageListener() {

        @Override
        public void onProductPage(Product product) {
            if (product == null) {
                nullProductCount++;
            }
            products.add(product);
            //Log.d("googlesearch", product != null ? product.getProductName() : "Product is null");
            if (products.size() == urlCount) {
                if (nullProductCount == urlCount) {
                    detailsBinding.setVariable(BR.errorText, "No results found!");
                    detailsBinding.setVariable(BR.status, 2);
                } else {
                    detailsBinding.setVariable(BR.status, 0);
                    OverlayViewAdapter overlayViewAdapter = new OverlayViewAdapter(products, clickListener);
                    detailsBinding.rvOverlayDetails.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
                    detailsBinding.rvOverlayDetails.setAdapter(overlayViewAdapter);
                }
            }
        }
    };

    @Override
    public void onChanged(@Nullable GoogleSearch googleSearch) {
        if (googleSearch != null && googleSearch.getItems() != null && googleSearch.getItems().size() > 0) {
            //Log.d("GoogleSearch", googleSearch.getItems().size() + "");
            urlCount = googleSearch.getItems().size();
            nullProductCount = 0;
            for (Item item : googleSearch.getItems()) {
                Log.d("GoogleSearch", item.getLink());
                ChromeParser.parseURL(item.getLink(), productPageListener);
            }
        } else {
            //Log.d("GoogleSearch", "No results found");
            if (!Utils.isNetworkAvailable(context)) {
                detailsBinding.setVariable(BR.errorText, "Check your internet connection!");
                detailsBinding.setVariable(BR.status, 2);
            } else {
                detailsBinding.setVariable(BR.errorText, "No results found!");
                detailsBinding.setVariable(BR.status, 2);
            }
            detailsBinding.executePendingBindings();
            //Toast.makeText(context, "No results", Toast.LENGTH_SHORT).show();
        }
        /*Uncomment this to show fb block view
        int currentViewCount = PreferenceManager.getIntFromPreference(PreferenceKeys.ASSISTANT_DETAIL_PAGE_VIEW_COUNT);
        if (currentViewCount >= Constants.MAX_VIEW_ALLOWED_COUNT + 1) {
            detailsBinding.setVariable(BR.showBlockView, true);
            detailsBinding.executePendingBindings();
            showTimer();
        }*/
    }

    private void showTimer() {
        new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                detailsBinding.setVariable(BR.counterText, "or wait: " + (millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                detailsBinding.setVariable(BR.showBlockView, false);
            }
        }.start();
    }
}
