package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.VideoView;

import com.android.databinding.library.baseAdapters.BR;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentHomeMyOrdersBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentHomeTutorialBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceKeys;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;

/**
 * Created by prince.sachdeva on 20/02/19.
 */

public class HomeTutorialFragment extends BaseFragment implements View.OnClickListener {
    private VideoView videoHolder;
    private int removedCount;

    @Override
    protected int getResourceId() {
        return R.layout.fragment_home_tutorial;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (videoHolder != null) {
            videoHolder.seekTo(1000);
            videoHolder.start();
        }
    }

    private void initView() {
        removedCount = PreferenceManager.getIntFromPreference(PreferenceKeys.HOME_TUTORIAL_VIEW_REMOVED_COUNT);
        if (removedCount > 2) {
            hideView();
            return;
        }

        binding.setVariable(BR.clickListener, this);
        videoHolder = ((FragmentHomeTutorialBinding) binding).videoHolder;
        Uri video = Uri.parse("android.resource://" + getContext().getPackageName() + "/" + R.raw.home_tutorial_mute);
        videoHolder.setVideoURI(video);
        videoHolder.seekTo(1);
        videoHolder.start();
        videoHolder.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });


        videoHolder.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                return false;
            }
        });
        /*videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                videoHolder.start();
            }
        });*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibDismiss:
                hideView();
                PreferenceManager.writeIntToPreference(PreferenceKeys.HOME_TUTORIAL_VIEW_REMOVED_COUNT, ++removedCount);
                break;
            case R.id.buttonDemo:
                ShoppingUtils.launchProductPageInDemo(v.getContext(),
                        "https://www.amazon.in/Redmi-Note-Pro-Black-Storage/dp/B07D856BVR",
                        "Mi Redmi Note 5 Pro (Gold, 64 GB, 4 GB RAM)");
                break;
        }

    }

    private void hideView() {
        ((FragmentHomeTutorialBinding) binding).rlParentContainer.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (videoHolder != null) {
            videoHolder.pause();
        }
    }
}
