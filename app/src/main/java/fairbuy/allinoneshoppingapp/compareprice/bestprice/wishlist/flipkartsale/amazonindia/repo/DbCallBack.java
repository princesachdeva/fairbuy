package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo;

/**
 * Created by aditya.amartya on 12/12/18
 */
public interface DbCallBack {
    void onQueryProcessed(boolean isSuccess);
}
