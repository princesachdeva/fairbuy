package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper;

import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;

/**
 * Created by prince.sachdeva on 27/12/18.
 */

public class WishlistHelper {

    public static void showWishlistHelperDialog(ArrayList<Product> productArrayList, Context context) {
        /*if (productArrayList == null || productArrayList.size() == 0) {
            return;
        }*/

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View sheetView = layoutInflater.inflate(R.layout.order_detail_message, null);
        bottomSheetDialog.setContentView(sheetView);

        bottomSheetDialog.show();

    }
}
