package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;

public class FabUtilNative {
    private Context context;
    private View parent, view;
    private OnFabClickListener onFabClickListener;
    private ViewGroup layoutSrpActions, container;
    private int fabItemParentId, detailsContainerId;

    /**
     * @param parent             - Parent view for inflating layout
     * @param resource           - layout resource, which we have to inflate
     * @param onFabClickListener - listener to provide Callback for click actions on inflated layout
     * @param fabItemParentId    - resource id of parent of current child items
     * @param fabCloseId         - resource id of close button
     */
    public FabUtilNative(View parent, int resource, OnFabClickListener onFabClickListener, int fabItemParentId, int fabCloseId, int detailsContainerId) {
        this.context = parent.getContext();
        this.parent = parent;
        this.onFabClickListener = onFabClickListener;
        this.fabItemParentId = fabItemParentId;
        this.detailsContainerId = detailsContainerId;
        inflateView(parent, resource, fabItemParentId, fabCloseId);
    }

    public void changeWishlistIcon(boolean isWishlisted, int wishlistId) {
        if (view != null) {
            ImageView fabWishlist = view.findViewById(wishlistId);
            fabWishlist.setImageResource(isWishlisted ? R.drawable.pop_up_wishlisted : R.drawable.pop_up_wishlist);
        }
    }

    private void inflateView(View parent, int resource, int fabItemParentId, int fabCloseId) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        animation.setInterpolator(new OvershootInterpolator());

        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(resource, (ViewGroup) parent);


        container = parent.findViewById(detailsContainerId);
        layoutSrpActions = parent.findViewById(fabItemParentId);
        ImageView fabClose = parent.findViewById(fabCloseId);

        FabItemClickListener fabItemClickListener = new FabItemClickListener();

        //fabClose.setOnClickListener(fabItemClickListener);

        layoutSrpActions.setOnClickListener(fabItemClickListener);
        layoutSrpActions.setAnimation(animation);

        for (int i = 0; i < layoutSrpActions.getChildCount(); i++) {
            View tempView = layoutSrpActions.getChildAt(i);
            if (tempView instanceof LinearLayout) {
                LinearLayout imageView = (LinearLayout) tempView;
                imageView.setOnClickListener(fabItemClickListener);
            }
        }
    }

    public interface OnFabClickListener {
        void onFabClick(View view);
    }

    private class FabItemClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id != fabItemParentId) {
                onFabClickListener.onFabClick(v);
            }
        }
    }

    public void removeFabItems() {
        Animation upDownAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_down);
        layoutSrpActions.setAnimation(upDownAnimation);
        ((ViewGroup) parent).removeView(container);
    }
}