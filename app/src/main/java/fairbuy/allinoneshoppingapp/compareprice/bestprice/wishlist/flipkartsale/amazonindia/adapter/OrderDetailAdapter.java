package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ItemOrderDetailBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ItemOrderDetailHeadingBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.Status;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.DateTimeUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView.TimelineView;

/**
 * Created by prince.sachdeva on 09/12/18.
 */

public class OrderDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_DISPLAY_DATA = 0;
    private final View.OnClickListener onClickListener;
    private ArrayList<Status> mStatusArrayList;
    private int viewType = VIEW_TYPE_DISPLAY_DATA;
    private String title;
    private int VIEW_TYPE_HEADER = -1;

    public OrderDetailAdapter(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void updateData(SmsProductData smsProductData) {
        if (this.mStatusArrayList == null) {
            this.mStatusArrayList = new ArrayList<>();
        } else {
            this.mStatusArrayList.clear();
        }
        if (smsProductData == null || smsProductData.getStatus() == null || smsProductData.getStatus().size() == 0) {
            return;
        }
        title = smsProductData.getProductName();
        int start = getItemCount();

        this.mStatusArrayList.addAll(smsProductData.getStatus());
       // notifyDataSetChanged();
         notifyItemRangeChanged(start, smsProductData.getStatus().size());
    }

    public void clearData() {
        if (mStatusArrayList != null) {
            mStatusArrayList.clear();
        }
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_HEADER) {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_order_detail_heading, viewGroup, false);
            return new OrderDetailHeadingViewHolder(binding);
        } else {
            ItemOrderDetailBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_order_detail, viewGroup, false);
            binding.timeLineView.initLine(viewType);
            binding.detailView.setOnClickListener(onClickListener);
            return new OrderDetailItemViewHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof OrderDetailHeadingViewHolder) {
            ((ItemOrderDetailHeadingBinding) ((OrderDetailHeadingViewHolder) viewHolder).binding).setVariable(BR.title, title);
        } else {
            Status status = getItem(position);
            OrderDetailItemViewHolder orderDetailItemViewHolder = (OrderDetailItemViewHolder) viewHolder;
            ItemOrderDetailBinding binding = (ItemOrderDetailBinding) orderDetailItemViewHolder.binding;
            binding.timeLineView.setMarker(Utils.getDrawable(((OrderDetailItemViewHolder) viewHolder).binding.getRoot().getContext(), R.drawable.ic_marker, R.color.color_009B2C));
            binding.detailView.setTag(status);
            binding.setVariable(BR.orderStatus, status.getStatus());
            binding.setVariable(BR.orderStatusTime, DateTimeUtils.getFormattedDate(status.getStatusDate(), DateTimeUtils.DATE_FORMAT_ORDER_DETAIL_PAGE));
        }
    }

    private Status getItem(int position) {
        return mStatusArrayList != null && mStatusArrayList.size() > 0 ? mStatusArrayList.get(mStatusArrayList.size() - position) : new Status();
    }

    @Override
    public int getItemCount() {

        return mStatusArrayList != null ? mStatusArrayList.size() + 1 : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == VIEW_TYPE_DISPLAY_DATA) {
            if (position == 0) {
                return VIEW_TYPE_HEADER;
            }
            return TimelineView.getTimeLineViewType(position - 1, getItemCount() - 1);
        }
        return 0;
    }

    public static class OrderDetailHeadingViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public OrderDetailHeadingViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class OrderDetailItemViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public OrderDetailItemViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
