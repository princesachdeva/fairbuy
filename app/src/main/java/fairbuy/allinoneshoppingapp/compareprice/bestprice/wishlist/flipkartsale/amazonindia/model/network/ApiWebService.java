package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.network;

/**
 * Created by aditya.amartya on 24/10/18
 */
public class ApiWebService {
    private static ApiInterface webService = null;

    public static ApiInterface getApiService() {
        if (webService == null) {
            webService = ApiClient.getClient().create(ApiInterface.class);
        }
        return webService;
    }
}
