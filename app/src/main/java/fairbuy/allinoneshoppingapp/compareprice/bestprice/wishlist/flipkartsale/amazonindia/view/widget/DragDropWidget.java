package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistItem;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.activity.OverlayDetailActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.activity.WishlistConfirmationActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.AnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser.ChromeParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.DbCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.WishListRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView.OverlayFbIconView;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView.OverlayWindow;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.WINDOW_SERVICE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.BUBBLE_CLICK;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.BUBBLE_COMPARE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.BUBBLE_WISHLIST_ERROR;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.CHROME_BROWSER;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.NATIVE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.SYSTEM_DIALOG_REASON_HOME_KEY;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.SYSTEM_DIALOG_REASON_KEY;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.SYSTEM_DIALOG_REASON_RECENT_APPS;

public class DragDropWidget {
    private static volatile DragDropWidget instance;
    private View view, container;
    private FabUtil fabUtil;
    private Application application;
    private Product mProduct, deducedProductForNativeView;
    private OverlayWindow overlayWindow;
    private String searchString;
    private boolean isWishlisted, isFabVisible;
    private Context context;
    private ArrayList<Product> deducedProductsForNativeView;
    private View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            if (container != null) {
                container.setVisibility(View.VISIBLE);
            }
            ClipData clipData = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                view.startDragAndDrop(clipData, shadowBuilder, v, 0);
            } else {
                v.startDrag(clipData, shadowBuilder, v, 0);
            }
            v.setVisibility(View.INVISIBLE);
            return true;
        }
    };
    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction();
            if (action == MotionEvent.ACTION_DOWN) {
                if (container != null) {
                    container.setVisibility(View.VISIBLE);
                }
                ClipData clipData = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                v.startDrag(clipData, shadowBuilder, v, 0);
                v.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    };
    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.myLayout:

                    fabUtil = new FabUtil(application, R.layout.view_overlay_options, onFabClickListener, R.id.ll_overlay_actions, R.id.fab_fb);
                    v.setVisibility(View.GONE);
                    v.setOnClickListener(null);
                    if (mProduct != null && !mProduct.isNative) {
                        //For chrome
                        sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.BUBBLE,
                                AnalyticsConstants.Label.BUBBLE_CLICK + mProduct.website.toString() + AnalyticsConstants.SEPERATOR + CHROME_BROWSER
                                        + AnalyticsConstants.SEPERATOR + mProduct.productName);


                        fabUtil.changeWishlistIcon(isWishlisted, R.id.fab_wishlist);


                        Bundle bundle = new Bundle();
                        bundle.putString("Store", mProduct.website.toString());
                        bundle.putString("Title", mProduct.productName);
                        bundle.putString("Placeholder", CHROME_BROWSER);
                        bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(mProduct.productPrice));

                        FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_CLICK, bundle);

                    } else {
                        //native
                        if (deducedProductForNativeView != null) {
                            sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.BUBBLE,
                                    AnalyticsConstants.Label.BUBBLE_CLICK + mProduct.website.toString() + AnalyticsConstants.SEPERATOR + AnalyticsConstants.Label.NATIVE + AnalyticsConstants.SEPERATOR
                                            + mProduct.productName);
                            fabUtil.changeWishlistIcon(isWishlisted, R.id.fab_wishlist);


                            Bundle bundle = new Bundle();
                            bundle.putString("Store", mProduct.website.toString());
                            bundle.putString("Title", mProduct.productName);
                            bundle.putString("Placeholder", AnalyticsConstants.Label.NATIVE);
                            bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(mProduct.productPrice));

                            FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_CLICK, bundle);
                        }
                    }

                    break;
            }
        }
    };
    private FabUtil.OnFabClickListener onFabClickListener = new FabUtil.OnFabClickListener() {
        @Override
        public void onFabClick(final View view) {
            switch (view.getId()) {
                case R.id.llCompare:
                    fabUtil.removeFabItems();
                    if (mProduct != null && !TextUtils.isEmpty(mProduct.productName) && !TextUtils.isEmpty(searchString)) {
                        Product product = new Product();
                        product.productName = mProduct.productName;
                        product.productBrand = mProduct.productBrand;
                        product.productPrice = mProduct.productPrice;
                        product.website = mProduct.website;
                        if (product.website == null || TextUtils.isEmpty(product.website.name())) {
                            product.website = ShoppingUtils.WEBSITE.TEMP;
                        }
                        if (mProduct != null && !TextUtils.isEmpty(mProduct.productImage)) {
                            product.productImage = mProduct.productImage;
                        }
                        Intent intent = new Intent(context, OverlayDetailActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.putExtra(OverlayDetailActivity.Args.PRODUCT, product);
                        intent.putExtra(OverlayDetailActivity.Args.SEARCH_STRING, searchString);
                        intent.putExtra(OverlayDetailActivity.Args.ORIGIN, "Bubble");
                        application.startActivity(intent);


                        if (mProduct != null && !mProduct.isNative) {
                            //
                            sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.BUBBLE,
                                    AnalyticsConstants.Label.BUBBLE_COMPARE + mProduct.website.toString() + AnalyticsConstants.SEPERATOR + CHROME_BROWSER
                                            + AnalyticsConstants.SEPERATOR + mProduct.productName);


                            Bundle bundle = new Bundle();
                            bundle.putString("Store", mProduct.website.toString());
                            bundle.putString("Title", mProduct.productName);
                            bundle.putString("Placeholder", CHROME_BROWSER);
                            bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(mProduct.productPrice));

                            FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_COMPARE, bundle);

                        } else {
                            if (deducedProductForNativeView != null) {
                                if (deducedProductForNativeView.website == null || TextUtils.isEmpty(deducedProductForNativeView.website.name())) {
                                    deducedProductForNativeView.website = ShoppingUtils.WEBSITE.TEMP;
                                }
                                sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.BUBBLE,
                                        AnalyticsConstants.Label.BUBBLE_COMPARE + deducedProductForNativeView.website.toString() + AnalyticsConstants.SEPERATOR
                                                + AnalyticsConstants.Label.NATIVE
                                                + AnalyticsConstants.SEPERATOR + deducedProductForNativeView.productName);

                                Bundle bundle = new Bundle();
                                bundle.putString("Store", deducedProductForNativeView.website.toString());
                                bundle.putString("Title", deducedProductForNativeView.productName);
                                bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(deducedProductForNativeView.productPrice));
                                bundle.putString("Placeholder", NATIVE);


                                FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_COMPARE, bundle);
                            }
                        }

                        //new DetailDialog(application).show();
                        //overlayWindow = new OverlayWindow(application, product, searchString);
                        //overlayWindow.show();
                        removeDragAndDropViews();
                    } else {
                        //Toast.makeText(getApplicationContext(), "Please Scroll", //Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.fab_share:
                    fabUtil.removeFabItems();
                    showDragDropWidget();
                    break;
                case R.id.llWishlist:
                    if (mProduct != null && !mProduct.isNative) {
                        //For chrome and inapp
                        if (isWishlisted) {
                            removeFromWishlist(view, mProduct);
                        } else {
                            addToWishlist(view, mProduct);
                        }
                        if (mProduct.website == null) {
                            mProduct.website = ShoppingUtils.WEBSITE.TEMP;
                        }
                        sendEvent(AnalyticsConstants.Category.WISHLISST_GOAL, AnalyticsConstants.Action.BUBBLE,
                                AnalyticsConstants.Label.BUBBLE_WISHLIST + mProduct.website.toString() + AnalyticsConstants.SEPERATOR + CHROME_BROWSER
                                        + AnalyticsConstants.SEPERATOR + mProduct.productName);

                    } else {
                        //native
                        if (deducedProductForNativeView != null) {
                            if (mProduct.website == null) {
                                mProduct.website = ShoppingUtils.WEBSITE.TEMP;
                            }
                            if (deducedProductForNativeView.website == null) {
                                deducedProductForNativeView.website = ShoppingUtils.WEBSITE.TEMP;
                            }

                            if (isWishlisted) {
                                removeFromWishlist(view, deducedProductForNativeView);
                            } else {
                                addToWishlist(view, deducedProductForNativeView);
                            }
                            sendEvent(AnalyticsConstants.Category.WISHLISST_GOAL, AnalyticsConstants.Action.BUBBLE,
                                    AnalyticsConstants.Label.BUBBLE_WISHLIST + deducedProductForNativeView.website.toString() + AnalyticsConstants.SEPERATOR + AnalyticsConstants.Label.NATIVE
                                            + AnalyticsConstants.SEPERATOR + deducedProductForNativeView.productName);


                        } else if (deducedProductsForNativeView != null && deducedProductsForNativeView.size() > 0) {
                            removeDragAndDropViews();
                            startWishlistConfirmationActivity();
                            sendEvent(AnalyticsConstants.Category.WISHLISST_GOAL, AnalyticsConstants.Action.BUBBLE,
                                    AnalyticsConstants.Label.BUBBLE_WISHLIST + deducedProductsForNativeView.get(0) != null && deducedProductsForNativeView.get(0).website != null
                                            ? deducedProductsForNativeView.get(0).website.toString() : "NA" + AnalyticsConstants.SEPERATOR + AnalyticsConstants.Label.NATIVE
                                            + AnalyticsConstants.SEPERATOR + deducedProductForNativeView.productName);
                        } else {
                            Toast.makeText(context, "Please reload the Page", Toast.LENGTH_SHORT).show();
                            String website = mProduct != null && mProduct.website != null ? mProduct.website.toString() : "NA";
                            String name = mProduct != null && !TextUtils.isEmpty(mProduct.productName) ? mProduct.productName : searchString;
                            sendEvent(AnalyticsConstants.Category.WISHLISST_GOAL, AnalyticsConstants.Action.BUBBLE,
                                    AnalyticsConstants.Label.BUBBLE_WISHLIST_ERROR + website + AnalyticsConstants.SEPERATOR
                                            + AnalyticsConstants.Label.NATIVE
                                            + AnalyticsConstants.SEPERATOR + name);

                            Bundle bundle = new Bundle();
                            bundle.putString("Store", website);
                            bundle.putString("Title", name);
                            bundle.putString("Placeholder", NATIVE);


                            FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_WISHLIST_ERROR, bundle);
                        }
                    }
                    break;
                case R.id.llFb:
                    fabUtil.removeFabItems();
                    showDragDropWidget();
                    break;
            }
        }
    };

    private void showDragDropWidget() {
        if (DragDropWidget.this.view != null) {
            DragDropWidget.this.view.setOnClickListener(clickListener);
            DragDropWidget.this.view.setVisibility(View.VISIBLE);
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!TextUtils.isEmpty(action) && action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
                if (reason != null) {
                    if (reason.equals(SYSTEM_DIALOG_REASON_HOME_KEY) || reason.equals(SYSTEM_DIALOG_REASON_RECENT_APPS)) {
                        removeDragAndDropViews();
                        if (overlayWindow != null) {
                            overlayWindow.hide();
                        }
                    }
                }
            }
        }
    };

    private DragDropWidget(Context context) {
        this.context = context;
        this.application = (Application) context.getApplicationContext();
    }

    public static DragDropWidget getInstance(Context application) {
        DragDropWidget localInstance = instance;
        if (localInstance == null) {
            synchronized (DragDropWidget.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new DragDropWidget(application);
                }
            }
        }
        return localInstance;
    }

    private void startWishlistConfirmationActivity() {
        Intent intent = new Intent(context, WishlistConfirmationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtra(WishlistConfirmationActivity.Args.PRODUCTS, deducedProductsForNativeView);
        context.startActivity(intent);
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

    public void setDeducedProductForNativeView(final Product product) {
        deducedProductForNativeView = product;
        if (fabUtil != null && deducedProductForNativeView != null && !TextUtils.isEmpty(product.productURL)) {
            WishListRepository.getInstance(application).isWishlisted(product.productURL, new DbCallBack() {
                @Override
                public void onQueryProcessed(boolean isSuccess) {
                    isWishlisted = isSuccess;
                    fabUtil.changeWishlistIcon(isWishlisted, R.id.fab_wishlist);
                }
            });
        }
    }

    public void setDeducedProductsForNativeView(final ArrayList<Product> products) {
        this.deducedProductsForNativeView = products;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public OverlayWindow getOverlayWindow() {
        return overlayWindow;
    }

    public void removeDragAndDropViews() {
        //Log.d("DragEvent", "Remove");
        isWishlisted = false;
        searchString = null;
        mProduct = deducedProductForNativeView = null;
        if (fabUtil != null) {
            fabUtil.removeFabItems();
        }

        WindowManager windowManager = (WindowManager) application.getSystemService(WINDOW_SERVICE);
        if (view != null) {
            if (view.getParent() != null) {
                windowManager.removeView(view);
            }
            view = null;
        }
        if (container != null) {
            if (container.getParent() != null) {
                windowManager.removeView(container);
            }
            container = null;
        }
        isFabVisible = false;
    }

    public void showAssistant(final Product product) {
        this.mProduct = product;
        if (product != null && !TextUtils.isEmpty(product.productURL)) {
            WishListRepository.getInstance(application).isWishlisted(product.productURL, new DbCallBack() {
                @Override
                public void onQueryProcessed(boolean isSuccess) {
                    isWishlisted = isSuccess;
                }
            });
        }
        if (mProduct != null && !TextUtils.isEmpty(mProduct.productName)) {
            searchString = mProduct.productName + " site:amazon.in OR site:snapdeal.com OR site:flipkart.com OR site:paytm.com OR site:myntra.com";
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.canDrawOverlays(application)) {
            if (view == null) {
                /*AssistantDragListener onDragListener = new AssistantDragListener();
                ShoppingDragWindow dragWindow = new ShoppingDragWindow(ShoppingAccessibilityService.this, R.layout.view_overlay, Gravity.RIGHT, Gravity.CENTER_VERTICAL);
                view = dragWindow.getView();

                if (container == null) {
                    ShoppingDragWindow dropWindow = new ShoppingDragWindow(ShoppingAccessibilityService.this, R.layout.overlay_container, Gravity.BOTTOM, Gravity.CENTER_HORIZONTAL);
                    container = dropWindow.getView();

                    container.setVisibility(View.INVISIBLE);
                    container.setOnDragListener(onDragListener);
                    dropWindow.show();
                }

                view.setOnDragListener(onDragListener);

                //view.setOnTouchListener(touchListener);
                view.setOnLongClickListener(longClickListener);
                view.setOnClickListener(clickListener);

                dragWindow.show();*/

                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        Build.VERSION.SDK_INT < Build.VERSION_CODES.O ? WindowManager.LayoutParams.TYPE_SYSTEM_ALERT : WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                                | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                        PixelFormat.TRANSLUCENT);

                layoutParams.gravity = Gravity.END | Gravity.CENTER_VERTICAL;
                WindowManager windowManager = (WindowManager) application.getSystemService(WINDOW_SERVICE);
                LayoutInflater layoutInflater = (LayoutInflater) application.getSystemService(LAYOUT_INFLATER_SERVICE);
                //view = layoutInflater.inflate(R.layout.view_overlay, null);

                OverlayFbIconView fbIconView = new OverlayFbIconView(application, R.layout.view_overlay);
                view = fbIconView.getOverlayIconView();

                View.OnDragListener onDragListener = new AssistantDragListener();
                if (container == null) {
                    WindowManager.LayoutParams layoutParamsContainer = new WindowManager.LayoutParams();
                    layoutParamsContainer.copyFrom(layoutParams);
                    layoutParamsContainer.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
                    container = layoutInflater.inflate(R.layout.overlay_container, null);
                    container.setVisibility(View.INVISIBLE);
                    container.setOnDragListener(onDragListener);
                    windowManager.addView(container, layoutParamsContainer);
                }

                view.setOnDragListener(onDragListener);

                //view.setOnTouchListener(touchListener);
                view.setOnLongClickListener(longClickListener);
                view.setOnClickListener(clickListener);
                fbIconView.showView(layoutParams);
                isFabVisible = true;
                //windowManager.addView(view, layoutParams);
            }
        }
    }

    public boolean isFabVisible() {
        return isFabVisible;
    }


    public void showOverlay(Product product, String searchString, String origin) {
        Intent intent = new Intent(context, OverlayDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtra(OverlayDetailActivity.Args.PRODUCT, product);
        intent.putExtra(OverlayDetailActivity.Args.SEARCH_STRING, searchString);
        intent.putExtra(OverlayDetailActivity.Args.ORIGIN, origin);
        application.startActivity(intent);

       /* overlayWindow = new OverlayWindow(application, product, searchString);
        overlayWindow.show();*/
    }

    private void removeFromWishlist(final View view, Product product) {
        WishListRepository.getInstance(application).deleteWishListItem(product.productURL, new DbCallBack() {
            @Override
            public void onQueryProcessed(final boolean isSuccess) {
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess) {
                            isWishlisted = false;
                            //Toast.makeText(application, "This item has been removed from the wishlist.", Toast.LENGTH_SHORT).show();
                            //((ImageView) view).setImageResource(R.drawable.pop_up_wishlist);
                            fabUtil.changeWishlistIcon(false, R.id.fab_wishlist);
                        }
                    }
                });
            }
        });
    }

    private void addToWishlist(final View view, Product product) {
        WishlistItem wishlistItem = new WishlistItem();
        wishlistItem.setItem(product.productName);
        wishlistItem.setImageUrl(product.productImage);
        wishlistItem.setProductUrl(product.productURL);
        wishlistItem.setShoppingSite(product.webSiteName);
        wishlistItem.setLastModifiedTime(System.currentTimeMillis());
        WishListRepository.getInstance(application).insertWishListItem(wishlistItem, new DbCallBack() {
            @Override
            public void onQueryProcessed(final boolean isSuccess) {
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess) {
                            isWishlisted = true;
                            //Toast.makeText(application, "You have been successfully wishlisted this item.", Toast.LENGTH_SHORT).show();
                            fabUtil.changeWishlistIcon(true, R.id.fab_wishlist);
                            //((ImageView) view).setImageResource(R.drawable.pop_up_wishlisted);
                        }
                    }
                });
            }
        });
    }

    public BroadcastReceiver getBroadcastReceiver() {
        return broadcastReceiver;
    }

    private class AssistantDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //Log.d("DragEvent", "Started");
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //Log.d("DragEvent", "Entered");
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //Log.d("DragEvent", "Exited");
                    break;
                case DragEvent.ACTION_DROP:
                    //Log.d("DragEvent", "Dropped");
                    removeDragAndDropViews();
                    break;
                case DragEvent.ACTION_DRAG_LOCATION:
                    break;

                case DragEvent.ACTION_DRAG_ENDED:
                    //Log.d("DragEvent", "Ended");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        int currentY = (int) event.getY();
                        int currentX = (int) event.getX();
                        int displayHeight = ShoppingUtils.getDisplayHeight(application);
                        int displayWidth = ShoppingUtils.getDisplayWidth(application);
                        if (currentY >= (displayHeight - 120) && currentX >= (displayWidth / 2)) {
                            //v.updateDragShadow(new View.DragShadowBuilder(new View(application)));
                            removeDragAndDropViews();
                        } else {
                            //Log.d("DragEvent", "Ended");
                            container.setVisibility(View.INVISIBLE);
                            if (view != null) {
                                view.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        //Log.d("DragEvent", "Ended");
                        container.setVisibility(View.INVISIBLE);
                        if (view != null) {
                            view.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public void hideFabView() {
        if (fabUtil != null) {
            fabUtil.removeFabItems();
        }
    }

    public void sendEvent(String category, String action, String label) {
        AnalyticsManager.getInstance().sendEvent(category, action, label);

    }

}
