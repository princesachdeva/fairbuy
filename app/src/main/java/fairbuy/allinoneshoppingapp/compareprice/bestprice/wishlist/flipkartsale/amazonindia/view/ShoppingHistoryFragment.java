package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.ShoppingHistoryAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.SearchHistory;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentShoppingHistoryBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.viewmodel.ShoppingViewModel;

import java.util.List;

/**
 * Created by aditya.amartya on 27/09/18
 */
public class ShoppingHistoryFragment extends Fragment implements Observer<List<SearchHistory>> {
    private FragmentShoppingHistoryBinding binding;
    private ShoppingViewModel shoppingViewModel;
    private ShoppingHistoryAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shoppingViewModel = ViewModelProviders.of(this).get(ShoppingViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shopping_history, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvShoppingHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvShoppingHistory.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        adapter = new ShoppingHistoryAdapter(false);
        binding.rvShoppingHistory.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shoppingViewModel.getLiveData(5).observe(this, this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        shoppingViewModel.getLiveData(5).removeObserver(this);
    }

    @Override
    public void onChanged(@Nullable List<SearchHistory> searchHistories) {
        //shoppingViewModel.getLiveData().removeObserver(this);
        adapter.clearData();
        binding.setVariable(BR.status, (searchHistories != null && searchHistories.size() > 0) ? 0 : 1);
        binding.executePendingBindings();
        adapter.addData(searchHistories);
    }
}
