package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.lrucache;


public class Node<Key, Value> {
     Key key;
     Value value;
     Node pre;
     Node next;

    public Node(Key key, Value value) {
        this.key = key;
        this.value = value;
    }


}
