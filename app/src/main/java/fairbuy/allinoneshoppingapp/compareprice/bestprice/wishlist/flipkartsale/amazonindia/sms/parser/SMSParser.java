package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.Status;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.repository.SmsRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by prince.sachdeva on 12/11/18.
 */

public class SMSParser {

    public interface OnTaskCompletionListener {
        void onTaskCompleted();
    }

    public static final int SOURCE_UNKNOWN = -1;
    public static final int SOURCE_FLIPKART = 0;
    public static final int SOURCE_AMAZON = 1;
    public static final int SOURCE_MYNTRA = 2;

    private static final String STATUS_ORDER_PLACED = "STATUS_ORDER_PLACED";
    private static final String STATUS_PACKED = "STATUS_PACKED";
    private static final String STATUS_SHIPPED = "STATUS_SHIPPED";
    static final String STATUS_OUT_FOR_DELIVERY = "STATUS_OUT_FOR_DELIVERY";
    private static final String STATUS_DELIVERED = "STATUS_DELIVERED";
    private static final String STATUS_DEMO_SCHEDULED = "STATUS_DEMO_SCHEDULED";
    private static final String STATUS_TECHICIAN_VISIT = "STATUS_TECHICIAN_VISIT";
    private static final String STATUS_INSTALLATION_COMPLETED = "STATUS_INSTALLATION_COMPLETED";
    private static final String STATUS_CANCELLED = "STATUS_CANCELLED";
    private static final String STATUS_REFUND_PROCESSED = "STATUS_REFUND_PROCESSED";


    static String[] hardCodedmessagesAmazon =
            {
                    "Confirmed: Order for WEXFORD Men's Cotton Polo T-Shirt Gr.. & 4 items is successfully placed.Track:http://amzn.in/80F1P4v or give missed call to 180030001001",
                    "Confirmed: Order for Pediasure Vanilla Delight - 200 g (Jar) is successfully placed & will reach you by 22-May. Track:http://amzn.in/dflQQOr",
                    "Dispatched: Philips Deco Mini 0.5-Watt B22 Base LED Bulb (White) will reach you by 06-Aug. Track at http://amzn.in/8kmD1sz or give missed call to 180030001001",
                    "Dispatched: Your package with US Polo Association Men's Slim... will reach you by 06-Jun. Track at http://amzn.in/3Kx0O4e or give missed call to 180030001001",
                    "Dispatched: Your package with Amazon Brand - Solimo Non-Stic... will reach you by 31-May. Track at http://amzn.in/dw1Dnrv or give missed call to 180030001001",
                    "On the way: Your Amazon package with Figaro Olive Oil Tin, 1L is on time and will reach you by 04-Aug. Track at http://amzn.in/8ziIXlB",
                    "On the way: Your Amazon package with US Polo Association Men's Slim Fit Cotton For.. is on time and will reach you by 06-Jun. Track at http://amzn.in/e3qXMKW",
                    "Arriving Today: WEXFORD Men's Cotton Polo T-Shirt Grey_Medium will be delivered by AmzAgent(+917249907770 PIN 4834). Track: http://amzn.in/5UcXSFK",
                    "Arriving early: Philips Deco Mini 0.5-Watt B22 Base LED Bulb (White) will be delivered by AmzAgent(+917249907770 PIN 2081). Track: http://amzn.in/bUipPVX",
                    "Arriving by 9pm: Your package with Tulsi P.. & 1 item is on road with our AmzAgent(+917249907770 PIN 2212) & will be delivered by 9pm. http://amzn.in/9LJPE01",
                    "Delivered: Your package with WEXFORD Men's Cotton Polo T-Shirt Grey_Medium. More info at http://amzn.in/cSOrYdF",
                    "Delivered: Your package with Sparx Women's Sky Blue and Yellow Nordic Walking Shoes - 5 UK... was successfully delivered. More info at http://amzn.in/bcYGyS1",
                    "Payment FAILED for order Envie ECR2.... To convert it to COD at no extra cost, click http://www.amazon.in/d?p=QAjTY7zlc20aW8zea_mNeknCdOPXxB6IRZQ-0k1-NZR525s",
                    "Return pickup for Alan Jones Clothi... is scheduled on 04-Aug, 7:00AM-1:00PM. Please return with original packaging, tags, manual etc. http://amzn.in/0EOwavY",
                    "Return pickup for US Polo Associati... is scheduled on 12-Jun, 7:00AM-1:00PM. Please return with original packaging, tags, manual etc. http://amzn.in/bL5KGo2",
                    "Your return pickup for 171-6390538-2876329 is scheduled for today and will be picked up between 10:00AM-1:00PM by AmzAgent(8447927588). http://amzn.in/hMp9ic3",
                    "Return picked up successfully for US Polo Associatio~.We'll notify once refund is initiated.Track: http://amzn.in/fwgvJ5T or give missed call to 180030001001",
                    "Return picked up successfully for Prestige PGMFB 800~.We'll notify once refund is initiated.Track: http://amzn.in/3Rggb18 or give missed call to 180030001001",
                    "Return picked up successfully for CPEX 7 Colour Chan~.We'll notify once refund is initiated.Track: http://amzn.in/b0VYeC4 or give missed call to 180030001001",
                    "Refund Initiated: Rs.649.00 for Alan Jones Clothing Men's Cotton Track Pant (JOG18-BG..., available on original method of payment. Details http://amzn.in/orders",
                    "Refund Initiated: Rs.1,308.00 for US Polo Association Men's Slim Fit Cotton Forma..., available in your card in 2-4 business days. Details http://amzn.in/orders"
            };


    static String[] hardCodedRegexForAmazon =
            {
                    "(.*?): Order for (.*?) is successfully placed",
                    "(.*?): Your Amazon package with (.*?) is on time",
                    "(.*?): Your package with (.*?) will reach you",
                    "(.*?): (.*?) will reach you by",
                    "(.*?): Your package with (.*?) is on road",
                    "(.*?): (.*?) will be delivered",
                    "(.*?): Your package with (.*?) was successfully delivered.",
                    "(.*?): Your package with (.*?). More info at http",
                    "(.*?) for (.*?) is scheduled on",
                    "(.*?) up successfully for(.*?)~..?We'll notify",
                    "(.*?): Rs.+ for(.*?), available"


            };


    static String[] hardCodedmessagesFlipkart =
            {
                    "Order Placed: Your order for Apple iPhone 7 (Gold, ... with order ID OD212308022828753000 amounting to Rs.47999 has been received. You can expect delivery by Tuesday, May 15, "
                            + "2018. We will send you an update when your order is packed/shipped. Manage your order here http://fkrt.it/eEqXSKNNNN",
                    "Order Placed: Your order for Apple iPhone 7 (Gold, ... with order ID OD212300661826367000 amounting to Rs.34499 has been received. You can expect delivery by Tuesday, May 15, "
                            + "2018. We will send you an update when your order is packed/shipped. Manage your order here http://fkrt.it/ucgVyKNNNN",
                    "Order Placed: Your order for Vu 98 cm (39 inch) Ful...+1 more product with order ID OD211992921084046000 amounting to Rs.16823 has been received. Your delivery is scheduled on "
                            + "Mon, Apr 09, 2018 between 9:00 AM and 1:00 PM. We will send you an update when your order is packed/shipped. Manage your order here http://fkrt.it/mI!0VKNNNN",
                    "Order Placed: Your order for Home Elite 104 TC Cott...+1 more product with order ID OD113337276892442000 amounting to Rs.598 has been received. You can expect delivery by "
                            + "Tuesday, Sep 11, 2018. We will send you an update when your order is packed/shipped. Manage your order here http://fkrt.it/XjavmKNNNN",
                    "Order Placed: Your order for Play King Funny Windup... with order ID OD113193808495388000 amounting to Rs.149 has been received. You can expect delivery by Tuesday, Aug 28, "
                            + "2018. We will send you an update when your order is packed/shipped. Manage your order here http://fkrt.it/jQ!kVnuuuN",
                    "Packed: Your clothvilla Women's Max... with order ID OD113161473030664000 has  been packed by the seller and will be shipped soon. We will share the tracking details once the "
                            + "item is on its  way to you. Manage your order here http://fkrt.it/CiaAHKNNNN",
                    "Shipped: Your Apple iPhone 7 (Gold, ... with order ID OD212308022828753000 has been shipped and will be delivered  by Tuesday, May 15, 2018. You will receive another SMS when "
                            + "Ekart Logistics's Wishmaster is out to deliver it. Track your shipment here http://fkrt.it/h4Bt9nuuuN",
                    "Shipped: Your Apple iPhone 7 (Gold, ... with order ID OD212300661826367000 has been shipped and will be delivered  by Tuesday, May 15, 2018. You will receive another SMS when "
                            + "Ekart Logistics's Wishmaster is out to deliver it. Track your shipment here http://fkrt.it/uc5ADKNNNN",
                    "Shipped: Your Vu 98 cm (39 inch) Ful... with order ID OD211992921084046000 has been shipped and will be delivered on Monday, Apr 09, 2018 between 01:00 PM and 05:00 PM. You "
                            + "will receive another SMS when Ekart Logistics's Wishmaster is out to deliver it. Track your shipment here http://fkrt.it/aO7h!LuuuN",
                    "Item Cancelled: Based on your request, 1 item (Yonex B SHOES COURTACE...) from your order OD212300729320024000 has been cancelled  by the seller.  The amount you have paid for "
                            + "this item will be refunded. We will notify you via email and SMS when the refund is processed. You can check details of the cancelled item here  http://fkrt"
                            + ".it/B7UTGnuuuN",
                    "Item Cancelled: Based on your request, 1 item (Hajamat V - 5 Blade Sh...) from your order OD113913907062455000 has been cancelled  by the seller.  You can check details of the "
                            + "cancelled item here  http://fkrt.it/JqkIZnuuuN",
                    "Items Cancelled: Based on your request, 2 items (Fogg Scent Impressio E...+1 more product) from your order OD113913875777075000 have been cancelled  by the seller. You can "
                            + "check details of the cancelled items here  http://fkrt.it/Y42pzKNNNN",
                    "Refund Processed: The refund of Rs. 34499.0 for your order OD212300661826367000 is successfully transferred to your card and it will be credited by May 16, 2018.",
                    "Refund Processed: The refund of Rs. 1915.0 for your order OD212300729320024000 is successfully transferred to your account and it will be credited by May 16, 2018. In case of "
                            + "delay, you may contact the bank's customer care using the number mentioned on the back of your card with the refund reference number: 18051300363796125.",
                    "Out for Delivery: Vu 98 cm (39 inch) Ful... with tracking ID FMPP0150425692 from flipkart.com, will be delivered between 01PM-05PM  today by an EKART Wish Master (call "
                            + "01139595329, PIN 113).",
                    "Out for Delivery: Play King Funny Windup... with tracking ID FMPP0184298042 from flipkart.com, will be delivered today by an EKART Service Partner (call 08071965428).",
                    "Out for Delivery: Home Elite 104 TC Cott... with tracking ID FMPP0189083152 from flipkart.com, will be delivered today by an EKART Service Partner .",
                    "Out for Delivery: Apple iPhone 7 (Gold 1... with tracking ID FMPP0157509553 from flipkart.com, will be delivered  before 7pm today by an EKART Wish Master (call 01139595329, "
                            + "PIN 119).",
                    "Delivered: Apple iPhone 7 (Gold 1... with tracking ID FMPP0157509553 from flipkart.com was delivered to your family member,  chaya, today. Click to give feedback: ",
                    "Delivered: Vu 98 cm (39 inch) Ful... with tracking ID FMPP0150425692 from flipkart.com was delivered,  today. Click to give feedback: http://ekrt.in/mmXxU2NNNN .",
                    "Delivered: Home Elite 104 TC Cott... with tracking ID FMPP0189083152 from flipkart.com was delivered,  Chhaya Sharma, by anil02 today. Click to give feedback: http://ekrt"
                            + ".in/!ru5d2NNNN",
                    "Delivered: Play King Funny Windup... with tracking ID FMPP0184298042 from flipkart.com was delivered,  Chhaya Sharma, by RAVINDRA today. Click to give feedback: http://ekrt"
                            + ".in/QltadKNNNN",
                    "Installation and Demo Scheduled: Installation and Demo for Vu 98 cm (39 inch) Full HD LED TV with order ID OD211992921084046000 has been scheduled with  jeeves consumer "
                            + "services private limited. Your service request number for the same is 8D9-27013FLPKRT-5147688 and will be done by Tuesday, Apr 10, 2018.",
                    "Technician Visit Scheduled: Technician Visit for Sansui 1.5 Ton 5 Star BEE Rating 2017 Split AC  - White with order ID OD111339724152822000 has been scheduled with our "
                            + "authorized partner sansui. Your service request number for the same is DEL0502180016 and will be done by Thursday, Feb 08, 2018.",
                    "Installation and Demo Completed: We trust that Installation and Demo for Vu 98 cm (39 inch) Full HD LED TV with order ID OD211992921084046000 was successfully done. We hope you"
                            + " are happy with the service provided.",
                    "Installation Completed: We trust that Installation for Sansui 1.5 Ton 5 Star BEE Rating 2017 Split AC  - White with order ID OD111339724152822000 was successfully done. We hope"
                            + " you are happy with the service provided."
            };

    static HashMap<String, String> hardCodedRegexForFlipkart1 = new HashMap<>();

    static {
        hardCodedRegexForFlipkart1.put("(.*?): Your order for (.*?) with order ID (.*?) amounting to", STATUS_ORDER_PLACED);
        hardCodedRegexForFlipkart1.put("(.*?): Your (.*?) with order ID (.*?) has been packed", STATUS_PACKED);
        hardCodedRegexForFlipkart1.put("(.*?): Your (.*?) with order ID (.*?) has been shipped", STATUS_SHIPPED);
        hardCodedRegexForFlipkart1.put("(.*?): (.*?) with tracking ID (.*?) from flipkart.com, will be delivered", STATUS_OUT_FOR_DELIVERY);
        hardCodedRegexForFlipkart1.put("(.*?): (.*?) with tracking ID (.*?) from flipkart.com, will be delivered", STATUS_DELIVERED);
        hardCodedRegexForFlipkart1.put("(.*?): Installation and Demo for (.*?) with order ID (.*?) has been scheduled", STATUS_DEMO_SCHEDULED);
        hardCodedRegexForFlipkart1.put("(.*?): Technician Visit for (.*?) with order ID (.*?) has been scheduled", STATUS_TECHICIAN_VISIT);
        hardCodedRegexForFlipkart1.put("(.*?): We trust that .+ for (.*?) with order ID (.*?) was successfully done", STATUS_INSTALLATION_COMPLETED);
        hardCodedRegexForFlipkart1.put("(.*?): Based on your request, .+ item.? .(.*?). from your order (.*?) h.+ been cancelled", STATUS_CANCELLED);
        hardCodedRegexForFlipkart1.put("(.*?): The refund of Rs. (.*?) for your order (.*?) is successfully", STATUS_REFUND_PROCESSED);
    }

    static String[] hardCodedRegexForFlipkart =
            {
                    // Order Plcaed
                    "(.*?): Your order for (.*?) with order ID (.*?) amounting to",

                    // Packed
                    "(.*?): Your (.*?) with order ID (.*?) has been packed",

                    // Shipped
                    "(.*?): Your (.*?) with order ID (.*?) has been shipped",

                    //Out for Delivery
                    "(.*?): (.*?) with tracking ID (.*?) from flipkart.com, will be delivered",

                    //Delivered
                    "(.*?): (.*?) with tracking ID (.*?) from flipkart.com was delivered",

                    //Installation and Demo Scheduled
                    "(.*?): Installation and Demo for (.*?) with order ID (.*?) has been scheduled",

                    //Technician Visit Scheduled
                    "(.*?): Technician Visit for (.*?) with order ID (.*?) has been scheduled",

                    //Installation Complted
                    "(.*?): We trust that .+ for (.*?) with order ID (.*?) was successfully done",

                    // Cancelled
                    "(.*?): Based on your request, .+ item.? .(.*?). from your order (.*?) h.+ been cancelled",

                    // Refund Processed
                    "(.*?): The refund of Rs. (.*?) for your order (.*?) is successfully"

            };

    static String[] hardCodedMessagesMyntra =
            {
                    "Order Confirmed: Your order for HRX Active by Hrithi... and 3 more item(s) (Order No. : 1081743-0265275-9032403) has been successfully placed. Complete order is expected to be "
                            + "delivered by 09 Jul 2018. Track your order at http://mynt.to/ac1892648am",
                    "Order Confirmed: Your order for Arrow Navy & White W... and 1 more item(s) (Order No. : 1077197-2373748-3748903) has been successfully placed. Complete order is expected to be "
                            + "delivered by 10 May 2018. Track your order at http://mynt.to/ac1892648am",
                    "Order Confirmed: Your order for SoundPEATS Black Q12 Blueto... (Order No. : 1077536-2787168-7447603) has been successfully placed. It is expected to be delivered by 09 May "
                            + "2018. Track your order at http://mynt.to/ac1892648am",
                    "Order Confirmed: Your order for Mast & Harbour Men N... and 3 more item(s) (Order No. : 1054526-9882349-1268501) has been successfully placed. It is expected to be delivered by"
                            + " 18 Aug 2017. Track your order at http://mynt.to/vjjm7876803",
                    "Order Confirmed: Your order for PUMA Women Black Expedite F... (Order No. : 1038266-6914058-8065903) has been successfully placed. It is expected to be delivered by 14 Feb "
                            + "2017. Track your order at http://mynt.to/nsqq92283x",
                    "Order Shipped: Your order for SoundPEATS Black Q12 Blueto... has been shipped through Myntra Logistics. The Myntra Logistics tracking code is ML0148941596.",
                    "Order Shipped: Your order for Mast & Harbour Men N... and 3 more item(s) has been shipped through Myntra Logistics. The Myntra Logistics tracking code is ML0146964838.",
                    "Order Shipped: Your order for PUMA Women Black Expedite F... (Order No. : 1038266-6914058-8065903) has been shipped through Myntra logistics and will be delivered to you by 14 "
                            + "Feb 2017. The Myntra logistics tracking code is ML0080296062. Track your order at http://mynt.to/ssbf21427e",
                    "ARRIVING EARLY: Your order for SoundPEATS Black Q12 Blueto... is out for delivery and is expected to be delivered today by our delivery partner (Phone: 7683005155).",
                    "ARRIVING EARLY: Your order for Mast & Harbour Men N... and 3 more item(s) is out for delivery and is expected to be delivered today by our delivery partner (Phone: 9958405820).",
                    "ARRIVING EARLY: Part of your order for Roadster Grey Sweater (Order No. : 1058154-3465553-0805901) is out for delivery and is expected to be delivered today by our delivery "
                            + "partner (Phone: 9910908168). Track your order at http://mynt.to/8ziy1850ib",
                    "ARRIVING EARLY: Part of your order for Harvard Men Maroon Printed ... (Order No. : 1058154-3465553-0805901) is out for delivery and is expected to be delivered today by SALEEM "
                            + "MALIK 2 (Phone: 8527895697). Track your order at http://mynt.to/wygn88073y",
                    "Order Delivered: We have successfully delivered your order for SoundPEATS Black Q12 Blueto....  Click http://mynt.to/prBkiAmp06G6c to give feedback.",
                    "Order Delivered: We have successfully delivered your order for Mast &amp; Harbour Men N... and 3 more item(s).  Click http://mynt.to/ctmy324680k to give feedback.",
                    "Order Delivered: We have successfully delivered your order for PUMA Women Black Expedite F... (Order No. : 1038266-6914058-8065903). Thank you for shopping at Myntra. Click to "
                            + "give feedback: http://mynt.to/z9k823873q",
                    "Order Delivered: We have successfully delivered your order for Mast & Harbour Men G... and 2 more item(s) (Order No. : 1044850-2315504-9847703). Thank you for shopping at "
                            + "Myntra. Click to give feedback: http://mynt.to/kdy279316y",
                    "Order Delivered: We have successfully delivered your order for Mast &amp; Harbour Men N... and 3 more item(s).  Click http://mynt.to/ctmy324680k to give feedback.",
                    "Exchange order confirmed: Your exchange order for Shaftesbury London Navy Blu... has been successfully placed and is expected to be delivered by Sun, 13 May. Please hand over "
                            + "the original item at the time of delivery.",
                    "Hi, your exchange order for British Club Men Olive Green &amp; Gold-Toned Woven Design Straight Kurta (Order No. : 1060729-2340653-0190403) has been successfully placed and is "
                            + "expected to be delivered by Thu, 26 Oct. Please hand over the original item at the time of delivery. Track your order at http://mynt.to/93i029538r",
                    "Hi, your exchange order for HRX by Hrithik Roshan Men White & Black Printed Casual Shirt (Order No. : 1055333-9148384-1367903) has been successfully placed and is expected to "
                            + "be delivered by Mon, 28 Aug. Please hand over the original item at the time of delivery. Track your order at http://mynt.to/i9dx55807ye",
                    "Exchange order shipped: Your exchange order for Mast &amp; Harbour Men Navy Blu... has been shipped through Myntra Logistics and is expected to be delivered to you by Sat, 28 "
                            + "Apr.",
                    "Hi, your exchange order for British Club Men Olive Green & Gold-Toned Woven Design Straight Kurta (Order No. : 1060729-2340653-0190403) has been shipped through Myntra "
                            + "Logistics and is expected to be delivered to you by Thu, 26 Oct.",
                    "Hi, your exchange order for Nayo Women Beige & Black Printed A-Line Kurta (Order No. : 1045111-3383771-7149601) has been shipped through Myntra Logistics and is expected to be "
                            + "delivered to you by Fri, 28 Apr.",
                    "Out for exchange: Your exchange order for Mast &amp; Harbour Men Navy Blu... is expected to be delivered by our delivery partner (Ph No. : 9958405820) by end of the day. Please"
                            + " hand over the original item at the time of delivery.",
                    "Hi, your exchange order for Nayo Women Beige & Black Printed A-Line Kurta (Order No. : 1045111-3383771-7149601) is expected to be delivered by ANUP KUMAR 1 (Ph No. : "
                            + "9718374575) by end of the day. Please hand over the original item at the time of delivery.",
                    "Hi, your return request for Roadster Men Grey & Navy Blue Striped Polo Collar T-shirt (Return No. : 221446501) has been successfully placed.   We expect to pick this up on or "
                            + "before Wed, 25 Jul.",
                    "Hi, your return request for Mast &amp; Harbour Men Navy Blue Printed Polo Collar T-shirt (Return No. : 216196546) has been successfully placed.   We expect to pick this up on "
                            + "or before Thu, 10 May.",
                    "Hi, your return request for HIGHLANDER Men White &amp; Navy Blue Slim Fit Printed Casual Shirt (Return No. : 215746027) has been successfully placed.   We expect to pick this "
                            + "up on or before Tue, 01 May.",
                    "Hi, your item Roadster Men Grey & Navy Blue Striped Polo Collar T-shirt (Return No. : 221446501) is expected to be picked up today by our associate SALEEM MALIK 2 (Ph No. : "
                            + "8527895697 ). Please keep it ready in an unsealed packet.",
                    "Hi, your item HIGHLANDER Men White &amp; Navy Blue Slim Fit Printed Casual Shirt (Return No. : 215746027) is expected to be picked up today by our pickup partner (Ph No. : "
                            + "9958405820 ). Please keep it ready in an unsealed packet.",
                    "Return picked up successfully for US Polo Associatio~.We'll notify once refund is initiated.Track: http://amzn.in/fwgvJ5T or give missed call to 180030001001",
                    "Return picked up successfully for Prestige PGMFB 800~.We'll notify once refund is initiated.Track: http://amzn.in/3Rggb18 or give missed call to 180030001001",
                    "Your Return pickup for Targus Black Casual Backpack... was successfully completed. We'll notify once the refund is initiated. Details: http://amzn.in/3TwoCNo",
                    "Hi, your return request for Roadster Men Grey & Navy Blue Striped Polo Collar T-shirt (Return No. : 221446501) has been processed.         Refund of Rs. 464.7 has been "
                            + "initiated to your card/bank account and is expected to reflect within 7-10 days.",
                    "\"Hi, your return request for Roadster Men Beige Slim Fit Solid Chinos (Return No. : 215622999) has been processed.\n"
                            + "\n"
                            + "Refund of Rs. 881.16 has been initiated to your card/bank account and is expected to reflect within 7-10 days.\"",
                    "\"Hi, your return request for Eavan Navy &amp; White Printed Georgette Layered Tunic (Return No. : 206979929) has been processed.\n"
                            + "\n"
                            + "Refund of Rs. 881.57 has been initiated to your card/bank account and is expected to reflect within 7-10 days.\"",
                    "Hi, the refund against the return request for \"Roadster Men Grey & Navy Striped Polo T-shirt\" (Return no: 216196560)has been successful and it will reflect in your bank "
                            + "account in the next 3 days. Refund reference # : 811316461039 Refund Amount: Rs.452.75/- . You may contact your debit/credit card support team by quoting Refund "
                            + "reference number for any further queries.",
                    "Hi, we would like to inform you that the refund against the return request for \"Roadster Grey Sweater\" (Return no: 206610771)has been successful and it will reflect in your "
                            + "bank account in the next 5 days. Refund reference # : 75503727299016021927015 Refund Amount: Rs.642.25/- . You may contact your debit/credit card support team by "
                            + "quoting Refund reference number for any further queries."
            };


    static String[] hardCodedRegexForMyntra = {
            // Order Shipped
            "(.*?): Your order for (.*?) has been shipped",

            // Arriving Early
            "(.*?): Your order for (.*?) is out for delivery",

            // Order Delivered
            "(.*?): We have successfully delivered your order for (.*?) Click http",

            //Exchange order confirmed
            "(.*?): Your exchange order for (.*?) has been successfully placed",

            "Hi, your (.*?) for (.*?) has been successfully placed",

            //Exchange order shipped
            "(.*?): Your exchange order for (.*?) has been shipped",

            //"Hi, your (.*?) for (.*?) has been successfully placed",

            //Return Request
            "Hi, your (.*?) for (.*?) has been successfully placed",

            //Return PickedUp
            "(.*?) successfully for (.*?)~.We'll notify once",
            "Your (.*?) for (.*?) was successfully completed",

            // Return Request Processed
            "Hi, your (.*?) for (.*?) has been processed",

            //Refund
            "Hi, the (.*?) the return request for (.*?) has been successful",
            "Hi, we would like to inform you that the (.*?) the return request for (.*?) has been successful"

    };


    private static SmsProductData parseAmazonSMS(String message, long time) {
        for (String regex : hardCodedRegexForAmazon) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(message);
            if (matcher.find()) {
                int matchCount = matcher.groupCount();
                if (matchCount > 1) {
                    String status = matcher.group(1);
                    String name = matcher.group(2);
                    if (!TextUtils.isEmpty(status) && !TextUtils.isEmpty(name)) {
                        return prepareSMSProductData(status, name, SOURCE_AMAZON, message, time);
                    }
                }
            }
        }
        return null;
    }

    private static SmsProductData prepareSMSProductData(String status, String name, int source, String message, long time) {
        SmsProductData smsProductData = new SmsProductData();
        smsProductData.setSource(source);
        smsProductData.setProductName(name);
        Status statusObj = new Status();
        statusObj.setStatus(status);
        statusObj.setStatusDate(time);
        statusObj.setMessage(message);
        smsProductData.appendStatus(statusObj);

        return smsProductData;
    }

    private static SmsProductData prepareSMSProductData(String status, String name, int source, String message, long time, String orderId, String trackingId) {
        SmsProductData smsProductData = prepareSMSProductData(status, name, source, message, time);
        smsProductData.setOrderId(orderId);
        smsProductData.setTrackId(trackingId);

        return smsProductData;
    }

    private static SmsProductData parseMyntraSMS(String message, long time) {
        for (String regex : hardCodedRegexForMyntra) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(message);
            if (matcher.find()) {
                int matchCount = matcher.groupCount();
                if (matchCount > 1) {
                    String status = matcher.group(1);
                    String name = matcher.group(2);
                    if (!TextUtils.isEmpty(status) && !TextUtils.isEmpty(name)) {
                        return prepareSMSProductData(status, name, SOURCE_MYNTRA, message, time);
                    }
                }
            }
        }
        return null;
    }

    private static SmsProductData parseFlipkartSMS1(String message, long time) {
        for (String regex : hardCodedRegexForFlipkart1.keySet()) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(message);
            if (matcher.find()) {
                int matchCount = matcher.groupCount();
                if (matchCount > 2) {
                    String status = matcher.group(1);
                    String name = matcher.group(2);
                    String id = matcher.group(3);
                    if (!TextUtils.isEmpty(status) && !TextUtils.isEmpty(name) && !TextUtils.isEmpty(id)) {
                        String orderStatusType = hardCodedRegexForFlipkart1.get(regex);
                        if (STATUS_ORDER_PLACED.equalsIgnoreCase(orderStatusType) || STATUS_PACKED.equalsIgnoreCase(orderStatusType) || STATUS_SHIPPED.equalsIgnoreCase(orderStatusType)
                                || STATUS_CANCELLED.equalsIgnoreCase(orderStatusType) || STATUS_DEMO_SCHEDULED.equalsIgnoreCase(orderStatusType) || STATUS_TECHICIAN_VISIT.equalsIgnoreCase(
                                orderStatusType) || STATUS_INSTALLATION_COMPLETED.equalsIgnoreCase(orderStatusType)) {
                            return prepareSMSProductData(status, name, SOURCE_FLIPKART, message, time, id, null);
                        } else if (STATUS_REFUND_PROCESSED.equalsIgnoreCase(orderStatusType)) {
                            return prepareSMSProductData(status, "", SOURCE_FLIPKART, message, time, id, null);
                        } else if (STATUS_OUT_FOR_DELIVERY.equalsIgnoreCase(orderStatusType) || STATUS_DELIVERED.equalsIgnoreCase(orderStatusType)) {
                            return prepareSMSProductData(status, "", SOURCE_FLIPKART, message, time, null, id);
                        }
                    }
                }
            }
        }
        return null;
    }

    private static SmsProductData parseFlipkartSMS(String message, long time) {
        for (String regex : hardCodedRegexForFlipkart) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(message);
            if (matcher.find()) {
                int matchCount = matcher.groupCount();
                if (matchCount > 2) {
                    String status = matcher.group(1);
                    String name = matcher.group(2);
                    String id = matcher.group(3);
                    if (!TextUtils.isEmpty(status) && !TextUtils.isEmpty(name) && !TextUtils.isEmpty(id)) {
                        return prepareSMSProductData(status, name, SOURCE_FLIPKART, message, time);
                    }
                }
            }
        }
        return null;
    }


    private static SmsProductData parseMessage(String body, String sender, long time) {
        int source = getSourceFromSender(sender);
        if (source == SOURCE_UNKNOWN) {
            return null;
        } else if (source == SOURCE_FLIPKART) {
            return parseFlipkartSMS(body, time);
        } else if (source == SOURCE_AMAZON) {
            return parseAmazonSMS(body, time);
        } else if (source == SOURCE_MYNTRA) {
            return parseMyntraSMS(body, time);
        }
        return null;
    }


    private static int getSourceFromSender(String sender) {
        if (!TextUtils.isEmpty(sender)) {
            sender = sender.toLowerCase();
            if (sender.contains("amazon") || sender.contains("51466")) {
                return SOURCE_AMAZON;
            } else if (sender.contains("flpkrt") || sender.contains("ekartl")) {
                return SOURCE_FLIPKART;
            } else if (sender.contains("myntra")) {
                return SOURCE_MYNTRA;
            }
            return SOURCE_UNKNOWN;
        }
        return SOURCE_UNKNOWN;

    }

    public static void parsePreviousMessages(Application context, OnTaskCompletionListener onTaskCompletionListener) {
        ArrayList<SmsProductData> smsProductDataArrayList = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {

                try {
                    String body = cursor.getString(cursor.getColumnIndexOrThrow("body"));
                    String address = cursor.getString(cursor.getColumnIndexOrThrow("address"));
                    String timeString = cursor.getString(cursor.getColumnIndexOrThrow("date"));
                    long time = 0;
                    if (!TextUtils.isEmpty(timeString) && TextUtils.isDigitsOnly(timeString)) {
                        time = Long.parseLong(timeString);
                    }
                    SmsProductData smsProductData = SMSParser.parseMessage(body, address, time);
                    if (smsProductData != null && smsProductData.getStatus() != null && smsProductData.getStatus().size() > 0) {
                        if (smsProductDataArrayList.contains(smsProductData)) {
                            SmsProductData smsProductData1 = smsProductDataArrayList.get(smsProductDataArrayList.indexOf(smsProductData));
                            smsProductData1.appendStatus(smsProductData.getStatus().get(0));
                        } else {
                            smsProductDataArrayList.add(smsProductData);
                        }
                    }

                } catch (Exception e) {

                }

            } while (cursor.moveToNext());
            Log.i("#### Cursor Count : ", "" + cursor.getCount());
            cursor.close();
        } else {

        }

        for (SmsProductData smsProductData : smsProductDataArrayList) {
            SmsRepository.getInstance((Application) context).insertSmsData(smsProductData);
        }
        onTaskCompletionListener.onTaskCompleted();
    }

    public static void parseIncomingMessage(final String body, String sender, final Application context) {

        final SmsProductData smsProductData = parseMessage(body, sender, System.currentTimeMillis());
        if (smsProductData == null) {
            return;
        }

        final LiveData<List<SmsProductData>> liveData = SmsRepository.getInstance(context).getSmsDataBySource(smsProductData.getSource());
        liveData.observeForever(new Observer<List<SmsProductData>>() {
            @Override
            public void onChanged(@Nullable List<SmsProductData> smsProductDataArrayList) {
                liveData.removeObserver(this);
                if (smsProductData != null && smsProductData.getStatus() != null && smsProductData.getStatus().size() > 0) {
                    Toast.makeText(context.getApplicationContext(), "Product Status Updated", Toast.LENGTH_SHORT).show();
                    if (smsProductDataArrayList.contains(smsProductData)) {
                        SmsProductData smsProductData1 = smsProductDataArrayList.get(smsProductDataArrayList.indexOf(smsProductData));
                        smsProductData1.appendStatusAtZero(smsProductData.getStatus().get(0));
                        SmsRepository.getInstance(context).insertSmsData(smsProductData1);
                    } else {
                        SmsRepository.getInstance(context).insertSmsData(smsProductData);
                    }
                }
            }
        });
    }

}
