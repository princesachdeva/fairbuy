package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.ShoppingDatabase;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsDao;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by aditya.amartya on 21/11/18
 */
public class SmsRepository {
    private static volatile SmsRepository instance;
    private SmsDao smsDao;
    private ExecutorService executorService;

    private SmsRepository(Application application) {
        smsDao = ShoppingDatabase.getInstance(application).getSmsDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public static SmsRepository getInstance(Application application) {
        SmsRepository localInstance = instance;
        if (localInstance == null) {
            synchronized (SmsRepository.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new SmsRepository(application);
                }
            }
        }
        return localInstance;
    }

    public LiveData<List<SmsProductData>> getSmsData() {
        return smsDao.getSmsData();
    }

    public LiveData<List<SmsProductData>> getSmsData(int count) {
        return smsDao.getSmsData(count);
    }

    public LiveData<List<SmsProductData>> getSmsDataBySource(int source) {
        return smsDao.getSmsDataBySource(source);
    }

    public void insertSmsData(final SmsProductData smsData) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                smsDao.insert(smsData);
            }
        });
    }

    public LiveData<Integer> getCount() {
        return smsDao.getCount();

    }
}
