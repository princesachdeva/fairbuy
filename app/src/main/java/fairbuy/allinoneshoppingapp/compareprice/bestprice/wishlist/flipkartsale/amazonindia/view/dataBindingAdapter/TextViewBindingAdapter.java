package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.dataBindingAdapter;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

/**
 * Created by aditya.amartya on 19/12/18
 */
public class TextViewBindingAdapter {
    /**
     * Set all types of drawable to a textview. Mainly for handling vector drawables
     *
     * @param textView             View on which drawable will be set
     * @param drawableLeftCompat   left drawable
     * @param drawableTopCompat    top drawable
     * @param drawableRightCompat  right drawable
     * @param drawableBottomCompat bottom drawable
     */
    @BindingAdapter(value = {"drawableLeftCompat", "drawableTopCompat", "drawableRightCompat", "drawableBottomCompat"}, requireAll = false)
    public static void setCompoundDrawableCompat(TextView textView, Drawable drawableLeftCompat, Drawable drawableTopCompat, Drawable drawableRightCompat, Drawable drawableBottomCompat) {
        textView.setCompoundDrawablesWithIntrinsicBounds(drawableLeftCompat, drawableTopCompat, drawableRightCompat, drawableBottomCompat);
    }
}
