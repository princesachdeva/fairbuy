package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView;

import android.app.Application;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.WindowManager;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;

import static android.content.Context.WINDOW_SERVICE;

public class BaseOverlayView {
    protected ViewDataBinding dataBinding;
    protected OverlayLayout myLayout;
    private WindowManager windowManager;

    public BaseOverlayView(Application application, int layoutId) {
        windowManager = (WindowManager) application.getSystemService(WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        myLayout = new OverlayLayout(application, this);
        myLayout.setId(R.id.myLayout);
        dataBinding = DataBindingUtil.inflate(inflater, layoutId, myLayout, true);
    }

    public void showView(WindowManager.LayoutParams layoutParams) {
        windowManager.addView(myLayout, layoutParams);
    }

    public void hide() {
        if (myLayout.getParent() != null)
            windowManager.removeView(myLayout);
    }
}
