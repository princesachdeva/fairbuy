package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.ShoppingHistoryAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.SearchHistory;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentShoppingHistoryBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.FairBuyActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.RecentlyViewedActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.listeners.OnViewAllClickListener;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.viewmodel.ShoppingViewModel;

import java.util.List;

/**
 * Created by aditya.amartya on 27/09/18
 */
public class HomeShoppingHistoryFragment extends BaseFragment implements Observer<List<SearchHistory>>, View.OnClickListener {
    private ShoppingViewModel shoppingViewModel;
    private ShoppingHistoryAdapter adapter;
    private boolean isHome = true;
    private FairBuyActivity onViewAllClickListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shoppingViewModel = ViewModelProviders.of(this).get(ShoppingViewModel.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FairBuyActivity) {
            onViewAllClickListener = ((FairBuyActivity) context);
        }
    }


    @Override
    protected int getResourceId() {
        return R.layout.fragment_shopping_history;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(ARGS.IS_HOME)) {
            isHome = bundle.getBoolean(ARGS.IS_HOME);
        }

        if (isHome) {
            binding.setVariable(BR.isHome, true);
            binding.setVariable(BR.clickListener, this);
        }
        RecyclerView rvShoppingHistory = ((FragmentShoppingHistoryBinding) binding).rvShoppingHistory;

        rvShoppingHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new ShoppingHistoryAdapter(isHome, this);
        rvShoppingHistory.setAdapter(adapter);

        if (!isHome) {
            FirebaseAnalyticsManager.getInstance().setScreen(getActivity(), "RecentlyViewed");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        shoppingViewModel.getLiveData(isHome ? 5 : 25).observe(this, this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        shoppingViewModel.getLiveData(isHome ? 5 : 25).removeObserver(this);
    }

    @Override
    public void onChanged(@Nullable List<SearchHistory> searchHistories) {

        adapter.clearData();
        boolean hasData = searchHistories != null && searchHistories.size() > 0;
        binding.setVariable(BR.status, hasData ? 0 : 1);
        binding.executePendingBindings();
        adapter.addData(searchHistories);
        if (!hasData) {
            hideView();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonViewAll:
                if (onViewAllClickListener != null) {
                    onViewAllClickListener.onViewAll(OnViewAllClickListener.VIEW_ALL_RECENTLY_VIEWED);
                } else {
                    Intent intent = new Intent(getContext(), RecentlyViewedActivity.class);
                    getContext().startActivity(intent);
                }
                break;
        }
    }

    public interface ARGS {
        String IS_HOME = "is_home";
    }

    private void hideView() {
        if (isHome) {
            ((FragmentShoppingHistoryBinding) binding).frameContainer.setVisibility(View.GONE);
        }
    }
}
