package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.listeners;

/**
 * Created by aditya.amartya on 20/12/18
 */
public interface OnViewAllClickListener {
    int VIEW_ALL_WISHLIST = 0;
    int VIEW_ALL_RECENTLY_VIEWED = VIEW_ALL_WISHLIST + 1;
    int VIEW_ALL_SEARCH = VIEW_ALL_RECENTLY_VIEWED + 1;

    void onViewAll(int viewAllType);

    void onViewHome();
}
