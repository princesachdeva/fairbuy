package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aditya.amartya on 24/10/18
 */
public class ApiClient {
    private volatile static Retrofit retrofitClient;

    public static Retrofit getClient() {
        Retrofit localClient = retrofitClient;
        if(localClient == null) {
            synchronized (ApiClient.class) {
                localClient = retrofitClient;
                if(localClient == null) {
                    localClient = retrofitClient = new Retrofit.Builder().baseUrl("https://www.googleapis.com/").
                            addConverterFactory(GsonConverterFactory.create()).build();
                }
            }
        }
        return localClient;
    }
}
