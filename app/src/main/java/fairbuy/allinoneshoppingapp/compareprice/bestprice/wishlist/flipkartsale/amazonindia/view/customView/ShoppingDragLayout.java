package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView;

import android.content.Context;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.widget.LinearLayout;

/**
 * Created by aditya.amartya on 22/11/18
 */
public class ShoppingDragLayout extends LinearLayout {
    ShoppingDragWindow dragWindow;

    public ShoppingDragLayout(Context context, ShoppingDragWindow dragWindow) {
        super(context);
        this.dragWindow = dragWindow;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0) {
                getKeyDispatcherState().startTracking(event, this);
                return true;

            } else if (event.getAction() == KeyEvent.ACTION_UP) {
                getKeyDispatcherState().handleUpEvent(event);

                if (event.isTracking() && !event.isCanceled()) {
                    // dismiss your window:
                    dragWindow.hide();

                    return true;
                }
            }
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean dispatchDragEvent(DragEvent event) {
        boolean dispatchDragEvent = super.dispatchDragEvent(event);
        if (dispatchDragEvent && (event.getAction() == DragEvent.ACTION_DRAG_STARTED
                || event.getAction() == DragEvent.ACTION_DRAG_ENDED || event.getAction() == DragEvent.ACTION_DROP
                || event.getAction() == DragEvent.ACTION_DRAG_EXITED)) {
            // If we got a start or end and the return value is true, our
            // onDragEvent wasn't called by ViewGroup.dispatchDragEvent
            // So we do it here.
            onDragEvent(event);
        }
        return dispatchDragEvent;
    }
}
