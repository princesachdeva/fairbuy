package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;

public class FlexLayoutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<String> trendingList;
    private View.OnClickListener onClickListener;

    public FlexLayoutAdapter(ArrayList<String> trendingList, View.OnClickListener onClickListener) {
        this.trendingList = trendingList;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_flex_layout_trending, viewGroup, false);
        return new FlexViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        FlexViewHolder holder = (FlexViewHolder) viewHolder;
        holder.binding.setVariable(BR.trendingText, trendingList.get(position));
        holder.binding.setVariable(BR.clickListener, onClickListener);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return trendingList.size();
    }

    private static class FlexViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public FlexViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
