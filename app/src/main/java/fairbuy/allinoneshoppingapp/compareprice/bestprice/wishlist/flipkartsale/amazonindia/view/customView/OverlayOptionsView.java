package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView;

import android.app.Application;
import android.view.View;

public class OverlayOptionsView extends BaseOverlayView {
    public OverlayOptionsView(Application application, int layoutId) {
        super(application, layoutId);
    }

    public View getView() {
        return myLayout;
    }
}
