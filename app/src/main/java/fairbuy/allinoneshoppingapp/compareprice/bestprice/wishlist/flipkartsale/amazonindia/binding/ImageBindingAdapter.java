package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.binding;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by aditya.amartya on 04/10/18
 */
public class ImageBindingAdapter {
    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            if (!imageUrl.startsWith("http")) {
                imageUrl = "http:" + imageUrl;
            }
            RequestOptions options = new RequestOptions().fitCenter();
            Glide.with(imageView.getContext()).setDefaultRequestOptions(options).load(imageUrl).into(imageView);
        }
    }

    @BindingAdapter({"imgSrc"})
    public static void setImageResource(ImageView imageView, int id) {
        imageView.setImageResource(id);
    }
}
