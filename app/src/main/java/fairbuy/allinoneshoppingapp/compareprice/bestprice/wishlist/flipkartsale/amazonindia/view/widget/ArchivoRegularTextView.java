package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by aditya.amartya on 05/12/18
 */
public class ArchivoRegularTextView extends AppCompatTextView {

    public ArchivoRegularTextView(Context context) {
        super(context);
        setFonts(context);
    }

    public ArchivoRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFonts(context);
    }

    public ArchivoRegularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFonts(context);
    }

    private void setFonts(Context ctx) {
        Typeface primeTypeFace = Typeface.createFromAsset(ctx.getAssets(), "fonts/Archivo-Regular.ttf");
        setTypeface(primeTypeFace);
    }
}
