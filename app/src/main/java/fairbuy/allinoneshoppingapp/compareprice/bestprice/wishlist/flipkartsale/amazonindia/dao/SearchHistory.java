package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;

/**
 * Created by aditya.amartya on 28/09/18
 */

@Entity(tableName = "search_history")
public class SearchHistory {


    @ColumnInfo
    private String item;

    @ColumnInfo(name = "image_url")
    private String imageUrl;

    @ColumnInfo(name = "shopping_site")
    private String shoppingSite;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "product_url")
    private String productUrl;

    @ColumnInfo
    private String brand;

    public long getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(long lastModifiedTime) {
        this.lastModifiedTime = System.currentTimeMillis();
    }

    @ColumnInfo(name = "last_modified_time")
    private long lastModifiedTime = System.currentTimeMillis();


    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getShoppingSite() {
        return shoppingSite;
    }

    public void setShoppingSite(String shoppingSite) {
        this.shoppingSite = shoppingSite;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        productUrl = Utils.getUrlWithoutParameters(productUrl);
        this.productUrl = productUrl;
    }

    public WishlistItem getWishListItem() {
        WishlistItem wishlistItem = new WishlistItem();
        wishlistItem.setProductUrl(productUrl);
        wishlistItem.setItem(item);
        wishlistItem.setShoppingSite(shoppingSite);
        wishlistItem.setImageUrl(imageUrl);
        wishlistItem.setLastModifiedTime(System.currentTimeMillis());
        return wishlistItem;
    }

    public int getAppIcon() {
        if (!TextUtils.isEmpty(productUrl)) {
            if (productUrl.contains("flipkart")) {
                return R.drawable.flipkart;
            } else if (productUrl.contains("amazon")) {
                return R.drawable.amazon;
            } else if (productUrl.contains("snapdeal")) {
                return R.drawable.snapdeal;
            } else if (productUrl.contains("myntra")) {
                return R.drawable.myntra;
            } else if (productUrl.contains("paytm")) {
                return R.drawable.paytm;
            }
        }
        return R.drawable.flipkart;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Product getProduct() {
        Product product = new Product();
        product.productName = item;
        product.productImage = imageUrl;
        product.productURL = productUrl;
        product.website = getWebsite();

        return product;
    }

    public ShoppingUtils.WEBSITE getWebsite() {
        if (!TextUtils.isEmpty(productUrl)) {
            if (productUrl.contains("flipkart")) {
                return ShoppingUtils.WEBSITE.FLIPKART;
            } else if (productUrl.contains("amazon")) {
                return ShoppingUtils.WEBSITE.AMAZON;
            } else if (productUrl.contains("snapdeal")) {
                return ShoppingUtils.WEBSITE.SNAPDEAL;
            } else if (productUrl.contains("myntra")) {
                return ShoppingUtils.WEBSITE.MYNTRA;
            } else if (productUrl.contains("paytm")) {
                return ShoppingUtils.WEBSITE.PAYTM;
            }
        }
        return ShoppingUtils.WEBSITE.TEMP;
    }
}
