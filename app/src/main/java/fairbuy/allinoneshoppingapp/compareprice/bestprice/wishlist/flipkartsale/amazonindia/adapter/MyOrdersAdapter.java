package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.OrderDetailActivity;

/**
 * Created by prince.sachdeva on 07/12/18.
 */

public class MyOrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private ArrayList<SmsProductData> mProductDataArrayList;
    private View.OnClickListener mOnClickListener;


    public static final int VIEW_TYPE_ASK_PERMISSION = 0;
    public static final int VIEW_TYPE_DISPLAY_DATA = 1;
    public static final int VIEW_TYPE_EMPTY_LIST = 2;
    public static final int VIEW_TYPE_LOADING = 3;


    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    private int viewType = VIEW_TYPE_LOADING;

    public void updateData(List<SmsProductData> smsProductData) {
        if (this.mProductDataArrayList == null) {
            this.mProductDataArrayList = new ArrayList<>();
        } else {
            this.mProductDataArrayList.clear();
        }
        if (smsProductData == null || smsProductData.size() == 0) {
            return;
        }
        int start = getItemCount();

        this.mProductDataArrayList.addAll(smsProductData);
        notifyItemRangeChanged(start, smsProductData.size());
    }

    public void clearData() {
        if (mProductDataArrayList != null) {
            mProductDataArrayList.clear();
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_DISPLAY_DATA) {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_my_orders, viewGroup, false);
            binding.setVariable(BR.clickListener, this);
            return new MyOrdersHolder(binding);
        } else if (viewType == VIEW_TYPE_EMPTY_LIST) {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_my_orders_list_empty, viewGroup, false);
            binding.getRoot().setOnClickListener(this);
            return new EmptyViewHolder(binding);
        } else if (viewType == VIEW_TYPE_LOADING) {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.progress_bar, viewGroup, false);
            binding.getRoot().setOnClickListener(this);
            return new ProgressViewHolder(binding);
        } else {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_my_orders_ask_permission, viewGroup, false);
            binding.setVariable(BR.clickListener, mOnClickListener);
            return new AskPermissionViewHolder(binding);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof MyOrdersHolder) {
            MyOrdersHolder holder = (MyOrdersHolder) viewHolder;
            SmsProductData smsProductData = mProductDataArrayList.get(position);
            holder.binding.setVariable(BR.title, smsProductData.getProductName());
            holder.binding.setVariable(BR.status, smsProductData.getListingDisplayStatus());
            holder.binding.setVariable(BR.drawableId, smsProductData.getAppIcon());
            holder.binding.getRoot().setTag(smsProductData);
        }
    }

    @Override
    public int getItemCount() {
        if (viewType == VIEW_TYPE_DISPLAY_DATA) {
            return mProductDataArrayList != null ? mProductDataArrayList.size() : 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(view.getContext(), OrderDetailActivity.class);
        intent.putExtra(OrderDetailActivity.ARGS.SMS_PRODUCT_DATA, (Serializable) view.getTag());
        view.getContext().startActivity(intent);
    }

    public static class MyOrdersHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public MyOrdersHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class EmptyViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public EmptyViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public ProgressViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class AskPermissionViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public AskPermissionViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void setAskPermissionClickListener(View.OnClickListener clickListener) {
        this.mOnClickListener = clickListener;
    }

}
