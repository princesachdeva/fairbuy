package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.SearchItemAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ActivitySearchBinding;

/**
 * Created by aditya.amartya on 03/10/18
 */
public class SearchItemActivity extends AppCompatActivity implements WebSearchFragment.OnBackPressTaskCompltedListener {
    private ActivitySearchBinding binding;
    private String searchKeyword;
    private MenuItem searchItem;
    private SearchView searchView;
    private SearchItemAdapter searchItemAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        searchKeyword = getIntent().getStringExtra(ARGS.SEARCH_STRING);
        initActionBar();
        initView();
    }

    private void initView() {
        searchItemAdapter = new SearchItemAdapter(getSupportFragmentManager(), searchKeyword);
        binding.pager.setAdapter(searchItemAdapter);
        binding.tab.setupWithViewPager(binding.pager);
    }

    private void initActionBar() {
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(searchKeyword);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchItem.expandActionView();
        searchItem.setOnActionExpandListener(onActionExpandListener);
        searchView.setQuery(searchKeyword, false);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(queryTextListener);
        return true;
    }

    MenuItem.OnActionExpandListener onActionExpandListener = new MenuItem.OnActionExpandListener() {
        @Override
        public boolean onMenuItemActionExpand(MenuItem item) {
            return true;
        }

        @Override
        public boolean onMenuItemActionCollapse(MenuItem item) {
            onBackPressed();
            return true;
        }
    };

    private SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            searchItemAdapter.updateSearchKeyword(s);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            return false;
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (binding.pager != null) {
            Fragment fragment = (Fragment) binding.pager.getAdapter().instantiateItem(binding.pager, binding.pager.getCurrentItem());
            if (fragment instanceof WebSearchFragment) {
                ((WebSearchFragment) fragment).onBackPressed();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void onBackPressTaskCompleted() {
        super.onBackPressed();
    }

    public interface ARGS {
        String SEARCH_STRING = "search_string";
    }
}
