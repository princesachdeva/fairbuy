package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;

/**
 * Created by aditya.amartya on 27/09/18
 */
public class ShoppingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private View.OnClickListener onClickListener;

    public ShoppingListAdapter(View.OnClickListener clickListener) {
        this.onClickListener = clickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_shopping_list, viewGroup, false);
        binding.setVariable(BR.clickListener, onClickListener);
        binding.executePendingBindings();
        return new ShoppingListHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ShoppingListHolder holder = (ShoppingListHolder) viewHolder;
        ShoppingUtils.WEBSITE website = ShoppingUtils.WEBSITE.values()[position];
        holder.binding.setVariable(BR.title, website.toString());
        holder.binding.setVariable(BR.description, website.getValue());
        holder.binding.setVariable(BR.drawableId, website.getDrawableId());
        holder.binding.getRoot().setTag(website);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return ShoppingUtils.WEBSITE.dataValues().length;
    }

    private static class ShoppingListHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public ShoppingListHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
