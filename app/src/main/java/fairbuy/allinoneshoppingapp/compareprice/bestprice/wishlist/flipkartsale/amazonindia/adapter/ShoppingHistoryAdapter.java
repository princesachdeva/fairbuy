package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.SearchHistory;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ItemHistoryBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.DbCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.WishListRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.WebSiteViewActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aditya.amartya on 04/10/18
 */
public class ShoppingHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private final WishListRepository wishListRepository;
    private ArrayList<SearchHistory> searchHistories;
    private boolean isHome;
    private BaseFragment mBaseFragment;

    public ShoppingHistoryAdapter(boolean isHome) {
        wishListRepository = WishListRepository.getInstance((Application) ShoppingAssistantApplication.getContext());
        this.isHome = isHome;
    }

    public ShoppingHistoryAdapter(boolean isHome, BaseFragment baseFragment) {
        this(isHome);
        this.mBaseFragment = baseFragment;
    }

    public void addData(List<SearchHistory> searchHistories) {
        if (this.searchHistories == null) {
            this.searchHistories = new ArrayList<>();
        }
        if (searchHistories == null || searchHistories.size() == 0) {
            return;
        }
        int start = getItemCount();
        this.searchHistories.addAll(searchHistories);
        notifyItemRangeChanged(start, searchHistories.size());
    }

    public void clearData() {
        if (searchHistories != null) {
            searchHistories.clear();
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_history, viewGroup, false);
        binding.setVariable(BR.clickListener, mOnClickListener);
        return new ShoppingHistoryHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final ShoppingHistoryHolder holder = (ShoppingHistoryHolder) viewHolder;
        SearchHistory searchHistory = searchHistories.get(position);
        final ItemHistoryBinding binding = (ItemHistoryBinding) holder.binding;
        wishListRepository.isWishlisted(searchHistory.getProductUrl(), new DbCallBack() {
            @Override
            public void onQueryProcessed(boolean isSuccess) {
                binding.ivWishlist.setTag(R.id.ivWishlist, isSuccess);
                binding.ivWishlist.setImageResource(isSuccess ? R.drawable.ic_wishlisted : R.drawable.ic_wishlist);
            }
        });
        binding.ivWishlist.setTag(R.id.cardView, searchHistory);
        holder.binding.setVariable(BR.searchHistory, searchHistory);
        holder.binding.setVariable(BR.isHome, isHome);
        holder.binding.setVariable(BR.drawableId, searchHistory.getAppIcon());
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return searchHistories != null ? searchHistories.size() : 0;
    }

    @Override
    public void onClick(View view) {
        ShoppingUtils.WEBSITE website = ShoppingUtils.WEBSITE.TEMP;
        website.setUrl((String) view.getTag());
        website.setName((String) view.getTag());
        Intent intent = new Intent(view.getContext(), WebSiteViewActivity.class);
        intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, website);
        view.getContext().startActivity(intent);
    }

    public static class ShoppingHistoryHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public ShoppingHistoryHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivWishlist:
                    handleWishlist(view);
                    break;
                case R.id.cardItem:
                    if (view.getTag() != null && view.getTag() instanceof SearchHistory) {
                        SearchHistory searchHistory = (SearchHistory) view.getTag();
                        onItemClick(view.getContext(), searchHistory);
                    }
                    break;
            }
        }
    };

    private void onItemClick(Context context, SearchHistory searchHistory) {
        if (!TextUtils.isEmpty(searchHistory.getProductUrl())) {
            if (mBaseFragment != null) {
                mBaseFragment.showProgressBarForDelay();
            }
            Product product = searchHistory.getProduct();
            if (isHome) {
                product.productOrigin = AnalyticsConstants.Label.RV_HOME;
            } else {
                product.productOrigin = AnalyticsConstants.Label.RV_NAV;
            }
            ShoppingUtils.launchProductPageInAppRV(context, product);
        }
    }

    private void handleWishlist(View view) {
        boolean isWishlisted = (boolean) view.getTag(R.id.ivWishlist);
        SearchHistory searchHistory = (SearchHistory) view.getTag(R.id.cardView);
        if (!TextUtils.isEmpty(searchHistory.getProductUrl())) {
            final int i = searchHistories.indexOf(searchHistory);

            if (isWishlisted) {
                wishListRepository.deleteWishListItem(searchHistory.getProductUrl(), new DbCallBack() {
                    @Override
                    public void onQueryProcessed(boolean isSuccess) {
                        postResponseToMainThread(i);
                    }
                });
            } else {
                wishListRepository.insertWishListItem(searchHistory.getWishListItem(), new DbCallBack() {
                    @Override
                    public void onQueryProcessed(boolean isSuccess) {
                        postResponseToMainThread(i);
                    }
                });
            }
        }
    }

    private void postResponseToMainThread(final int position) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                notifyItemChanged(position);
            }
        });
    }
}
