package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.OrderDetailActivity.ARGS.SMS_PRODUCT_DATA;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ActivityOrderDetailBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment.OrderDetailFragment;

/**
 * Created by prince.sachdeva on 09/12/18.
 */

public class OrderDetailActivity extends BaseActivity {

    public interface ARGS {
        String SMS_PRODUCT_DATA = "SMS_PRODUCT_DATA";
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail);
        initToolBar(((ActivityOrderDetailBinding) binding).toolbar, true, "Order Details");

        if (getIntent() != null && getIntent().getSerializableExtra(SMS_PRODUCT_DATA) != null) {
            changeFragment(OrderDetailFragment.getInstance((SmsProductData) getIntent().getSerializableExtra(SMS_PRODUCT_DATA)), "");
        } else {
            finish();
        }
    }
}
