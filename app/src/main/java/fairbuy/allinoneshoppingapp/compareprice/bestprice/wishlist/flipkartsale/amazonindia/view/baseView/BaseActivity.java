package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;

/**
 * Created by aditya.amartya on 03/10/18
 */
public abstract class BaseActivity<V extends ViewDataBinding> extends AppCompatActivity {
    public ViewDataBinding binding;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            //decorSystemUIVisibility = decor.getSystemUiVisibility();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
    }

    protected void initToolBar(Toolbar toolbar, boolean isShowBackButton, String title) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(isShowBackButton);
            //actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setTitle(title);
        }
    }


    public void changeFragment(Fragment fragment, String tag) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContainer, fragment);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        FragmentManager manger = getSupportFragmentManager();
        BaseFragment fragment = (BaseFragment) manger.findFragmentById(R.id.flContainer);
        hideKeyboard();
        if (manger.getBackStackEntryCount() <= 1) {
            if (fragment != null) {
                if (fragment.onBackPressed()) {
                    this.finish();
                }
            } else {
                this.finish();
            }
        } else {
            if (fragment != null) {
                if (fragment.onBackPressed()) {
                    manger.popBackStack();
                }
            } else {
                manger.popBackStack();
            }

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    protected void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void setToolbarLogo(boolean logo) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayUseLogoEnabled(logo);
            getSupportActionBar().setDisplayShowTitleEnabled(!logo);
            getSupportActionBar().setLogo(R.drawable.logo_actionbar);
        }
    }

    protected void showProgressBarForDelay() {

    }

}
