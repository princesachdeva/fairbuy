package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ItemOverlayBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;

/**
 * Created by aditya.amartya on 05/11/18
 */
public class OverlayViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Product> products;
    private View.OnClickListener clickListener;

    public OverlayViewAdapter(ArrayList<Product> products, View.OnClickListener clickListener) {
        this.products = products;
        this.clickListener = clickListener;
    }

    public void updateProductWishlistState(int position, boolean isWishlist) {
        if (products != null && products.size() > 0) {
            products.get(position).isWishlisted = isWishlist;
            notifyItemChanged(position);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_overlay, viewGroup, false);
        binding.setVariable(BR.clickListener, clickListener);
        return new OverlayViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {
        final OverlayViewHolder holder = (OverlayViewHolder) viewHolder;
        final Product product = products.get(position);
        if (product != null) {
            setViewHeight(holder, false);
            //product.isWishlisted = WishListRepository.getInstance((Application) ShoppingAssistantApplication.getContext()).isWishlisted(product.productURL);
            holder.binding.setVariable(BR.product, product);
            ((ItemOverlayBinding) holder.binding).ivWishlist.setTag(R.string.product_tag, product);
            ((ItemOverlayBinding) holder.binding).ivWishlist.setTag(R.string.position_tag, position);
            holder.binding.executePendingBindings();
        } else {
            setViewHeight(holder, true);
        }
    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    private void setViewHeight(RecyclerView.ViewHolder holder, boolean hide) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) (holder.itemView.getLayoutParams());
        layoutParams.height = hide ? 0 : ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams.bottomMargin = hide ? 0 : Utils.dpToPx(8, holder.itemView.getContext());
        holder.itemView.setLayoutParams(layoutParams);
    }

    private static class OverlayViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public OverlayViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
