package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ActivityOrderDetailBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment.HomeShoppingHistoryFragment;

/**
 * Created by prince.sachdeva on 02/01/19.
 */

public class RecentlyViewedActivity extends BaseActivity {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail);
        initToolBar(((ActivityOrderDetailBinding) binding).toolbar, true, "Recently Viewed");

        HomeShoppingHistoryFragment homeShoppingHistoryFragment = new HomeShoppingHistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(HomeShoppingHistoryFragment.ARGS.IS_HOME, false);
        homeShoppingHistoryFragment.setArguments(bundle);
        changeFragment(homeShoppingHistoryFragment, "");

    }
}