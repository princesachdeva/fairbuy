package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util;

import android.content.SharedPreferences;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;

/**
 * Created by prince.sachdeva on 26/11/18.
 */

public class PreferenceManager {

    private static final String PREF_FILE = "MyPref";

    public static void save(String key, boolean value) {

        SharedPreferences pref = ShoppingAssistantApplication.getContext().getSharedPreferences(PREF_FILE, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean get(String key, boolean defaultValue) {
        SharedPreferences pref = ShoppingAssistantApplication.getContext().getSharedPreferences(PREF_FILE, 0); // 0 - for private mode
        return pref.getBoolean(key, defaultValue);
    }

    public static void writeIntToPreference(String key, int value) {
        SharedPreferences pref = ShoppingAssistantApplication.getContext().getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int getIntFromPreference(String key) {
        SharedPreferences pref = ShoppingAssistantApplication.getContext().getSharedPreferences(PREF_FILE, 0);
        return pref.getInt(key, 0);
    }

    public static void writeLOngToPreference(String key, long value) {
        SharedPreferences pref = ShoppingAssistantApplication.getContext().getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static long getLongFromPreference(String key, long defaultValue) {
        SharedPreferences pref = ShoppingAssistantApplication.getContext().getSharedPreferences(PREF_FILE, 0);
        return pref.getLong(key, 0);
    }
}
