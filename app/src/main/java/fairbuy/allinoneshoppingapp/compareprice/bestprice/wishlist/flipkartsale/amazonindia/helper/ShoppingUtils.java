package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.BESTSELLER;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.COMPARE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.DEMO;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.RECENTLY_VIEWED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.SEARCH;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.WISHLIST;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Category.PURCHASE_GOAL;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.BESTSERLLER_SELECTED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.COMPARE_ITEM_SELECTED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.DEMO_SELECTED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.RECENT_ITEM_SELECTED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.SEARCH_SELECTED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.WISHLIST_ITEM_SELECTED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.BESTSELLER_HOME;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.DEMO_HOME;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.PRODUCT_VIEWED;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.SEPERATOR;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.SEPERATOR_UNDERSCORE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils.WEBSITE.TEMP;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.AnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser.ChromeParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.services.ShoppingAccessibilityService;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.WebSiteViewActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by aditya.amartya on 27/09/18
 */
public class ShoppingUtils {

    public enum WEBSITE {
        AMAZON("https://www.amazon.in/", "Amazon", "https://www.amazon.in/s/?keywords=", R.drawable.amazon, ""),
        FLIPKART("https://www.flipkart.com", "Flipkart", "https://www.flipkart.com/search?q=", R.drawable.flipkart, ""),
        SNAPDEAL("https://m.snapdeal.com", "Snapdeal", "https://m.snapdeal.com/search?keyword=", R.drawable.snapdeal, ""),
        MYNTRA("https://www.myntra.com/", "Myntra", "https://www.myntra.com/", R.drawable.myntra, ""),
        PAYTM("https://paytm.com/", "Paytm", "https://paytmmall.com/shop/search?q=", R.drawable.paytm, ""),
        JABONG("https://www.jabong.com/", "Jabong", "https://www.jabong.com/find/", R.drawable.jabong_logo, ""),
        AJIO("https://www.ajio.com/", "AJIO", "https://www.ajio.com/search/?text=", R.drawable.ajio_logo, ""),
        TEMP("", "NA", "", 0, "");

        private String url;
        private String name;
        private String searchQuery;
        private int drawableId;

        public String getSource() {
            return TextUtils.isEmpty(source) ? "NA" : source;

        }

        private String source;


        public String getTempURL() {
            return tempURL;
        }

        public void setTempURL(String tempURL) {
            this.tempURL = tempURL;
        }

        private String tempURL;

        WEBSITE(final String url, final String name, final String searchQuery, final int drawableId, final String tempURL) {
            this.url = url;
            this.name = name;
            this.searchQuery = searchQuery;
            this.drawableId = drawableId;
            this.tempURL = tempURL;
        }

        public void setUrl(final String url) {
            this.url = url;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public String getValue() {
            return url;
        }

        @Override
        public String toString() {
            return name;
        }

        public String getSearchQuery() {
            return searchQuery;
        }

        public void setSearchQuery(String searchQuery) {
            this.searchQuery = searchQuery;
        }

        public int getDrawableId() {
            return drawableId;
        }

        public void setDrawableId(int drawableId) {
            this.drawableId = drawableId;
        }

        public static WEBSITE[] dataValues() {
            return Arrays.copyOf(WEBSITE.values(), WEBSITE.values().length - 3);
        }

        public static WEBSITE getWebsite(String url, String source) {
            if (!TextUtils.isEmpty(url)) {
                for (WEBSITE website : WEBSITE.values()) {
                    if (url.contains(website.name.toLowerCase())) {
                        website.setTempURL(url);
                        website.source = source;
                        return website;
                    }
                }
            }
            TEMP.setTempURL(url);
            return TEMP;
        }

        public static WEBSITE getWebsite(Product product) {
            if (product != null && !TextUtils.isEmpty(product.productURL)) {
                for (WEBSITE website : WEBSITE.values()) {
                    if (product.productURL.contains(website.name.toLowerCase())) {
                        website.setTempURL(product.productURL);
                        website.source = product.productOrigin;
                        return website;
                    }
                }
            }
            TEMP.setTempURL(product.productURL);
            return TEMP;
        }

    }


    public static boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        final String service = mContext.getPackageName() + "/" + ShoppingAccessibilityService.class.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            //Log.v(TAG, "accessibilityEnabled = " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            //Log.e(TAG, "Error finding setting, default accessibility to not found: "+ e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            //Log.v(TAG, "***ACCESSIBILITY IS ENABLED*** -----------------");
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    //Log.v(TAG, "-------------- > accessibilityService :: " + accessibilityService + " " + service);
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        //Log.v(TAG, "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        } else {
            //Log.v(TAG, "***ACCESSIBILITY IS DISABLED***");
        }

        return false;
    }

    public static String getBrandFromTitle(Context context, String title) {
        String expectedBrand = title.split(" ")[0];
        String brand = null;
        InputStream inputStream = context.getResources().openRawResource(R.raw.amazonbrandsdd);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        try {
            line = reader.readLine();
        } catch (IOException e) {
        }

        ArrayList<String> brandList = null;
        if (line != null) {
            brandList = new ArrayList<>(Arrays.asList(line.split(" \\*\\** ")));
        }

        ArrayList<String> brandsMatched = new ArrayList<>();
        if (brandList != null) {
            for (String s : brandList) {
                if (s.contains(expectedBrand)) {
                    brand = s;
                    break;
                }
                /*String titleArray[] = title.split(" ");
                for (String str : titleArray) {
                    if (str.equalsIgnoreCase(s)) {
                        //brandsMatched.add(s);
                        //brand = s;
                        //break;
                    }
                }*/
            }
            /*if (TextUtils.isEmpty(brand)) {
                for (String s : brandList) {
                    if (title.contains(s)) {
                        brand = s;
                        break;
                    }
                }
            }*/
        }
        return brand;
    }

    public static int dpToPixel(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static final int getDisplayWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }

    public static final int getDisplayHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int matchedTextsPercentage(String prev, String current) {
        int matchCount = 0;
        String words[] = prev.split("\\s+");
        HashSet<String> stringHashSet = new HashSet<>(Arrays.asList(words));
        for (String word : stringHashSet) {
            if (current.contains(word)) {
                matchCount++;
            }
        }
        return matchCount > 0 ? ((matchCount * 100) / (stringHashSet.size())) : 0;
    }

    public static boolean isSupportedPackage(String packageName) {
        for (String name : AssistantConstants.packageNamesList) {
            if (name.equalsIgnoreCase(packageName)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMandatoryPermissionsGranted(Context context) {
        boolean isAccessibilityServiceOn = ShoppingUtils.isAccessibilitySettingsOn(context.getApplicationContext());
        boolean isDrawOverOtherAppsPermissionGranted = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(context);

        return isAccessibilityServiceOn && isDrawOverOtherAppsPermissionGranted;
    }

    public static void launchProductPage(Context context, String url) {
        launchProductPageInApp(context, url);
    }

    public static void launchProductPageInApp(Context context, String url) {
        Intent intent = new Intent(context, WebSiteViewActivity.class);
        intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, WEBSITE.getWebsite(url, "NA"));
        intent.putExtra(WebSiteViewActivity.IS_DIRECT_DETAIL_PAGE, true);
        context.startActivity(intent);
    }

    public static void launchProductPageInAppSearch(Context context, Product product) {
        if (product != null && !TextUtils.isEmpty(product.productURL)) {
            Intent intent = new Intent(context, WebSiteViewActivity.class);
            intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, WEBSITE.getWebsite(product));
            intent.putExtra(WebSiteViewActivity.IS_DIRECT_DETAIL_PAGE, true);
            context.startActivity(intent);

            double priceInNumericFormat = ChromeParser.getPriceInNumericFormat(
                    product.productPrice);
            AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, SEARCH,
                    PRODUCT_VIEWED + product.productOrigin + SEPERATOR + product.website.toString() + SEPERATOR + product.productName + SEPERATOR + priceInNumericFormat);

            Bundle bundle = new Bundle();
            bundle.putString("Store", product.website.toString());
            bundle.putString("Title", product.productName);
            bundle.putInt("Price", (int) priceInNumericFormat);
            bundle.putString("Placeholder", "search");

            FirebaseAnalyticsManager.getInstance().sendEvent(SEARCH_SELECTED, bundle);

        }
    }

    public static void launchProductPageInAppRV(Context context, Product product) {
        if (product != null && !TextUtils.isEmpty(product.productURL)) {

            Intent intent = new Intent(context, WebSiteViewActivity.class);
            intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, WEBSITE.getWebsite(product));
            intent.putExtra(WebSiteViewActivity.IS_DIRECT_DETAIL_PAGE, true);
            context.startActivity(intent);

            double priceInNumericFormat = ChromeParser.getPriceInNumericFormat(
                    product.productPrice);
            AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, RECENTLY_VIEWED,
                    PRODUCT_VIEWED + product.productOrigin + SEPERATOR + product.website.toString() + SEPERATOR + product.productName + SEPERATOR + priceInNumericFormat);

            Bundle bundle = new Bundle();
            bundle.putString("Store", product.website.toString());
            bundle.putString("Title", product.productName);
            bundle.putInt("Price", (int) priceInNumericFormat);
            bundle.putString("Placeholder", product.productOrigin);

            FirebaseAnalyticsManager.getInstance().sendEvent(RECENT_ITEM_SELECTED, bundle);
        }
    }

    public static void launchProductPageInAppWishlist(Context context, Product product) {
        if (product != null && !TextUtils.isEmpty(product.productURL)) {

            Intent intent = new Intent(context, WebSiteViewActivity.class);
            intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, WEBSITE.getWebsite(product));
            intent.putExtra(WebSiteViewActivity.IS_DIRECT_DETAIL_PAGE, true);
            context.startActivity(intent);

            double priceInNumericFormat = ChromeParser.getPriceInNumericFormat(
                    product.productPrice);
            AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, WISHLIST,
                    PRODUCT_VIEWED + product.productOrigin + SEPERATOR + product.website.toString() + SEPERATOR + product.productName + SEPERATOR + priceInNumericFormat);

            Bundle bundle = new Bundle();
            bundle.putString("Store", product.website.toString());
            bundle.putString("Title", product.productName);
            bundle.putInt("Price", (int) priceInNumericFormat);
            bundle.putString("Placeholder", product.productOrigin);

            FirebaseAnalyticsManager.getInstance().sendEvent(WISHLIST_ITEM_SELECTED, bundle);
        }
    }

    public static void launchProductPageInAppBestSeller(Context context, String productUrl, String productName) {
        if (!TextUtils.isEmpty(productUrl)) {
            AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, BESTSELLER,
                    PRODUCT_VIEWED + BESTSELLER_HOME + SEPERATOR + productName);
            Intent intent = new Intent(context, WebSiteViewActivity.class);
            intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, WEBSITE.getWebsite(productUrl, "BestSeller"));
            intent.putExtra(WebSiteViewActivity.IS_DIRECT_DETAIL_PAGE, true);
            context.startActivity(intent);

            Bundle bundle = new Bundle();
            bundle.putString("Store", "Bestseller");
            bundle.putString("Title", productName);
            bundle.putInt("Price", 0);
            bundle.putString("Placeholder", "Bestseller");

            FirebaseAnalyticsManager.getInstance().sendEvent(BESTSERLLER_SELECTED , bundle);
        }
    }

    public static void launchProductPageInDemo(Context context, String productUrl, String productName) {
        if (!TextUtils.isEmpty(productUrl)) {

            Intent intent = new Intent(context, WebSiteViewActivity.class);
            intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, WEBSITE.getWebsite(productUrl, "DEMO"));
            intent.putExtra(WebSiteViewActivity.IS_DIRECT_DETAIL_PAGE, true);
            intent.putExtra(WebSiteViewActivity.IS_DEMO, true);
            context.startActivity(intent);

            AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, DEMO,
                    PRODUCT_VIEWED + DEMO_HOME + SEPERATOR + productName);

            Bundle bundle = new Bundle();
            bundle.putString("Store", "DEMO");
            bundle.putString("Title", productName);
            bundle.putInt("Price", 0);
            bundle.putString("Placeholder", "DEMO");

            FirebaseAnalyticsManager.getInstance().sendEvent(DEMO_SELECTED, bundle);
        }
    }

    public static void launchProductPageInAppSuggestions(Context context, Product product, int oldPrice) {
        if (product != null && !TextUtils.isEmpty(product.productURL)) {
            Intent intent = new Intent(context, WebSiteViewActivity.class);
            intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, WEBSITE.getWebsite(product));
            intent.putExtra(WebSiteViewActivity.IS_DIRECT_DETAIL_PAGE, true);
            context.startActivity(intent);
            double priceInNumericFormat = ChromeParser.getPriceInNumericFormat(
                    product.productPrice);
            AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, COMPARE,
                    PRODUCT_VIEWED + product.productOrigin + SEPERATOR + product.website.toString() + SEPERATOR + product.productName + SEPERATOR + priceInNumericFormat);

            Bundle bundle = new Bundle();
            bundle.putString("Store", product.website.toString());
            bundle.putString("Title", product.productName);
            bundle.putInt("Price", (int) priceInNumericFormat);
            bundle.putString("Placeholder", product.productOrigin);

            FirebaseAnalyticsManager.getInstance().sendEvent(COMPARE_ITEM_SELECTED, bundle);
        }
    }

}
