package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.worker;

import android.support.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.notification.NotificationManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;

/**
 * Created by prince.sachdeva on 16/01/19.
 */

public class AccessibilityCheckJob extends Job {
    public static final String TAG = "accessibility_job_tag";

    @NonNull
    @Override
    protected Result onRunJob(@NonNull Params params) {
        Log.e("TAG", "dfgdf");
        if (!ShoppingUtils.isMandatoryPermissionsGranted(getContext())) {
            NotificationManager.createGrantPermissionsNotification(getContext());
        } else if (ShoppingUtils.isAccessibilitySettingsOn(getContext().getApplicationContext())) {
            Utils.startAccessibilityServiceIfStopped(getContext());
        }
        return Result.SUCCESS;
    }

}
