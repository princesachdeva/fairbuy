package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.ShoppingListAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentShopListBinding;

import java.io.Serializable;

/**
 * Created by aditya.amartya on 27/09/18
 */
public class ShopListFragment extends Fragment implements View.OnClickListener {

    private FragmentShopListBinding binding;
    private String[] apps = new String[]{"Flipkart", "Amazon", "Snapdeal", "Paytm", "Myntra", "Jabong"};
    private String[] appDescriptions = new String[]{"Most popular shopping website in India", "World's biggest shopping website", "Special offers website",
            "PayTm Karo", "Biggest clothing & apparels site", "Special offers on clothing"};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvShopList.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvShopList.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        binding.rvShopList.setAdapter(new ShoppingListAdapter(this));
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), WebSiteViewActivity.class);
        intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, (Serializable) v.getTag());
        v.getContext().startActivity(intent);
    }
}
