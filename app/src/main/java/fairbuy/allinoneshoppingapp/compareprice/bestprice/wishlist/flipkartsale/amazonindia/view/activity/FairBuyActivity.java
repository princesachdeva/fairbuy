package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication.getContext;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.SEARCH;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Category.PURCHASE_GOAL;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.*;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appsee.Appsee;

import java.util.List;
import java.util.concurrent.TimeUnit;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ActivityFairBuyBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.Constants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.AnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.navigation.Navigation;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.notification.NotificationManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser.ChromeParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.services.ShoppingAccessibilityService;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceKeys;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment.AskPermissionsFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment.HomeFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment.HomeMyOrdersFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment.HomeShoppingHistoryFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment.MyOrdersFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment.SearchFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment.WishlistFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.listeners.OnViewAllClickListener;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget.DragDropWidget;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.worker.AccessibilityCheckWorker;

/**
 * Created by aditya.amartya on 05/12/18
 */
public class FairBuyActivity extends BaseActivity implements HomeMyOrdersFragment.OnViewAllOrdersClickListener, OnViewAllClickListener {

    private BottomNavigationView bottomNavigationView;
    public static final String NAVIAGTION = "NAVIAGTION";
    private String searchCriteria;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_fair_buy);
        initToolBar(((ActivityFairBuyBinding) binding).toolbar, false, "Fair Buy");
        initBottomView();
        Utils.startAccessibilityServiceIfStopped(getContext());
        //  test();
        handleDeeplink();
    }

    private void test() {

        ChromeParser.parseURL("https://www.flipkart.com/l-oreal-paris-casting-creme-gloss-hair-color/p/itmfypzmpfufqbuh", new ChromeParser.OnProductPageListener() {
            @Override
            public void onProductPage(Product product) {
                if (product != null) {
                    Log.e("TAG", product.toString());
                }
            }
        });

        boolean myServiceRunning = isMyServiceRunning(ShoppingAccessibilityService.class);
        Log.e("TAG", myServiceRunning + "");
        AccessibilityCheckWorker.logApiRequestAndResponse(getApplicationContext(), "test", myServiceRunning + "FairBuyActivity");
    }


    private void initBottomView() {
        bottomNavigationView = ((ActivityFairBuyBinding) binding).bottomNavigationView;
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnNavigationItemReselectedListener(mOnNavigationItemReselectedListener);
    }


    private BottomNavigationView.OnNavigationItemReselectedListener mOnNavigationItemReselectedListener = new BottomNavigationView.OnNavigationItemReselectedListener() {
        @Override
        public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

        }
    };

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            switch (menuItem.getItemId()) {
                case R.id.home:
                    if (true) {
                        getSupportActionBar().show();
                        setToolbarLogo(true);
                        changeFragment(new HomeFragment(), Constants.HOME);
                        bottomNavigationView.setVisibility(View.VISIBLE);
                    } else {
                        getSupportActionBar().hide();
                        changeFragment(new AskPermissionsFragment(), Constants.HOME);
                        bottomNavigationView.setVisibility(View.GONE);
                    }
                    break;
                case R.id.wishlist:
                    WishlistFragment wishlistFragment = new WishlistFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(WishlistFragment.ARGS.IS_HOME, false);
                    wishlistFragment.setArguments(bundle);
                    changeFragment(wishlistFragment, Constants.WISHLIST);
                    break;
                case R.id.myOrders:
                    HomeShoppingHistoryFragment homeShoppingHistoryFragment = new HomeShoppingHistoryFragment();
                    Bundle bundleHistory = new Bundle();
                    bundleHistory.putBoolean(HomeShoppingHistoryFragment.ARGS.IS_HOME, false);
                    homeShoppingHistoryFragment.setArguments(bundleHistory);
                    changeFragment(homeShoppingHistoryFragment, Constants.RECENTLY_VIEWED);
                    break;
                case R.id.search:
                    AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, SEARCH, SEARCH_NAV);
                    FirebaseAnalyticsManager.getInstance().sendEvent(AnalyticsConstants.FirebaseEvents.SEARCH_NAV, null);
                    SearchFragment fragment = new SearchFragment();
                    if (!TextUtils.isEmpty(searchCriteria)) {
                        Bundle searchBundle = new Bundle();
                        searchBundle.putString(SearchFragment.SEARCH_TEXT, searchCriteria);
                        fragment.setArguments(searchBundle);
                        searchCriteria = null;
                    }
                    changeFragment(fragment, Constants.SEARCH);
                    break;
            }
            return true;
        }
    };

    @Override
    public void onBackPressed() {
        FragmentManager manger = getSupportFragmentManager();
        BaseFragment fragment = (BaseFragment) manger.findFragmentById(R.id.flContainer);
        if (fragment instanceof WishlistFragment || fragment instanceof MyOrdersFragment || fragment instanceof SearchFragment || fragment instanceof HomeShoppingHistoryFragment) {
            String name = getSupportFragmentManager().getBackStackEntryAt(1).getName();
            getSupportFragmentManager().popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            ((ActivityFairBuyBinding) binding).bottomNavigationView.getMenu().getItem(0).setChecked(true);
            return;
        } else if (fragment instanceof HomeFragment) {
            this.finish();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onViewAllOrdersClick() {
        bottomNavigationView.setSelectedItemId(R.id.myOrders);
    }

    @Override
    public void onViewAll(int viewAllType) {
        switch (viewAllType) {
            case OnViewAllClickListener.VIEW_ALL_WISHLIST:
                bottomNavigationView.setSelectedItemId(R.id.wishlist);
                break;
            case OnViewAllClickListener.VIEW_ALL_RECENTLY_VIEWED:
                bottomNavigationView.setSelectedItemId(R.id.myOrders);
                break;
            case OnViewAllClickListener.VIEW_ALL_SEARCH:
                bottomNavigationView.setSelectedItemId(R.id.search);
                break;
        }
    }

    public static boolean isMandatoryPermissionsGranted() {
        boolean isAccessibilityServiceOn = ShoppingUtils.isAccessibilitySettingsOn(getContext().getApplicationContext());
        boolean isDrawOverOtherAppsPermissionGranted = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? Settings.canDrawOverlays(getContext()) : true;

        return isAccessibilityServiceOn && isDrawOverOtherAppsPermissionGranted;
    }

    @Override
    public void onViewHome() {
        bottomNavigationView.setSelectedItemId(R.id.home);
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void showProgressBarForDelay() {
        final ProgressBar progressBar = ((ActivityFairBuyBinding) binding).progressBar;
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            }, 1000);
        }
    }

    private void handleDeeplink() {
        Intent intent = getIntent();
        if (intent != null) {
            Navigation navigation = getIntent().getParcelableExtra(NAVIAGTION);
            if (navigation != null) {
                switch (navigation.getMenuId()) {
                    case R.id.Compare:
                        if (navigation.getParams() != null && navigation.getParams().size() > 0) {
                            handleCompare(navigation.getParams().get(0));
                        }
                        break;
                    case R.id.Search:
                        if (navigation.getParams() != null && navigation.getParams().size() > 0) {
                            searchCriteria = navigation.getParams().get(0);
                        }
                        bottomNavigationView.setSelectedItemId(R.id.search);

                        break;
                    case R.id.Wishlist:
                        bottomNavigationView.setSelectedItemId(R.id.wishlist);
                        break;
                    case R.id.History:
                        bottomNavigationView.setSelectedItemId(R.id.myOrders);
                        break;
                    case R.id.Permission:
                        showPermissionDialog();
                        break;
                }
            }
        }
    }

    private void handleCompare(String title) {
        Product product = new Product();
        product.productName = title;
        product.website = ShoppingUtils.WEBSITE.TEMP;
        String searchString = title + " site:amazon.in OR site:snapdeal.com OR site:flipkart.com OR site:paytm.com OR site:myntra.com";
        DragDropWidget.getInstance(getApplication()).showOverlay(product, searchString, "Deeplink");
    }

    private void showAskPermissionDialog() {

        if (ShoppingAssistantApplication.showAskPermissionDialog) {
            long lastVisibleTime = PreferenceManager.getLongFromPreference(PreferenceKeys.LAST_TIME_ASK_PERMISSION_SHOWN, 0);
            long difference = System.currentTimeMillis() - lastVisibleTime;

            long hours = TimeUnit.MILLISECONDS.toHours(difference);
            if (hours > 24) {
                showPermissionDialog();
            }
        }
        ShoppingAssistantApplication.showAskPermissionDialog = false;
    }

    private void showPermissionDialog() {
        AskPermissionsFragment askPermissionsFragment = new AskPermissionsFragment();
        final FragmentManager supportFragmentManager = getSupportFragmentManager();
        askPermissionsFragment.show(supportFragmentManager, "TAG");
        supportFragmentManager.registerFragmentLifecycleCallbacks(new FragmentManager.FragmentLifecycleCallbacks() {
            @Override
            public void onFragmentViewDestroyed(@NonNull FragmentManager fm, @NonNull Fragment f) {
                super.onFragmentViewDestroyed(fm, f);
                if (f instanceof AskPermissionsFragment) {
                    supportFragmentManager.unregisterFragmentLifecycleCallbacks(this);
                    checkAutoStartSupportedOnDevice();
                }

            }
        }, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showAskPermissionDialog();
    }


    private void checkAutoStartSupportedOnDevice() {

        try {
            Intent intent = new Intent();
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }

            List<ResolveInfo> list = getContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                showAutoStartDialog(intent);
            }
        } catch (Exception e) {
            Log.e("exc", String.valueOf(e));
        }
    }


    protected void showAutoStartDialog(final Intent intent) {
        if (!PreferenceManager.get(PreferenceKeys.IS_AUTO_START_OK_CLICKED, false)) {
            final AlertDialog alertDialog = new AlertDialog.Builder(
                    this).create();

            View view = LayoutInflater.from(this).inflate(R.layout.autostart_dialog, null);
            alertDialog.setView(view);
            final SwitchCompat toggle_btn = view.findViewById(R.id.toggle_btn);

            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    final Handler handler = new Handler();

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            toggle_btn.setChecked(true);
                            toggle_btn.setClickable(false);
                        }
                    }, 1000);
                }
            });

            TextView btnOk = view.findViewById(R.id.tv_ok);
            TextView btnLater = view.findViewById(R.id.tv_later);


            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PreferenceManager.save(PreferenceKeys.IS_AUTO_START_OK_CLICKED, true);
                    startActivity(intent);
                    alertDialog.dismiss();
                }
            });
            btnLater.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Write your code here to execute after dialog closed
                    alertDialog.dismiss();
                }
            });

            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        }
    }

}
