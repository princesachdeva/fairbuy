package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;

/**
 * Created by prince.sachdeva on 13/02/19.
 */

public class FirebaseAnalyticsManager {

    private static FirebaseAnalyticsManager mInstance;
    private static FirebaseAnalytics mFirebaseAnalytics;

    public static FirebaseAnalyticsManager getInstance() {
        if (mInstance == null) {
            synchronized (FirebaseAnalyticsManager.class) {
                if (mInstance == null) {
                    mInstance = new FirebaseAnalyticsManager();
                }
            }
        }
        initalize();
        return mInstance;
    }

    private FirebaseAnalyticsManager() {

    }

    public static void initalize() {

        Context context = ShoppingAssistantApplication.getContext();
        if (context == null) {
            return;
        }
        if (mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        }
    }

    public void sendEvent(String event, Bundle bundle) {

        if (mFirebaseAnalytics != null) {
            mFirebaseAnalytics.logEvent(event, bundle);
        }
    }

    public void setScreen(Activity activity, String screenName) {
        if (mFirebaseAnalytics != null) {
            mFirebaseAnalytics.setCurrentScreen(activity, screenName, null);
            Log.e("TAG1", screenName);
        }
    }
}
