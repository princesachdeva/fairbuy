package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.SEARCH;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Category.PURCHASE_GOAL;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.SEARCH_RESULT;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.SEARCH_TERM;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.*;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.SEPERATOR;

import android.app.Application;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.FlexLayoutAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.SearchResultAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistItem;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentSearchBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.AnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.GoogleSearch;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Item;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser.ChromeParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.DbCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.WishListRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.FairBuyActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget.GridItemDecorator;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.viewmodel.ShoppingViewModel;

/**
 * Created by aditya.amartya on 05/12/18
 */
public class SearchFragment extends BaseFragment implements View.OnClickListener, Observer<GoogleSearch> {

    public static final String SEARCH_TEXT = "SEARCH_TEXT";
    private ShoppingViewModel shoppingViewModel;
    private SearchResultAdapter adapter;
    private int urlCount;
    private ArrayList<Product> products;
    private SearchView searchView;
    private ArrayList<String> trendingList;
    private String[] trendingArray = new String[]{"livon serum", "boat headphones", "honor 7c mobile", "philips beard trimmer", "mi note 5 pro", "lakme eyeconic kajal"};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        products = new ArrayList<>();
        trendingList = new ArrayList<>(Arrays.asList(trendingArray));
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        shoppingViewModel = ViewModelProviders.of(this).get(ShoppingViewModel.class);
    }

    @Override
    protected int getResourceId() {
        setHasOptionsMenu(true);
        return R.layout.fragment_search;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!Utils.isNetworkAvailable(getActivity())) {
            binding.setVariable(BR.errorText, "Check your internet connection!");
            binding.setVariable(BR.status, 3);
        } else {
            /*binding.setVariable(BR.errorText, "Search product");
            binding.setVariable(BR.status, 4);*/
            binding.setVariable(BR.status, 5);

        }

        RecyclerView rvFlexTrending = ((FragmentSearchBinding) binding).rvFlexTrending;
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this.getActivity());
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        rvFlexTrending.setLayoutManager(layoutManager);
        FlexLayoutAdapter flexLayoutAdapter = new FlexLayoutAdapter(trendingList, this);
        rvFlexTrending.setAdapter(flexLayoutAdapter);


        binding.setVariable(BR.clickListener, this);
        ((FragmentSearchBinding) binding).rvSearch.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        //((FragmentSearchBinding) binding).rvSearch.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        ((FragmentSearchBinding) binding).rvSearch.addItemDecoration(new GridItemDecorator(1));
        adapter = new SearchResultAdapter(this);
        ((FragmentSearchBinding) binding).rvSearch.setAdapter(adapter);
        FirebaseAnalyticsManager.getInstance().setScreen(getActivity(), "Search");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.expandActionView();
        searchItem.setOnActionExpandListener(onActionExpandListener);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint("Search for brands & products");
        searchView.setOnQueryTextListener(queryTextListener);
        searchView.clearFocus();
        setFontOnSearchView();

        handleLinkingSearch();
    }

    private void handleLinkingSearch() {
        if (getArguments() != null && !TextUtils.isEmpty(getArguments().getString(SEARCH_TEXT))) {
            String string = getArguments().getString(SEARCH_TEXT);
            searchView.setQuery(string, true);
        }
    }

    private void setFontOnSearchView() {
        try {
            TextView searchTextView = (TextView)
                    searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            if (searchTextView != null) {
                Typeface primeTypeFace = Typeface.createFromAsset(getContext().getAssets(), "fonts/Archivo-Regular.ttf");
                searchTextView.setTypeface(primeTypeFace);
            }


        } catch (Exception e) {

        }
    }

    MenuItem.OnActionExpandListener onActionExpandListener = new MenuItem.OnActionExpandListener() {
        @Override
        public boolean onMenuItemActionExpand(MenuItem item) {
            return true;
        }

        @Override
        public boolean onMenuItemActionCollapse(MenuItem item) {
            if (getActivity() != null && getActivity() instanceof FairBuyActivity) {
                ((FairBuyActivity) getActivity()).onBackPressed();
            }
            return true;
        }
    };

    private SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            if (!TextUtils.isEmpty(s)) {
                initSearchApi(s);
                AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, SEARCH, SEARCH_KEYWORD + s);
                Bundle bundle = new Bundle();
                bundle.putString("Keyword", s);
                FirebaseAnalyticsManager.getInstance().sendEvent(AnalyticsConstants.FirebaseEvents.SEARCH, bundle);
            }
            //searchView.clearFocus();
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (TextUtils.isEmpty(s)) {
                binding.setVariable(BR.status, 5);
                binding.executePendingBindings();
            }
            return false;
        }
    };

    private void initSearchApi(String s) {
        try {
            ((FragmentSearchBinding) binding).setVariable(BR.status, 1);
            String searchString = s + " site:amazon.in OR site:snapdeal.com OR site:flipkart.com OR site:paytm.com OR site:myntra.com";
            shoppingViewModel.getSearchResult("https://www.googleapis.com/customsearch/v1?q=" + URLEncoder.encode(searchString, "UTF-8")
                    + "&cx=006274864137540837670:miaykmfzw6w&key=AIzaSyCXWYW8gqAW98GqnYPf3cpBcgI5_KE2RB0&alt=json");

            shoppingViewModel.getSearchLiveData().observe(SearchFragment.this, SearchFragment.this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.rl_item_search:
                showProgressBarForDelay();
                ShoppingUtils.launchProductPageInAppSearch(v.getContext(), (Product) v.getTag());
                break;
            case R.id.fab_wishlist_product:
                Product product = (Product) v.getTag(R.string.product_tag);
                final int position = (int) v.getTag(R.string.position_tag);
                if (product.isWishlisted) {
                    WishListRepository.getInstance((Application) ShoppingAssistantApplication.getContext()).deleteWishListItem(product.productURL, new DbCallBack() {
                        @Override
                        public void onQueryProcessed(final boolean isSuccess) {
                            v.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (isSuccess) {
                                        adapter.updateProductWishlistState(position, false);
                                    }
                                }
                            });
                        }
                    });

                } else {
                    WishlistItem wishlistItem = new WishlistItem();
                    wishlistItem.setItem(product.productName);
                    wishlistItem.setImageUrl(product.productImage);
                    wishlistItem.setProductUrl(product.productURL);
                    wishlistItem.setShoppingSite(product.webSiteName);
                    wishlistItem.setLastModifiedTime(System.currentTimeMillis());
                    WishListRepository.getInstance((Application) ShoppingAssistantApplication.getContext()).insertWishListItem(wishlistItem, new DbCallBack() {
                        @Override
                        public void onQueryProcessed(final boolean isSuccess) {
                            v.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (isSuccess) {
                                        //detailsBinding.rvOverlayDetails.getAdapter().notifyItemChanged(position);
                                        adapter.updateProductWishlistState(position, true);
                                    }
                                }
                            });
                        }
                    });
                }
                break;
            case R.id.llTryAgain:
                handleTryAgain();
                break;

            case R.id.tvFlexItemTrending:
                searchView.setQuery(v.getTag().toString(), true);
                break;
        }
    }

    private void handleTryAgain() {
        CharSequence query = searchView.getQuery();
        if (TextUtils.isEmpty(query)) {
            binding.setVariable(BR.status, 5);
            binding.executePendingBindings();
        } else {
            searchView.setQuery(query, true);
        }
    }

    @Override
    public void onChanged(@Nullable GoogleSearch googleSearch) {
        searchView.clearFocus();
        products.clear();
        if (googleSearch != null && googleSearch.getItems() != null && googleSearch.getItems().size() > 0) {
            urlCount = googleSearch.getItems().size();
            for (Item item : googleSearch.getItems()) {
                ChromeParser.parseURL(item.getLink(), productPageListener);
            }
        } else {
            if (!Utils.isNetworkAvailable(getActivity())) {
                binding.setVariable(BR.errorText, "Check your internet connection!");
                binding.setVariable(BR.status, 3);
            } else {
                AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, SEARCH, SEARCH_RESPONSE_ZERO + searchView.getQuery());
                binding.setVariable(BR.status, 2);
                binding.setVariable(BR.errorText, "No results found!");
                ((FragmentSearchBinding) binding).setVariable(BR.status, 2);
                sendResponseEvent(0);
            }
            //Toast.makeText(context, "No results", Toast.LENGTH_SHORT).show();
        }
    }

    private ChromeParser.OnProductPageListener productPageListener = new ChromeParser.OnProductPageListener() {
        @Override
        public void onProductPage(Product product) {
            products.add(product);
            if (products.size() == urlCount) {
                products.removeAll(Collections.singleton(null));
                if (products.size() > 0) {
                    FirebaseAnalyticsManager.getInstance().setScreen(getActivity(), "Search_Result");
                    AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, SEARCH, SEARCH_RESPONSE_COUNT + products.size() + SEPERATOR + searchView.getQuery());
                    ((FragmentSearchBinding) binding).setVariable(BR.status, 0);
                    adapter.setData(products);
                } else {
                    AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, SEARCH, SEARCH_RESPONSE_ZERO + searchView.getQuery());
                    binding.setVariable(BR.status, 2);
                    binding.setVariable(BR.errorText, "No results found!");
                }
                binding.executePendingBindings();
                sendResponseEvent(products.size());
            }
        }
    };

    private void sendResponseEvent(int count) {
        Bundle bundle = new Bundle();
        bundle.putString("keyword", searchView.getQuery().toString());
        bundle.putInt("Results", count);
        bundle.putString("type", "server");
        FirebaseAnalyticsManager.getInstance().sendEvent(SEARCH_RESULT, bundle);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (shoppingViewModel.getSearchLiveData() != null) {
            shoppingViewModel.getSearchLiveData().removeObserver(this);
        }
    }
}
