package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.navigation;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by prince.sachdeva on 12/02/19.
 */

public class Navigation implements Parcelable {
    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public ArrayList<String> getParams() {
        return params;
    }

    public void setParams(ArrayList<String> params) {
        this.params = params;
    }

    private int menuId;
    private ArrayList<String> params;

    public Navigation() {

    }

    protected Navigation(Parcel in) {
        menuId = in.readInt();
        params = in.createStringArrayList();
    }

    public static final Creator<Navigation> CREATOR = new Creator<Navigation>() {
        @Override
        public Navigation createFromParcel(Parcel in) {
            return new Navigation(in);
        }

        @Override
        public Navigation[] newArray(int size) {
            return new Navigation[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(menuId);
        dest.writeStringList(params);
    }
}
