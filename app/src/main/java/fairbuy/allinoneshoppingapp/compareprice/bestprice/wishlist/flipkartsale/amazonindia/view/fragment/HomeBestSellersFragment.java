package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.BestSellerAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.BestSellerRecyclerViewBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentHomeBestsellersBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView.OverlayWindow;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget.DragDropWidget;

/**
 * Created by prince.sachdeva on 19/01/19.
 */

public class HomeBestSellersFragment extends BaseFragment implements View.OnClickListener {
    private OverlayWindow overlayWindow;
    private DragDropWidget dragDropWidget;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            dragDropWidget = DragDropWidget.getInstance(getActivity().getApplication());
        }
    }

    @Override
    protected int getResourceId() {
        return R.layout.fragment_home_bestsellers;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            getActivity().registerReceiver(dragDropWidget.getBroadcastReceiver(), new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        JSONObject jsonObject = ((ShoppingAssistantApplication) ShoppingAssistantApplication.getContext()).getBestSellersData();
        if (jsonObject != null) {
            Iterator<String> iter = jsonObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                JSONArray jsonArray = jsonObject.optJSONArray(key);
                if (jsonArray != null && !TextUtils.isEmpty(key)) {
                    addBestSellersView(key, jsonArray);
                }
            }
            binding.executePendingBindings();
        }
    }

    public void addBestSellersView(String key, JSONArray jsonArray) {
        LinearLayout linearLayout = ((FragmentHomeBestsellersBinding) binding).llContainer;

        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.best_seller_recycler_view, linearLayout, false);
        ((LinearLayout) linearLayout).addView(viewDataBinding.getRoot());

        BestSellerAdapter bestSellerAdapter = new BestSellerAdapter(this);
        bestSellerAdapter.updateData(jsonArray);

        RecyclerView rvBestSeller = ((BestSellerRecyclerViewBinding) viewDataBinding).rvStatus;
        rvBestSeller.setAdapter(bestSellerAdapter);

        viewDataBinding.setVariable(BR.title, key + " (Best Seller's)");
        viewDataBinding.executePendingBindings();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCompare:
                String title = (String) v.getTag();
                if (!TextUtils.isEmpty(title)) {
                    Product product = new Product();
                    product.productName = title;
                    product.website = ShoppingUtils.WEBSITE.TEMP;
                    String searchString = title + " site:amazon.in OR site:snapdeal.com OR site:flipkart.com OR site:paytm.com OR site:myntra.com";
                    dragDropWidget.showOverlay(product, searchString, "BestSeller");

                }
                break;
            case R.id.cardView:
                JSONObject jsonObject = (JSONObject) v.getTag();
                if (jsonObject != null) {
                    showProgressBarForDelay();
                    ShoppingUtils.launchProductPageInAppBestSeller(v.getContext(), jsonObject.optString("http"), jsonObject.optString("text"));
                }

                break;


        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null) {
            getActivity().unregisterReceiver(dragDropWidget.getBroadcastReceiver());
        }
    }
}
