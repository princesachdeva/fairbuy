package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.ShoppingDatabase;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistItem;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistDao;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;


public class WishListRepository {
    private static volatile WishListRepository instance;
    private WishlistDao wishlistDao;
    private ExecutorService executorService;

    private WishListRepository(Application application) {
        wishlistDao = ShoppingDatabase.getInstance(application).getWishlistDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public static WishListRepository getInstance(Application application) {
        WishListRepository localInstance = instance;
        if (localInstance == null) {
            synchronized (WishListRepository.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new WishListRepository(application);
                }
            }
        }
        return localInstance;
    }

    public LiveData<List<WishlistItem>> getWishList() {
        return wishlistDao.getWishList();
    }

    public LiveData<List<WishlistItem>> getWishListHome() {
        return wishlistDao.getWishListHome();
    }

    public void insertWishListItem(final WishlistItem wishlist, final DbCallBack dbCallBack) {
        if (wishlist != null && !TextUtils.isEmpty(wishlist.getProductUrl())) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    long id = wishlistDao.insert(wishlist);
                    dbCallBack.onQueryProcessed(id > 0);
                }
            });
        } else {
            dbCallBack.onQueryProcessed(false);
        }

    }

    public void deleteWishListItem(final String productUrl, final DbCallBack dbCallBack) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                wishlistDao.deleteByProductUrl(Utils.getUrlWithoutParameters(productUrl));

                dbCallBack.onQueryProcessed(true);
            }
        });
    }

    /*public void isWishlisted(final String productUrl, final DbCallBack dbCallBack) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                int isWishlisted = wishlistDao.isWishListedUiThread(productUrl);
                dbCallBack.onQueryProcessed(isWishlisted > 0);
            }
        });
    }*/

    public boolean isWishlisted(final String productUrl) {
        int isWishlisted = wishlistDao.isWishListedUiThread(Utils.getUrlWithoutParameters(productUrl));
        return isWishlisted > 0;
    }

    public void isWishlisted(final String productUrl, final DbCallBack dbCallBack) {
        final LiveData<Integer> data = wishlistDao.isWishListed(Utils.getUrlWithoutParameters(productUrl));
        data.observeForever(new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                data.removeObserver(this);
                if (integer != null) {
                    dbCallBack.onQueryProcessed(integer != 0);
                } else {
                    dbCallBack.onQueryProcessed(false);
                }
            }
        });
    }

    public LiveData<Integer> getCount() {
        return wishlistDao.getCount();

    }
}
