package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.repository.SmsRepository;

import java.util.List;

/**
 * Created by aditya.amartya on 21/11/18
 */
public class SmsViewModel extends AndroidViewModel {
    private SmsRepository smsRepository;

    public SmsViewModel(@NonNull Application application) {
        super(application);
        smsRepository = SmsRepository.getInstance(application);
    }

    public LiveData<List<SmsProductData>> getLiveData() {
        return smsRepository.getSmsData();
    }

    public LiveData<List<SmsProductData>> getLiveData(int maxCount) {
        return smsRepository.getSmsData(maxCount);
    }

    public void insertSmsData(SmsProductData smsData) {
        smsRepository.insertSmsData(smsData);
    }

    public LiveData<Integer> getCount() {
       return smsRepository.getCount();
    }
}
