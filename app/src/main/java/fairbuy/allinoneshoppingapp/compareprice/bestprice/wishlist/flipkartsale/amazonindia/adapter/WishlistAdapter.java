package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;

/**
 * Created by prince.sachdeva on 14/12/18.
 */

public class WishlistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public static final int VIEW_TYPE_DISPLAY_DATA = 1;
    public static final int VIEW_TYPE_EMPTY_LIST = 2;
    public static final int VIEW_TYPE_LOADING = 3;
    public static final int VIEW_TYPE_VIEW_ALL = 4;
    public static final int VIEW_TYPE_NO_INTERNET = 4;

    private int index;
    private boolean isHome;

    public WishlistAdapter(View.OnClickListener onClickListener, boolean isHome) {
        this.mOnClickListener = onClickListener;
        this.isHome = isHome;
    }


    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    private int viewType = VIEW_TYPE_LOADING;
    private ArrayList<Product> mProductArrayList;
    private View.OnClickListener mOnClickListener;


    public void updateData(List<Product> productList) {
        if (this.mProductArrayList == null) {
            this.mProductArrayList = new ArrayList<>();
        } else {
            this.mProductArrayList.clear();
        }
        if (productList == null || productList.size() == 0) {
            return;
        }
        setViewType(VIEW_TYPE_DISPLAY_DATA);
        int start = getItemCount();
        this.mProductArrayList.addAll(productList);
        notifyItemRangeChanged(start, productList.size());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (viewType == VIEW_TYPE_DISPLAY_DATA) {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_wishlist, viewGroup, false);
            binding.setVariable(BR.clickListener, mOnClickListener);
            return new WishlistItemHolder(binding);
        } else if (viewType == VIEW_TYPE_EMPTY_LIST) {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),   R.layout.item_wishlist_empty, viewGroup,
                    false);
            return new EmptyViewHolder(binding);
        } else if (viewType == VIEW_TYPE_NO_INTERNET) {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_no_internet, viewGroup,
                    false);
            binding.setVariable(BR.clickListener, mOnClickListener);
            return new EmptyViewHolder(binding);
        } else {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.progress_bar, viewGroup, false);
            return new ProgressViewHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof WishlistItemHolder) {
            WishlistItemHolder holder = (WishlistItemHolder) viewHolder;
            Product product = mProductArrayList.get(i);
            holder.binding.setVariable(BR.isHome, isHome);
            holder.binding.setVariable(BR.product, product);
            holder.binding.setVariable(BR.drawableId, product.getAppIcon());
            holder.binding.executePendingBindings();
        }
    }

    @Override
    public int getItemCount() {
        if (viewType == VIEW_TYPE_DISPLAY_DATA) {
            return mProductArrayList != null ? mProductArrayList.size() : 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }

    public static class WishlistItemHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public WishlistItemHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class EmptyViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public EmptyViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public ProgressViewHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public int removeItem(Product product) {
        index = mProductArrayList.indexOf(product);
        mProductArrayList.remove(index);
        if (getItemCount() == 0) {
            viewType = VIEW_TYPE_EMPTY_LIST;
            notifyDataSetChanged();
        } else {
            notifyItemRemoved(index);
        }
        return index;
    }

    public void addDeletedItem(Product product) {
        viewType = VIEW_TYPE_DISPLAY_DATA;
        mProductArrayList.add(index, product);
        if (mProductArrayList.size() == 1) {
            notifyDataSetChanged();
        } else {
            notifyItemInserted(index);
        }
    }
}
