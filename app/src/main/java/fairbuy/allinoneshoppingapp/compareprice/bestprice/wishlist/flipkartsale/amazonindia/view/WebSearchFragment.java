package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.analytics.MoEAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.SearchHistory;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistItem;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentWebSearchBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.activity.OverlayDetailActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.FetchBitmap;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.AnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser.ChromeParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.DbCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.WishListRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView.CustomSwipeRefreshLayout;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget.DragDropWidget;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget.FabUtilNative;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.viewmodel.ShoppingViewModel;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.BUBBLE_CLICK;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.BUBBLE_COMPARE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.BUBBLE_SHOW;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.BUBBLE_WISHLIST_INAPP;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.WEBVIEW;

/**
 * Created by aditya.amartya on 05/10/18
 */
public class WebSearchFragment extends Fragment implements FetchBitmap.BitmapListener, CustomSwipeRefreshLayout.CanChildScrollUpCallback {
    private FragmentWebSearchBinding binding;
    Handler handlerForJavascriptInterface = new Handler();
    public static final String BUNDLE_COMPANY = "BUNDLE_COMPANY";
    public static final String BUNDLE_SEARCH_KEYWORD = "BUNDLE_SEARCH_KEYWORD";
    public static final String IS_DIRECT_DETAIL_PAGE = "IS_DIRECT_DETAIL_PAGE";
    public static final String IS_DEMO = "IS_DEMO";
    private ShoppingUtils.WEBSITE site_type;
    private String prevProductName;
    private ShoppingViewModel viewModel;
    private boolean isBrandDeduced, isOnDetailPage;
    private boolean isCategoryDeduced;
    private Product product;
    private DragDropWidget dragDropWidget;
    private boolean isDirectDetailPage;
    private FabUtilNative fabUtil;
    private boolean isWishlisted;
    private String searchString;
    private boolean isDemo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            dragDropWidget = DragDropWidget.getInstance(getActivity().getApplication());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_web_search, container, false);
        binding.setVariable(BR.clickListener, onClickListener);
        if (getActivity() != null) {
            //getActivity().registerReceiver(dragDropWidget.getBroadcastReceiver(), new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(ShoppingViewModel.class);
        initWebView();
    }

    private void initWebView() {
        site_type = (ShoppingUtils.WEBSITE) getArguments().getSerializable(BUNDLE_COMPANY);
        String searchKeyword = getArguments().getString(BUNDLE_SEARCH_KEYWORD, "");
        isDirectDetailPage = getArguments().getBoolean(IS_DIRECT_DETAIL_PAGE, false);
        isDemo = getArguments().getBoolean(IS_DEMO, false);
        if (isDirectDetailPage && !TextUtils.isEmpty(site_type.getTempURL())) {
            binding.webview.loadUrl(Utils.getAffilatedURL(site_type.getTempURL()));
        } else if (!TextUtils.isEmpty(searchKeyword)) {
            binding.webview.loadUrl(site_type.getSearchQuery() + searchKeyword);
        } else {
            binding.webview.loadUrl(Utils.getAffilatedURL(site_type.getValue()));
        }

        binding.swipeRefreshLayout.setCanChildScrollUpCallback(this);
        binding.webview.getSettings().setJavaScriptEnabled(true);
        binding.webview.getSettings().setDomStorageEnabled(true);
        binding.webview.addJavascriptInterface(new MyJavaScriptInterface(getActivity()), "HtmlViewer");

        binding.webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(final WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (TextUtils.isEmpty(view.getOriginalUrl())) {
                    return;
                }
                // Flipkart
                boolean condition = newProgress == 100 && ((view.getOriginalUrl().contains("/p/") && site_type == ShoppingUtils.WEBSITE.FLIPKART) ||
                        (view.getOriginalUrl().contains("/product/") && site_type == ShoppingUtils.WEBSITE.SNAPDEAL) ||
                        ((view.getOriginalUrl().contains("-pdp") || view.getOriginalUrl().contains("/p/")) && site_type == ShoppingUtils.WEBSITE.PAYTM) ||
                        (view.getOriginalUrl().contains("/p/") && site_type == ShoppingUtils.WEBSITE.AJIO) ||
                        (site_type == ShoppingUtils.WEBSITE.JABONG) ||
                        (view.getOriginalUrl().contains("/buy") && site_type == ShoppingUtils.WEBSITE.MYNTRA));
                if (condition && binding != null && binding.webview != null && binding.webview.getHandler() != null) {
                    binding.webview.getHandler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                binding.webview.loadUrl("javascript:window.HtmlViewer.showHTML" +
                                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                            } catch (Exception e) {

                            }
                        }
                    }, 2000);
                } else {
                    if (site_type != ShoppingUtils.WEBSITE.AMAZON) {
                        onDetailPage(false);
                    }
                }

            }
        });
        binding.webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showHideToolBarProgress(true);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                showHideToolBarProgress(false);
                if (site_type == ShoppingUtils.WEBSITE.AMAZON) {
                    // Amazon
                    boolean contains = url.contains("/dp/");
                    if (contains && view != null) {
                        try {
                            view.loadUrl("javascript:window.HtmlViewer.showHTML" +
                                    "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                        } catch (Exception e) {

                        }
                    } else {
                        onDetailPage(false);
                    }
                }
            }
        });

        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                binding.webview.reload();
                binding.swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    class MyJavaScriptInterface {
        private Context ctx;

        MyJavaScriptInterface(Context ctx) {
            this.ctx = ctx;
        }

        @JavascriptInterface
        public void showHTML(final String html) {
            handlerForJavascriptInterface.post(new Runnable() {
                @Override
                public void run() {
                    product = new Product();
                    product.isNative = false;
                    try {
                        org.jsoup.nodes.Document document = Jsoup.parse(html);
                        if (site_type == ShoppingUtils.WEBSITE.AMAZON) {
                            product.productName = document.getElementById("title").text();
                            product.productImage = document.getElementById("main-image").attr("src");
                            product.productPrice = getAmazonProductPrice(document);
                            product.productBrand = document.getElementById("bylineInfo").text();
                            product.productFeatures = document.getElementsByClass("a-list-item").text();
                            product.rating = null;
                            product.ratedUsersCount = document.getElementById("acrCustomerReviewLink").text();
                        } else if (site_type == ShoppingUtils.WEBSITE.FLIPKART) {
                            product.productName = document.getElementsByClass("-vBYKX").text();
                            product.productImage = document.getElementsByClass("_2lhqqr _32mw3p").get(0).attr("src");
                            product.productPrice = document.getElementsByClass("_14X7rf").text();
                            product.productBrand = null;
                            product.productFeatures = document.getElementsByClass("b0xpwc").text();
                            product.rating = document.getElementsByClass("nj_v5O").text();
                            product.ratedUsersCount = document.getElementsByClass("mbaARG").text();
                        } else if (site_type == ShoppingUtils.WEBSITE.SNAPDEAL) {
                            product.productName = document.getElementsByClass("product-title").text();
                            product.productImage = document.getElementsByClass("lazy-load").get(0).attr("src");
                            product.productPrice = document.getElementById("sdPrice").text();
                            product.productBrand = null;
                            product.productFeatures = document.getElementsByClass("highlights").text();
                            product.rating = document.getElementsByClass("rating-content").get(0).text();
                            product.ratedUsersCount = document.getElementsByClass("rating-reviews").text();
                        } else if (site_type == ShoppingUtils.WEBSITE.PAYTM) {
                            product.productName = document.getElementsByClass("_2RLZ").text();
                            product.productImage = document.getElementsByClass("_3u24").get(0).getElementsByTag("img").attr("src");
                            product.productPrice = document.getElementsByClass("_1vi8").get(0).text();
                            product.productBrand = document.getElementsByClass("_1NCY").text();
                            product.productFeatures = document.getElementsByClass("_9-yD").text();
                            product.rating = null;
                            product.ratedUsersCount = null;
                        } else if (site_type == ShoppingUtils.WEBSITE.JABONG) {
                            product.productName = document.getElementsByClass("prdt-tlt").text();
                            product.productImage = document.getElementsByClass("carousal loaded").attr("src");
                            product.productPrice = document.getElementsByClass("sell-prc-lg").text();
                            product.productBrand = document.getElementsByClass("over-brd-lg").text();
                            product.productFeatures = document.getElementsByClass("product-attr").text();
                            product.rating = null;
                            product.ratedUsersCount = null;
                        } else if (site_type == ShoppingUtils.WEBSITE.AJIO) {
                            product.productName = document.getElementsByClass("prod-name").text();
                            product.productImage = document.getElementsByClass("rilrtl-lazy-img img-alignment rilrtl-lazy-img-loaded").get(0).attr("src");
                            product.productPrice = document.getElementsByClass("prod-sp").text();
                            product.productBrand = null;
                            product.productFeatures = document.getElementsByClass("prod-list").text();
                            product.rating = null;
                            product.ratedUsersCount = null;
                        } else if (site_type == ShoppingUtils.WEBSITE.MYNTRA) {
                            product.productName = document.getElementsByClass("product-detail").text();
                            product.productImage = document.getElementsByClass("img img-responsive img-loaded").get(0).attr("src");
                            product.productPrice = document.getElementsByClass("price").get(0).text();
                            product.productBrand = null;
                            product.productFeatures = document.getElementsByClass("description").text();
                            product.rating = null;
                            product.ratedUsersCount = null;
                        }
                    } catch (Exception e) {

                    }
                    if (!TextUtils.isEmpty(product.productName)) {
                        if (site_type == ShoppingUtils.WEBSITE.JABONG && TextUtils.isEmpty(product.productImage)) {
                            return;
                        }
                        isBrandDeduced = true;
                        isCategoryDeduced = true;


                        SearchHistory searchHistory = new SearchHistory();
                        searchHistory.setItem(product.productName);
                        searchHistory.setBrand(product.productBrand);
                        searchHistory.setProductUrl(binding.webview.getOriginalUrl());
                        searchHistory.setImageUrl(product.productImage);
                        searchHistory.setShoppingSite(site_type.toString());
                        if (!product.productName.equalsIgnoreCase(prevProductName)) {
                            sendGAOnFabShow();
                            viewModel.insertItem(searchHistory);
                        }

                        product.webSiteName = site_type.toString();
                        product.productURL = binding.webview.getOriginalUrl();
                        product.website = site_type;

                        // new FetchBitmap(WebSearchFragment.this).execute(product.productImage);

                        prevProductName = product.productName;
                        postEvent();
                        onDetailPage(true);
                    }
                }
            });
        }
    }

    private void onDetailPage(boolean isOnDetailPage) {
        Log.e("TAG", isOnDetailPage ? "On Detail Page" + product.toString() : "Exit Detail Page");
        this.isOnDetailPage = isOnDetailPage;
        //if (dragDropWidget != null) {
        if (isOnDetailPage) {
            if (product != null && !TextUtils.isEmpty(product.productURL)) {
                WishListRepository.getInstance(ShoppingAssistantApplication.getInstance()).isWishlisted(product.productURL, new DbCallBack() {
                    @Override
                    public void onQueryProcessed(boolean isSuccess) {
                        isWishlisted = isSuccess;
                    }
                });
            }
            if (product != null && !TextUtils.isEmpty(product.productName)) {
                searchString = product.productName + " site:amazon.in OR site:snapdeal.com OR site:flipkart.com OR site:paytm.com OR site:myntra.com";
            }
            binding.setVariable(BR.closeFbSuggestion, false);
            binding.llOverLay.setVisibility(View.VISIBLE);
            binding.llOverLay.setOnClickListener(onClickListener);
            //dragDropWidget.showAssistant(product);
        } else {
            if (binding != null && binding.llOverLay != null) {
                binding.llOverLay.setVisibility(View.GONE);
            }
            //dragDropWidget.removeDragAndDropViews();
        }
        //}
    }

    private String getAmazonProductPrice(Document document) {
        String productPrice = null;
        try {
            productPrice = document.getElementById("priceblock_ourprice").text();
        } catch (Exception e) {

        }
        if (TextUtils.isEmpty(productPrice)) {
            try {
                productPrice = document.getElementsByClass("buyingPrice").get(0).text();
            } catch (Exception e) {

            }
        }
        return productPrice;
    }


    public void showHideToolBarProgress(boolean visibility) {
        binding.setVariable(BR.status, visibility);
        binding.executePendingBindings();
        // dataBinding.toolbarProgressBar.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }


    @Override
    public void setBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            // searchImageOnDevice(bitmap);
            // searchTextFromImage(bitmap);
        }
    }

    /*private void searchImageOnDevice(Bitmap bitmap) {
        showHideToolBarProgress(true);
        FirebaseVisionLabelDetectorOptions options = new FirebaseVisionLabelDetectorOptions.Builder().setConfidenceThreshold(0.5f).build();
        final FirebaseVisionLabelDetector detector = FirebaseVision.getInstance().getVisionLabelDetector(options);
        final FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        Task<List<FirebaseVisionLabel>> result =
                detector.detectInImage(image)
                        .addOnSuccessListener(
                                new OnSuccessListener<List<FirebaseVisionLabel>>() {
                                    @Override
                                    public void onSuccess(List<FirebaseVisionLabel> labels) {
                                        //... Task completed successfully
                                        showHideToolBarProgress(false);
                                        isCategoryDeduced = true;
                                        if (labels != null && labels.size() == 0) {
                                            // Toast.makeText(WebSiteViewActivity.this, "No result found", Toast.LENGTH_SHORT).show();
                                        } else {
                                            //resultAdapter.setFirebaseVisionLabels(labels);
                                            StringBuilder stringBuilder = new StringBuilder();
                                            for (FirebaseVisionLabel label : labels) {
                                                stringBuilder.append(label.getLabel()).append(" ").append(label.getConfidence()).append(",");
                                            }
                                            if (product != null) {
                                                product.deducedCategory = stringBuilder.toString();
                                            }
                                            postEvent();
                                            // Toast.makeText(WebSiteViewActivity.this, stringBuilder.toString(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }).
                        addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        // Task failed with an exception
                                        isCategoryDeduced = true;
                                        showHideToolBarProgress(false);
                                        postEvent();
                                        // Toast.makeText(WebSiteViewActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                    }
                                });
    }*/

   /* private void searchTextFromImage(Bitmap bitmap) {
        showHideToolBarProgress(true);
        final FirebaseVisionTextDetector textDetector = FirebaseVision.getInstance().getVisionTextDetector();

        final FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        textDetector.detectInImage(image).

                addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                    @Override
                    public void onSuccess(FirebaseVisionText firebaseVisionText) {
                        isBrandDeduced = true;
                        showHideToolBarProgress(false);
                        List<FirebaseVisionText.Block> blocks = firebaseVisionText.getBlocks();
                        StringBuilder recognisedText = new StringBuilder();
                        for (int i = 0; i < blocks.size(); i++) {
                            recognisedText.append(blocks.get(i).getText());
                            recognisedText.append("\n");
                        }
                        if (!TextUtils.isEmpty(recognisedText.toString())) {
                            if (product != null) {
                                product.deducedBrand = recognisedText.toString();
                            }
                        }
                        postEvent();
                    }
                }).

                addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showHideToolBarProgress(false);
                        isBrandDeduced = true;
                        postEvent();
                        //  Toast.makeText(WebSiteViewActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
    }*/

    public void onBackPressed() {
        if (fabUtil != null) {
            fabUtil.removeFabItems();
        }
        binding.llOverLay.setVisibility(View.GONE);
        if (binding.webview.canGoBack()) {
            binding.webview.goBack();
        } else {
            ((OnBackPressTaskCompltedListener) getActivity()).onBackPressTaskCompleted();
        }
    }

    public interface OnBackPressTaskCompltedListener {
        void onBackPressTaskCompleted();

    }

    public void postEvent() {
        try {
            if (isBrandDeduced && isCategoryDeduced && product != null) {
                isBrandDeduced = false;
                isCategoryDeduced = false;
                MoEAnalyticsManager.sendEvent(WebSearchFragment.this.getContext().getApplicationContext(), "Product", product);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        return binding.webview.getScrollY() > 0;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null) {
            //dragDropWidget.removeDragAndDropViews();
            //getActivity().unregisterReceiver(dragDropWidget.getBroadcastReceiver());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isOnDetailPage && dragDropWidget != null && !dragDropWidget.isFabVisible() && product != null) {
            //dragDropWidget.showAssistant(product);
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ll_overLay:
                    handleOverlayClick(v);
                    break;

                case R.id.ivCloseFb:
                    binding.setVariable(BR.closeFbSuggestion, true);
                    break;
            }
        }
    };

    private void handleOverlayClick(View v) {
        if (isDemo) {
            handleCompareClick();
        } else {
            binding.llOverLay.setVisibility(View.GONE);
            fabUtil = new FabUtilNative(binding.flParent, R.layout.view_overlay_options, onFabClickListener, R.id.ll_overlay_actions, R.id.fab_fb, R.id.flOverlayOptions);
            v.setVisibility(View.GONE);
            v.setOnClickListener(null);
            if (product != null) {
                fabUtil.changeWishlistIcon(isWishlisted, R.id.fab_wishlist);
            }
            sendGAOnFabClick();
        }
    }

    private FabUtilNative.OnFabClickListener onFabClickListener = new FabUtilNative.OnFabClickListener() {
        @Override
        public void onFabClick(final View view) {
            switch (view.getId()) {
                case R.id.llCompare:
                    handleCompareClick();
                    break;
                case R.id.fab_share:
                    fabUtil.removeFabItems();
                    break;
                case R.id.llWishlist:
                    if (product != null) {
                        //For chrome and inapp
                        sendGAOnWishlist();
                        if (isWishlisted) {
                            removeFromWishlist(view, product);
                        } else {
                            addToWishlist(view, product);
                        }
                    } else {
                        sendGAOnWishListError();
                    }
                    break;
                case R.id.llFb:
                    fabUtil.removeFabItems();
                    binding.llOverLay.setOnClickListener(onClickListener);
                    binding.llOverLay.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    private void handleCompareClick() {
        if (fabUtil != null) {
            fabUtil.removeFabItems();
        }
        if (product != null && !TextUtils.isEmpty(product.productName) && !TextUtils.isEmpty(searchString)) {
            Product productPojo = new Product();
            productPojo.productName = product.productName;
            productPojo.productBrand = product.productBrand;
            productPojo.productPrice = product.productPrice;
            productPojo.website = product.website;
            if (product.website == null || TextUtils.isEmpty(product.website.name())) {
                productPojo.website = ShoppingUtils.WEBSITE.TEMP;
            }
            if (product != null && !TextUtils.isEmpty(product.productImage)) {
                productPojo.productImage = product.productImage;
            }
            Intent intent = new Intent(WebSearchFragment.this.getActivity(), OverlayDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.putExtra(OverlayDetailActivity.Args.PRODUCT, productPojo);
            intent.putExtra(OverlayDetailActivity.Args.SEARCH_STRING, searchString);
            intent.putExtra(OverlayDetailActivity.Args.ORIGIN, "Bubble");
            WebSearchFragment.this.getActivity().startActivity(intent);
            WebSearchFragment.this.sendGAOnCompareClick();
        } else {
            //Toast.makeText(getApplicationContext(), "Please Scroll", //Toast.LENGTH_SHORT).show();
        }
        binding.llOverLay.setOnClickListener(onClickListener);
        binding.llOverLay.setVisibility(View.VISIBLE);
    }

    private void removeFromWishlist(final View view, Product product) {
        WishListRepository.getInstance(this.getActivity().getApplication()).deleteWishListItem(product.productURL, new DbCallBack() {
            @Override
            public void onQueryProcessed(final boolean isSuccess) {
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess) {
                            isWishlisted = false;
                            fabUtil.changeWishlistIcon(false, R.id.fab_wishlist);
                            //Toast.makeText(application, "This item has been removed from the wishlist.", Toast.LENGTH_SHORT).show();
                            //((ImageView) view).setImageResource(R.drawable.pop_up_wishlist);
                        }
                    }
                });
            }
        });
    }

    private void addToWishlist(final View view, Product product) {
        WishlistItem wishlistItem = new WishlistItem();
        wishlistItem.setItem(product.productName);
        wishlistItem.setImageUrl(product.productImage);
        wishlistItem.setProductUrl(product.productURL);
        wishlistItem.setShoppingSite(product.webSiteName);
        wishlistItem.setLastModifiedTime(System.currentTimeMillis());
        WishListRepository.getInstance(this.getActivity().getApplication()).insertWishListItem(wishlistItem, new DbCallBack() {
            @Override
            public void onQueryProcessed(final boolean isSuccess) {
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess) {
                            isWishlisted = true;
                            fabUtil.changeWishlistIcon(true, R.id.fab_wishlist);
                            //Toast.makeText(application, "You have been successfully wishlisted this item.", Toast.LENGTH_SHORT).show();
                            //((ImageView) view).setImageResource(R.drawable.pop_up_wishlisted);
                        }
                    }
                });
            }
        });
    }

    public void sendGAOnFabShow() {

        String placeholder = isDirectDetailPage ? site_type.getSource() : WEBVIEW;
        AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.BUBBLE,
                AnalyticsConstants.Label.BUBBLE_SHOW + site_type.toString() + AnalyticsConstants.SEPERATOR + placeholder + AnalyticsConstants.SEPERATOR
                        + product.productName);

        Bundle bundle = new Bundle();
        bundle.putString("Store", site_type.toString());
        bundle.putString("Title", product.productName);
        bundle.putString("Placeholder", placeholder);
        bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(product.productPrice));

        FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_SHOW, bundle);

    }


    public void sendGAOnFabClick() {
        String placeholder = isDirectDetailPage ? site_type.getSource() : WEBVIEW;
        AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.BUBBLE,
                AnalyticsConstants.Label.BUBBLE_CLICK + site_type.toString() + AnalyticsConstants.SEPERATOR + placeholder
                        + AnalyticsConstants.SEPERATOR + product.productName);

        Bundle bundle = new Bundle();
        bundle.putString("Store", site_type.toString());
        bundle.putString("Title", product.productName);
        bundle.putString("Placeholder", placeholder);
        bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(product.productPrice));


        FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_CLICK, bundle);
    }

    public void sendGAOnCompareClick() {

        String placeholder = isDirectDetailPage ? site_type.getSource() : WEBVIEW;
        AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.BUBBLE,
                AnalyticsConstants.Label.BUBBLE_COMPARE + site_type.toString() + AnalyticsConstants.SEPERATOR + placeholder
                        + AnalyticsConstants.SEPERATOR + product.productName);

        Bundle bundle = new Bundle();
        bundle.putString("Store", site_type.toString());
        bundle.putString("Title", product.productName);
        bundle.putString("Placeholder", placeholder);
        bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(product.productPrice));

        FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_COMPARE, bundle);

    }

    public void sendGAOnWishlist() {

        String placeholder = isDirectDetailPage ? site_type.getSource() : WEBVIEW;
        AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.WISHLISST_GOAL, AnalyticsConstants.Action.BUBBLE,
                AnalyticsConstants.Label.BUBBLE_WISHLIST + site_type.toString() + AnalyticsConstants.SEPERATOR + placeholder
                        + AnalyticsConstants.SEPERATOR + product.productName);

        Bundle bundle = new Bundle();
        bundle.putString("Store", site_type.toString());
        bundle.putString("Title", product.productName);
        bundle.putString("Placeholder", placeholder);
        bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(product.productPrice));

        FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_WISHLIST_INAPP, bundle);
    }

    public void sendGAOnWishListError() {

        AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.WISHLISST_GOAL, AnalyticsConstants.Action.BUBBLE,
                AnalyticsConstants.Label.BUBBLE_WISHLIST_ERROR + site_type.toString() + AnalyticsConstants.SEPERATOR + (isDirectDetailPage ? site_type.getSource() : WEBVIEW)
                        + AnalyticsConstants.SEPERATOR + product.productName);

    }

}
