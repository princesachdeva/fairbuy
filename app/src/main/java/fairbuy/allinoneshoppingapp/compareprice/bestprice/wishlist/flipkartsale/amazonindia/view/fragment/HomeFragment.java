package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.*;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Action.SELECT_STORE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Category.PURCHASE_GOAL;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.TimeUnit;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.ShoppingListAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentHomeBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.AnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceKeys;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.WebSiteViewActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.FairBuyActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.listeners.OnViewAllClickListener;

/**
 * Created by aditya.amartya on 05/12/18
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener {

    private FairBuyActivity onViewAllClickListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FairBuyActivity) {
            onViewAllClickListener = ((FairBuyActivity) context);
        }
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }


    @Override
    protected int getResourceId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((FragmentHomeBinding) binding).rvShoppingApps.setAdapter(new ShoppingListAdapter(this));
        binding.setVariable(BR.clickListener, this);

        FirebaseAnalyticsManager.getInstance().setScreen(getActivity(), "Home");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llContainer:
                if (v.getTag() != null && v.getTag() instanceof ShoppingUtils.WEBSITE) {
                    Intent intent = new Intent(getActivity(), WebSiteViewActivity.class);
                    ShoppingUtils.WEBSITE website = (ShoppingUtils.WEBSITE) v.getTag();
                    intent.putExtra(WebSiteViewActivity.BUNDLE_COMPANY, website);
                    v.getContext().startActivity(intent);
                    AnalyticsManager.getInstance().sendEvent(PURCHASE_GOAL, SELECT_STORE, AnalyticsConstants.Label.CLICK_STORE + website.toString());

                    Bundle bundle = new Bundle();
                    bundle.putString("Storelabel", website.toString());

                    FirebaseAnalyticsManager.getInstance().sendEvent(FirebaseEvents.SELECT_STORE, bundle);
                }
                break;
            case R.id.llSearchContainer:
                if (onViewAllClickListener != null) {
                    onViewAllClickListener.onViewAll(OnViewAllClickListener.VIEW_ALL_SEARCH);
                }
                break;
        }
    }
}
