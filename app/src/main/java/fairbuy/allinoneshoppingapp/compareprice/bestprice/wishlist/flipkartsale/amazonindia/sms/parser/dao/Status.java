package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao;

import java.io.Serializable;

/**
 * Created by prince.sachdeva on 22/11/18.
 */

public class Status implements Serializable {

    String status;
    long statusDate;
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(long statusDate) {
        this.statusDate = statusDate;
    }

    @Override
    public String toString() {
        return status + "   " + statusDate + "    " + message;
    }
}
