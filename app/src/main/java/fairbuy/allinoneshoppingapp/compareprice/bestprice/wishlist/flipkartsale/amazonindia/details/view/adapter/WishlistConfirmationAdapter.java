package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.details.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;

import java.util.ArrayList;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ItemWishlistConfirmationBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;

public class WishlistConfirmationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Product> products;
    private View.OnClickListener onClickListener;

    public WishlistConfirmationAdapter(ArrayList<Product> products, View.OnClickListener onClickListener) {
        this.products = products;
        this.onClickListener = onClickListener;
    }

    public void updateProductWishlistState(int position, boolean isWishlist) {
        if (products != null && products.size() > 0) {
            products.get(position).isWishlisted = isWishlist;
            notifyItemChanged(position);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_wishlist_confirmation, viewGroup, false);
        binding.setVariable(BR.clickListener, onClickListener);
        return new WishlistConfirmationHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        WishlistConfirmationHolder holder = (WishlistConfirmationHolder) viewHolder;
        Product product = products.get(position);
        holder.binding.getRoot().setTag(product.productURL);
        ((ItemWishlistConfirmationBinding) holder.binding).ivWishlist.setTag(R.string.product_tag, product);
        ((ItemWishlistConfirmationBinding) holder.binding).ivWishlist.setTag(R.string.position_tag, position);
        holder.binding.setVariable(BR.product, product);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    private static class WishlistConfirmationHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public WishlistConfirmationHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
