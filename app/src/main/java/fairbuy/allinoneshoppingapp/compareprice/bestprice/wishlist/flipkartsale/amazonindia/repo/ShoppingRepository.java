package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.SearchHistory;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.ShoppingDao;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.ShoppingDatabase;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.GoogleSearch;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.network.ApiWebService;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aditya.amartya on 28/09/18
 */
public class ShoppingRepository {

    private ShoppingDao shoppingDao;

    private ExecutorService executorService;

    public ShoppingRepository(Application application) {
        ShoppingDatabase shoppingDatabase = ShoppingDatabase.getInstance(application);
        shoppingDao = shoppingDatabase.getDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public LiveData<List<SearchHistory>> getShoppingHistory(int count) {
        if (shoppingDao != null) {
            return shoppingDao.getHistory(count);
        }
        return null;
    }

    public void insertItem(final SearchHistory searchHistory) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                if (shoppingDao != null) {
                    shoppingDao.insert(searchHistory);
                }
            }
        });
    }

    public void getSearchResults(String url, final ApiCallBack<GoogleSearch> callBack) {
        ApiWebService.getApiService().getSearchResults(url).enqueue(new Callback<GoogleSearch>() {
            @Override
            public void onResponse(Call<GoogleSearch> call, Response<GoogleSearch> response) {
                if (response.isSuccessful() && response.code() == HttpURLConnection.HTTP_OK) {
                    callBack.onSuccess(response.body());
                } else {
                    callBack.onFail(new RuntimeException(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GoogleSearch> call, Throwable t) {
                callBack.onFail(t);
            }
        });
    }
}
