package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import android.Manifest;
import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.MyOrdersHomeAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentHomeMyOrdersBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.MyOrderHomeItemBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.SMSParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.viewmodel.SmsViewModel;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceKeys;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.PreferenceManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.OrderDetailActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget.SpanningLinearLayoutManager;

/**
 * Created by prince.sachdeva on 18/12/18.
 */

public class HomeMyOrdersFragment extends BaseFragment implements View.OnClickListener {


    private SmsViewModel smsViewModel;
    private RequestPermissionAction onPermissionCallBack;
    private final static int REQUEST_READ_SMS_PERMISSION = 3004;
    private OnViewAllOrdersClickListener mOnViewAllOrdersClickListener;

    @Override
    protected int getResourceId() {
        return R.layout.fragment_home_my_orders;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnViewAllOrdersClickListener) {
            mOnViewAllOrdersClickListener = (OnViewAllOrdersClickListener) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {

        binding.setVariable(BR.clickListener, mOnClickListener);
        binding.setVariable(BR.viewStatus, 2);
        binding.executePendingBindings();

        smsViewModel = ViewModelProviders.of(this).get(SmsViewModel.class);
        getLiveData();

        if (!checkReadSMSPermission()) {
            hideView();
            binding.setVariable(BR.viewStatus, 1);
            binding.executePendingBindings();
        } else {
            handleEmptyViewCase();
        }

    }

    private void getLiveData() {
        final LiveData<List<SmsProductData>> liveData = smsViewModel.getLiveData(3);
        liveData.observe(this, new Observer<List<SmsProductData>>() {
            @Override
            public void onChanged(@Nullable List<SmsProductData> smsProductData) {
                if (smsProductData != null && smsProductData.size() > 0) {
                    liveData.removeObserver(this);
                    binding.setVariable(BR.viewStatus, 4);
                    binding.executePendingBindings();
                    showOrders((ArrayList<SmsProductData>) smsProductData);
                }
            }
        });
    }


    private void askSMSPermission() {
        getReadSMSPermission(new RequestPermissionAction() {
            @Override
            public void permissionDenied() {
                Toast.makeText(getContext(), "Denied Permission", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void permissionGranted() {
                getPreviousMessages();
            }
        });
    }


    public void getReadSMSPermission(RequestPermissionAction onPermissionCallBack) {
        this.onPermissionCallBack = onPermissionCallBack;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkReadSMSPermission()) {
                requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS}, REQUEST_READ_SMS_PERMISSION);
                return;
            } else {
                onPermissionCallBack.permissionGranted();
            }
        } else {
            onPermissionCallBack.permissionGranted();
        }
    }

    private boolean checkReadSMSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardView:
                if (v.getTag() != null && v.getTag() instanceof SmsProductData) {
                    Intent intent = new Intent(getContext(), OrderDetailActivity.class);
                    intent.putExtra(OrderDetailActivity.ARGS.SMS_PRODUCT_DATA, (Serializable) v.getTag());
                    getContext().startActivity(intent);
                }
        }
    }


    public interface RequestPermissionAction {
        void permissionDenied();

        void permissionGranted();

    }

    public void getPreviousMessages() {

        if (!PreferenceManager.get(PreferenceKeys.IS_PREVIOUS_MESSAGE_PARSED, false)) {

            binding.setVariable(BR.viewStatus, 2);
            binding.executePendingBindings();
            final Handler handler = new Handler();

            PreferenceManager.save(PreferenceKeys.IS_PREVIOUS_MESSAGE_PARSED, true);

            final Runnable runnableEmptyView = new Runnable() {
                @Override
                public void run() {
                    handleEmptyViewCase();
                }
            };
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    SMSParser.parsePreviousMessages((Application) getContext().getApplicationContext(), new SMSParser.OnTaskCompletionListener() {
                        @Override
                        public void onTaskCompleted() {
                            handler.postDelayed(runnableEmptyView, 1000);
                        }
                    });
                }
            };


            ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.execute(runnable);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_READ_SMS_PERMISSION) {
            if (grantResults.length > 0) {
                boolean smsPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                if (smsPermission) {
                    if (onPermissionCallBack != null) {
                        onPermissionCallBack.permissionGranted();
                    }
                } else {
                    if (onPermissionCallBack != null) {
                        onPermissionCallBack.permissionDenied();
                    }
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_SMS)) {
                        Toast.makeText(getContext(), "Canclelled Permission By User", Toast.LENGTH_SHORT).show();
                    } else {

                    }
                }
            }
        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.buttonAskPermission:
                    askSMSPermission();
                    break;
                case R.id.buttonViewAll:
                    selectMyOrdersTab();
                    break;
            }
        }
    };

    public void selectMyOrdersTab() {
        if (mOnViewAllOrdersClickListener != null) {
            mOnViewAllOrdersClickListener.onViewAllOrdersClick();
        }
    }

    private void handleEmptyViewCase() {
        final LiveData<Integer> count = smsViewModel.getCount();
        count.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null && integer == 0) {
                    Log.e("TAG", "count == 0");
                    binding.setVariable(BR.viewStatus, 3);
                    binding.executePendingBindings();
                    getPreviousMessages();
                    hideView();
                }
                count.removeObserver(this);
            }
        });
    }

    public void showOrders(ArrayList<SmsProductData> smsProductDataList) {
        LinearLayout llContainer = ((FragmentHomeMyOrdersBinding) binding).llContainer;

        for (int i = 0; i < smsProductDataList.size(); i++) {
            SmsProductData smsProductData = smsProductDataList.get(i);
            ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.my_order_home_item, llContainer, false);
            ((LinearLayout) llContainer).addView(viewDataBinding.getRoot());
            viewDataBinding.setVariable(BR.smsProductData, smsProductData);
            viewDataBinding.setVariable(BR.clickListener, this);
            viewDataBinding.setVariable(BR.drawableId, smsProductData.getAppIcon());
            MyOrdersHomeAdapter adapter = new MyOrdersHomeAdapter();
            adapter.updateData(smsProductData.getStatus());
            RecyclerView rvStatus = ((MyOrderHomeItemBinding) viewDataBinding).rvStatus;
            rvStatus.setLayoutManager(new SpanningLinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            rvStatus.setAdapter(adapter);
            rvStatus.setLayoutFrozen(true);
            viewDataBinding.executePendingBindings();
        }
        binding.executePendingBindings();
    }

    public interface OnViewAllOrdersClickListener {
        public void onViewAllOrdersClick();
    }

    private void hideView(){
            ((FragmentHomeMyOrdersBinding) binding).llParentContainer.setVisibility(View.GONE);
    }
}
