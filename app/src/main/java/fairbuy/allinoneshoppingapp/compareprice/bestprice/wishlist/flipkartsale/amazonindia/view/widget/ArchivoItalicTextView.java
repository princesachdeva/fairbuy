package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;

/**
 * Created by aditya.amartya on 05/12/18
 */
public class ArchivoItalicTextView extends AppCompatTextView {
    public ArchivoItalicTextView(Context context) {
        super(context);
        setFonts(context);
    }

    private void setFonts(Context ctx) {
        Typeface primeTypeFace = Typeface.createFromAsset(ctx.getAssets(), "fonts/Archivo-Italic.ttf");
        setTypeface(primeTypeFace);
    }
}
