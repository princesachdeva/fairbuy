package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by aditya.amartya on 28/09/18
 */

@Dao
public interface ShoppingDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SearchHistory searchHistory);

    @Query("Select * from search_history Order by last_modified_time desc LIMIT :count")
    LiveData<List<SearchHistory>> getHistory(int count);
}
