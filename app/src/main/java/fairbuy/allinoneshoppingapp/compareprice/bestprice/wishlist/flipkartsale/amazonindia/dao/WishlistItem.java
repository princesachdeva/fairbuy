package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;


@Entity(tableName = "wish_list")
public class WishlistItem {

    @ColumnInfo
    private String item;

    @ColumnInfo(name = "image_url")
    private String imageUrl;

    @ColumnInfo(name = "shopping_site")
    private String shoppingSite;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "product_url")
    private String productUrl;

    @ColumnInfo(name = "last_modified_time")
    private long lastModifiedTime = System.currentTimeMillis();


    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getShoppingSite() {
        return shoppingSite;
    }

    public void setShoppingSite(String shoppingSite) {
        this.shoppingSite = shoppingSite;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        productUrl = Utils.getUrlWithoutParameters(productUrl);
        this.productUrl = productUrl;
    }

    public long getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(long lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }
}
