package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by aditya.amartya on 23/10/18
 */
public interface AssistantConstants {

    String AMAZON = "amazon";
    String AJIO = "ajio";
    String FLIPKART = "flipkart";
    String JABONG = "jabong";
    String MYNTRA = "myntra";
    String PAYTM = "paytm";
    String SNAPDEAL = "snapdeal";

    String ADD_TO_CART = "ADD TO CART";
    String ADD_TO_BASKET = "ADD TO BASKET";
    String BUY_NOW = "BUY NOW";
    String Buy_Now = "Buy Now";
    String NOTIFY_ME = "NOTIFY ME";
    String BUY_FOR = "Buy for";
    String COMING_SOON = "COMING SOON";
    String ALSO_AVAILABLE_BY = "Also Available by";
    String AMAZON_IN = ": Amazon.in: ";
    String BESTSELLER = "Bestseller";

    String OUT_OF_STOCK = "Out of Stock";
    String PRODUCT_OUT_OF_STOCK = "THIS PRODUCT IS OUT OF STOCK";
    String ADD_TO_BAG = "Add to Bag";
    String WISHLIST = "WISHLIST";
    String Save_to_closet = "Save to closet";

    String Add_to_Cart = "Add to Cart";
    String ADD_TO_WISH_LIST = "ADD TO WISH LIST";
    String ADD_TO_WISHLIST = "ADD TO WISHLIST";
    String addToCardIdAmazon = "add-to-cart-button";
    String addToWishListIdAmazon = "add-to-wishlist-button-submit";
    String SIGN_IN_TO_BUY = "Sign in to Buy";
    String ADDITIONAL_TAX = "Additional tax may apply";

    String paytmAppId = "net.one97.paytm";
    String amazonAppId = "in.amazon.mShop.android.shopping";
    String flipkartAppId = "com.flipkart.android";
    String snapdealAppId = "com.snapdeal.main";
    String myntraAppId = "com.myntra.android";
    String jabongAppId = "com.jabong.android";
    String ajioAppId = "com.ril.ajio";
    String chromeId = "com.android.chrome";
    String fairBuyId = "in.fairbuy.android";

    ArrayList<String> packageNamesList = new ArrayList<>(Arrays.asList(paytmAppId, amazonAppId, flipkartAppId, snapdealAppId, myntraAppId, jabongAppId, ajioAppId, chromeId));

    String SYSTEM_DIALOG_REASON_KEY = "reason";
    String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
    String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";


    //Ignore these strings found
    String ON_OFFER = "ON OFFER";
    String OFFERS = "Offers";
    String PRODUCT_DETAILS = "Product \n Details";
    String COLORS = "Colors";
    String PERCENTAGE_DISCOUNT = "% OFF";
    String RS = "Rs. ";
    String POPULAR = "POPULAR";
    String INCLUSIVE = "Inclusive";
    String ONLY = "Only";
    String LEFT = "Left";
    String EXCLUSIVE = "Exclusive";
    String RUPEE = "₹";
    String PAYTM_T_DESCRIPTION = "tDescription: ";
    String MRP = "MRP";
    String FILTER = "FILTER";
    String RUPEES = "rupees";
    String PRICE = "Price:";
    String DEAL_PRICE = "Deal Price:";

    int AMAZON_ID = 1001;
    int FLIPKART_ID = AMAZON_ID + 1;
    int SNAPDEAL_ID = FLIPKART_ID + 1;
    int PAYTM_ID = SNAPDEAL_ID + 1;
    int MYNTRA_ID = PAYTM_ID + 1;
    int JABONG_ID = MYNTRA_ID + 1;
    int AJIO_ID = JABONG_ID + 1;

    String AMAZON_AFFILATE_TAG = "tag=fairbuy0e-21";

}
