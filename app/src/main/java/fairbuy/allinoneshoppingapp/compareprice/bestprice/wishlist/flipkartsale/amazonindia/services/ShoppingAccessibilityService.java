package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.services;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.SearchHistory;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.AnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser.ChromeParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.ShoppingRepository;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget.DragDropWidget;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.FirebaseEvents.BUBBLE_SHOW;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.CHROME_BROWSER;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.NATIVE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ADDITIONAL_TAX;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ADD_TO_BAG;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ADD_TO_BASKET;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ADD_TO_CART;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ADD_TO_WISHLIST;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ADD_TO_WISH_LIST;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.AJIO;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.AJIO_ID;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ALSO_AVAILABLE_BY;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.AMAZON;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.AMAZON_ID;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.AMAZON_IN;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.Add_to_Cart;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.BUY_FOR;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.BUY_NOW;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.Buy_Now;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.COLORS;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.COMING_SOON;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.DEAL_PRICE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.EXCLUSIVE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.FLIPKART;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.FLIPKART_ID;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.INCLUSIVE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.JABONG;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.JABONG_ID;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.LEFT;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.MRP;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.MYNTRA;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.MYNTRA_ID;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.NOTIFY_ME;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.OFFERS;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ONLY;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ON_OFFER;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.OUT_OF_STOCK;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.PAYTM;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.PAYTM_ID;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.PAYTM_T_DESCRIPTION;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.PERCENTAGE_DISCOUNT;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.POPULAR;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.PRICE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.PRODUCT_DETAILS;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.RS;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.RUPEE;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.RUPEES;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.SIGN_IN_TO_BUY;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.SNAPDEAL;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.SNAPDEAL_ID;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.Save_to_closet;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.ajioAppId;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.amazonAppId;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.flipkartAppId;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.jabongAppId;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.myntraAppId;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.paytmAppId;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AssistantConstants.snapdealAppId;

/**
 * Created by aditya.amartya on 12/10/18
 */
public class ShoppingAccessibilityService extends AccessibilityService {


    private long DELAY = 1000L;
    private long lastEventTimeStamp;
    private String previousURL = "";
    private String previousTitle, searchString, title, brand, price;
    private ShoppingUtils.WEBSITE currentApp;
    private Product mProduct;
    private String currentProcessingURL;

    private ArrayList<String> appList;
    private Product lastVisitedProduct;
    private DragDropWidget dragDropWidget;
    private String previouspackageName;

    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";


    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event != null && avoidSameEvent(event)) {

            AccessibilityNodeInfo source = getRootInActiveWindow();

            //android:packageNames="com.flipkart.android,in.amazon.mShop.android.shopping,net.one97.paytm,com.snapdeal.main,com.myntra.android,com.jabong.android,com.ril.ajio,com.android.chrome"

            CharSequence packageName = event.getPackageName();
           /* if (!TextUtils.isEmpty(packageName) && !packageName.equals(getPackageName()) && !packageName.equals("com.android.systemui") && !packageName.equals(previouspackageName)) {
                Log.e("TAG", "HIDE");
                Log.e("TAG", packageName + "   " + previouspackageName);
                if (dragDropWidget.getOverlayWindow() != null) {
                    Log.e("TAG", "HIDE1");
                    dragDropWidget.getOverlayWindow().hide();
                }
            }*/

            if (!TextUtils.isEmpty(packageName) && !ShoppingUtils.isSupportedPackage(packageName.toString())) {
                return;
            }
            if (!TextUtils.isEmpty(packageName)) {
                //Log.d("EventText", packageName.toString());
                previouspackageName = packageName.toString();
            }
            if (("com.android.chrome").equals(packageName)) {
                handleChromeEvent(event);
                return;
            }

            if (source == null) {
                return;
            }

            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < source.getChildCount(); i++) {
                //Log.d("EventText", toStringHierarchy(source.getChild(i), 0));
                stringBuilder.append(toStringHierarchy(source.getChild(i), 0));
            }

            String allText = stringBuilder.toString();
            //Log.d("EventText", allText);

            //ajio
            if (allText.contains(ajioAppId) && (allText.contains(Save_to_closet) || allText.contains(ADD_TO_BAG))) {
                currentApp = ShoppingUtils.WEBSITE.AJIO;
                getTexts(allText, "text: ", 15, AJIO_ID);
            }

            //jabong
            else if (allText.contains(jabongAppId) && (allText.contains(ADD_TO_WISHLIST) || allText.contains(ADD_TO_BAG.toUpperCase()) ||
                    allText.contains(OUT_OF_STOCK.toUpperCase()))) {
                currentApp = ShoppingUtils.WEBSITE.JABONG;
                getTexts(allText, "text: ", 50, JABONG_ID);
            }

            //myntra
            else if (allText.contains(myntraAppId) && (allText.contains(ADD_TO_BAG.toUpperCase()) || allText.contains(OUT_OF_STOCK.toUpperCase()))) {
                currentApp = ShoppingUtils.WEBSITE.MYNTRA;
                getTexts(allText, "text: ", 100, MYNTRA_ID);
            }

            //snapdeal
            else if (allText.contains(snapdealAppId) && (allText.contains(Add_to_Cart) || allText.contains(Buy_Now))) {
                currentApp = ShoppingUtils.WEBSITE.SNAPDEAL;
                getTexts(allText, "text: ", 15, SNAPDEAL_ID);
            }

            //Paytm
            else if (allText.contains(paytmAppId) && (allText.contains(ADD_TO_BAG) || allText.contains(OUT_OF_STOCK))) {
                currentApp = ShoppingUtils.WEBSITE.PAYTM;
                getTexts(allText, "contentDescription: ", 50, PAYTM_ID);
            }

            //Amazon
            else if (allText.contains(amazonAppId) && ((allText.contains(Add_to_Cart) || allText.contains(ADD_TO_WISH_LIST) || allText.contains(SIGN_IN_TO_BUY)))) {
                currentApp = ShoppingUtils.WEBSITE.AMAZON;
                getTexts(allText, "text: ", 25, AMAZON_ID);
            }

            //Flipkart
            else if (allText.contains(flipkartAppId) && (allText.contains(BUY_NOW) || allText.contains(ADD_TO_CART) || allText.contains(NOTIFY_ME) || allText.contains(ADD_TO_BASKET))) {
                currentApp = ShoppingUtils.WEBSITE.FLIPKART;
                getTexts(allText, "text: ", 15, FLIPKART_ID);
            } else if (!allText.contains("cardHeader") && !allText.contains("ll_container") && !allText.contains("ll_overlay_actions")) {
                dragDropWidget.removeDragAndDropViews();
                if (dragDropWidget.getOverlayWindow() != null) {
                    dragDropWidget.getOverlayWindow().hide();
                }
                title = searchString = price = brand = null;
                mProduct = null;
            }
        }
    }

    private void getTexts(String allText, String patternText, int textLimit, int shoppingAppId) {
        StringBuilder textBuilder = new StringBuilder();
        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(allText);
        int count = 0;
        int titleBrandCount = 0;
        String prevText = null;
        ArrayList<String> myntraTexts = new ArrayList<>();
        while (matcher.find()) {
            if (count == textLimit) {
                break;
            }
            StringBuilder text = new StringBuilder();
            int start = matcher.start();
            for (int i = start + 6; ; i++) {
                char c = allText.charAt(i);
                if (c == ';') {
                    break;
                }
                text.append(c);
            }
            String currentText = text.toString().trim();
            if (!currentText.contains("null") && !TextUtils.isEmpty(currentText)) {
                if (shoppingAppId == AJIO_ID) {
                    if (!currentText.equalsIgnoreCase(EXCLUSIVE)) {
                        titleBrandCount++;
                        if (titleBrandCount == 1) {
                            brand = currentText;
                            //Log.d("AjioBrand", brand);
                        } else if (titleBrandCount == 2) {
                            title = currentText;
                            //Log.d("AjioTitle", title);
                        } else if (titleBrandCount == 3) {
                            price = currentText;
                            //Log.d("AjioBrandTitlePrice", price);
                            //Toast.makeText(ShoppingAccessibilityService.this, brand + "\n" + title + " " + price, //Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                } else if (shoppingAppId == PAYTM_ID) {
                    if (currentText.contains(PAYTM_T_DESCRIPTION)) {
                        currentText = currentText.replace(PAYTM_T_DESCRIPTION, "").trim();
                    }
                    if (!TextUtils.isEmpty(prevText) && !prevText.contains(BUY_FOR) && !prevText.contains(ALSO_AVAILABLE_BY) && (currentText.startsWith(RUPEE) || currentText.contains(MRP))) {
                        if (TextUtils.isEmpty(title)) {
                            title = prevText.replace(PAYTM_T_DESCRIPTION, "");
                            brand = ShoppingUtils.getBrandFromTitle(this, title);
                        }
                        if (currentText.contains(RUPEE)) {
                            price = currentText.replace(PAYTM_T_DESCRIPTION, "");

                            //Log.d("BrandTitlePrice", brand + "\n" + title + "\n" + price);
                            //Toast.makeText(ShoppingAccessibilityService.this, brand + "\n" + title + "\n" + price, //Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                    prevText = currentText;
                } else if (shoppingAppId == JABONG_ID) {
                    if (!TextUtils.isEmpty(prevText) && currentText.contains(RS.trim())) {
                        title = prevText;
                        price = currentText;
                        //Log.d("BrandTitlePrice", brand + "\n" + title + " " + price);
                        //Toast.makeText(ShoppingAccessibilityService.this, brand + "\n" + prevText + " " + price, //Toast.LENGTH_SHORT).show();
                        break;
                    }
                    brand = prevText;
                    prevText = currentText;
                } else if (shoppingAppId == MYNTRA_ID) {
                    if (currentText.equalsIgnoreCase(ADDITIONAL_TAX)) {
                        int size = myntraTexts.size();
                        String innerText = myntraTexts.get(size - 1);
                        if (innerText.contains(PERCENTAGE_DISCOUNT)) {
                            price = myntraTexts.get(size - 3);
                            title = myntraTexts.get(size - 4);
                        } else {
                            price = myntraTexts.get(size - 1);
                            title = myntraTexts.get(size - 2);
                        }
                        //Log.d("BrandTitlePrice", brand + "\n" + title + "\n" + price);
                        //Toast.makeText(ShoppingAccessibilityService.this, title + "\n" + price, //Toast.LENGTH_SHORT).show();
                        break;
                    } else {
                        myntraTexts.add(currentText);
                    }
                } else if (shoppingAppId == AMAZON_ID) {
                    if (TextUtils.isEmpty(title) && currentText.length() > 1 && !TextUtils.isDigitsOnly(currentText)) {
                        if (currentText.contains(AMAZON_IN)) {
                            int extensionTextIndex = currentText.indexOf(AMAZON_IN);
                            title = currentText.substring(0, extensionTextIndex);
                        } else {
                            title = currentText;
                        }
                        brand = ShoppingUtils.getBrandFromTitle(this, title);
                        //Log.d("BrandTitle", brand + "\n" + title);
                        //Toast.makeText(ShoppingAccessibilityService.this, brand + "\n" + title, //Toast.LENGTH_SHORT).show();
                        //break;
                    } else if (!TextUtils.isEmpty(title) && currentText.contains(RUPEES)) {
                        price = currentText.replace(RUPEES + " ", RUPEE);
                        //Toast.makeText(ShoppingAccessibilityService.this, title + "\n" + price, //Toast.LENGTH_SHORT).show();
                        break;
                    } else if (!TextUtils.isEmpty(prevText) && prevText.trim().equalsIgnoreCase(PRICE)) {
                        price = currentText;
                        //Toast.makeText(ShoppingAccessibilityService.this, title + "\n" + price, //Toast.LENGTH_SHORT).show();
                    } else if (!TextUtils.isEmpty(prevText) && prevText.trim().equalsIgnoreCase(DEAL_PRICE)) {
                        price = currentText;
                        //Toast.makeText(ShoppingAccessibilityService.this, title + "\n" + price, //Toast.LENGTH_SHORT).show();
                        break;
                    }
                    prevText = currentText;
                } else if (shoppingAppId == FLIPKART_ID && !currentText.contains(AssistantConstants.BESTSELLER)) {
                    if (!TextUtils.isEmpty(title) && (String.valueOf(currentText.charAt(0)).equals(AssistantConstants.RUPEE) ||
                            (!TextUtils.isEmpty(prevText) && String.valueOf(prevText.charAt(0)).equals(AssistantConstants.RUPEE)))) {
                        if (currentText.length() == 1 && String.valueOf(currentText.charAt(0)).equals(AssistantConstants.RUPEE)) {
                            prevText = currentText;
                            continue;
                        }
                        price = !currentText.contains(AssistantConstants.RUPEE) ? (prevText + currentText) : currentText;
                        //Log.d("BrandTitlePrice", brand + "\n" + title + "\n" + price);
                        //Toast.makeText(ShoppingAccessibilityService.this, title + "\n" + price, //Toast.LENGTH_SHORT).show();
                        break;
                    }
                    if ((currentText.contains("+") && TextUtils.isDigitsOnly(currentText.substring(1).trim())) || ((currentText.contains(COLORS) ||
                            currentText.contains(COLORS.toLowerCase()) || currentText.contains(COLORS.toUpperCase())) &&
                            TextUtils.isDigitsOnly(currentText.charAt(0) + ""))) {
                        continue;
                    }
                    if (TextUtils.isEmpty(title) && currentText.length() > 1 && !TextUtils.isDigitsOnly(currentText) && !currentText.equalsIgnoreCase(ON_OFFER) &&
                            !currentText.equalsIgnoreCase(OFFERS) && !currentText.equalsIgnoreCase(PRODUCT_DETAILS)) {
                        if (!allText.contains(PRODUCT_DETAILS) && TextUtils.isEmpty(brand)) {
                            brand = currentText;
                            continue;
                        }
                        title = currentText;
                        if (TextUtils.isEmpty(brand)) {
                            brand = ShoppingUtils.getBrandFromTitle(this, title);
                        }
                        //Log.d("BrandTitle", brand + "\n" + title);
                        //Toast.makeText(ShoppingAccessibilityService.this, brand + "\n" + title, //Toast.LENGTH_SHORT).show();
                        //break;
                    }
                    prevText = currentText;
                } else if (shoppingAppId == SNAPDEAL_ID) {
                    if (currentText.contains("+") && TextUtils.isDigitsOnly(currentText.substring(1).trim())) {
                        continue;
                    }
                    if (TextUtils.isEmpty(price) && currentText.contains(RS)) {
                        price = currentText;
                        continue;
                    }
                    if (currentText.length() > 1 && !TextUtils.isDigitsOnly(currentText) && !currentText.contains(ON_OFFER) && !currentText.contains(PERCENTAGE_DISCOUNT) && !currentText.contains(RS)
                            && (!currentText.contains(ONLY) || !currentText.contains(LEFT)) && !currentText.contains(POPULAR) && !currentText.contains(INCLUSIVE)) {
                        title = currentText;
                        brand = ShoppingUtils.getBrandFromTitle(this, title);
                        //Log.d("BrandTitle", brand + "\n" + title);
                        //Toast.makeText(ShoppingAccessibilityService.this, title + "\n" + price, //Toast.LENGTH_SHORT).show();
                        break;
                    }
                } /*else {
                    if (currentText.contains("+") && TextUtils.isDigitsOnly(currentText.substring(1).trim())) {
                        continue;
                    }
                    if (currentText.length() > 1 && !TextUtils.isDigitsOnly(currentText) && !currentText.equalsIgnoreCase(ON_OFFER) && !currentText.equalsIgnoreCase(OFFERS)
                            && !currentText.equalsIgnoreCase(PRODUCT_DETAILS) && !currentText.contains(PERCENTAGE_DISCOUNT) && !currentText.contains(RS)
                            && (!currentText.contains(ONLY) || !currentText.contains(LEFT))) {
                        //Log.d("Title=", currentText);
                        title = currentText;
                        //Toast.makeText(ShoppingAccessibilityService.this, title, //Toast.LENGTH_SHORT).show();
                        break;
                    }
                }*/


                textBuilder.append(currentText).append("****");
                count++;
            }
        }
        getProductDetails();
        //Log.d("EventText", textBuilder.toString());
        Product product = new Product();
        product.productName = title;
        product.productBrand = brand;
        product.productPrice = price;
        product.isNative = true;
        product.website = currentApp;
        dragDropWidget.showAssistant(product);
    }

    private void getProductDetails() {
        final ArrayList<Product> productList = new ArrayList<>();
        ChromeParser.getProductDetails(title, price, null, currentApp,
                new ChromeParser.OnProductSearchPageListener() {
                    @Override
                    public void onProductSearch(ArrayList<Product> products) {
                        if (products != null && products.size() > 0) {
                            for (Product product : products) {
                                if (product != null && !TextUtils.isEmpty(product.productName)) {
                                    productList.add(product);
                                    //productList.add(product);
                                    //productList.add(product);
                                }
                            }
                            if (products.size() == 1) {
                                saveProductToHistoryNative(products.get(0));
                                dragDropWidget.setDeducedProductForNativeView(products.get(0));
                            } else {
                                saveProductToHistoryNative(productList.get(0));
                                dragDropWidget.setDeducedProductsForNativeView(productList);
                            }
                        }
                    }
                });
    }

    @Override
    public void onInterrupt() {
        //unbindService(ShoppingAccessibilityService.this);
        unregisterReceiver(dragDropWidget.getBroadcastReceiver());
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        dragDropWidget = DragDropWidget.getInstance(this);
        registerReceiver(dragDropWidget.getBroadcastReceiver(), new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        AccessibilityServiceInfo info = this.getServiceInfo();

        info.flags = AccessibilityServiceInfo.FLAG_INCLUDE_NOT_IMPORTANT_VIEWS | AccessibilityServiceInfo.FLAG_REPORT_VIEW_IDS | AccessibilityServiceInfo.FLAG_RETRIEVE_INTERACTIVE_WINDOWS
                | AccessibilityServiceInfo.CAPABILITY_CAN_RETRIEVE_WINDOW_CONTENT;

        this.setServiceInfo(info);

        appList = new ArrayList<>();
        appList.addAll(Arrays.asList(AMAZON, AJIO, FLIPKART, JABONG, MYNTRA, PAYTM, SNAPDEAL));
    }

    private String toStringHierarchy(AccessibilityNodeInfo info, int depth) {
        if (info == null) return "";

        StringBuilder result = new StringBuilder("|");
        for (int i = 0; i < depth; i++) {
            //result.append("  ");
            result.append("");
        }

        result.append(info.toString());

        for (int i = 0; i < info.getChildCount(); i++) {
            //result.append("\n").append(toStringHierarchy(info.getChild(i), depth + 1));
            result.append(toStringHierarchy(info.getChild(i), depth + 1));
        }

        return result.toString();
    }

    private boolean avoidSameEvent(AccessibilityEvent accessibilityEvent) {
        int eventType = accessibilityEvent.getEventType();
        if (this.lastEventTimeStamp + this.DELAY > System.currentTimeMillis() && (eventType == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED
                || eventType == AccessibilityEvent.TYPE_WINDOWS_CHANGED || eventType == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED)) {
// Appended BY Prince
            //this.lastEventTimeStamp = System.currentTimeMillis();

            return false;
        }
        this.lastEventTimeStamp = System.currentTimeMillis();
        return true;
    }

    private void handleChromeEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityNodeInfo source = getRootInActiveWindow();
        if (source == null) {
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < source.getChildCount(); i++) {
            //Log.d("EventText", toStringHierarchy(source.getChild(i), 0));
            stringBuilder.append(toStringHierarchy(source.getChild(i), 0));
        }
        String allText = stringBuilder.toString();
        if (allText.contains(BUY_NOW) || allText.contains(ADD_TO_CART) || allText.contains(ADD_TO_BAG.toUpperCase()) || allText.contains(BUY_FOR) || allText.contains(COMING_SOON) || allText.contains(
                NOTIFY_ME)) {
            if (!TextUtils.isEmpty(previousTitle) && previousURL.contains("flipkart")) {
                onDetailPage(true, ShoppingUtils.WEBSITE.FLIPKART);
                return;
            } else if (!TextUtils.isEmpty(previousTitle) && previousURL.contains("myntra")) {
                if (allText.contains(ADD_TO_BAG.toUpperCase())) {
                    onDetailPage(true, ShoppingUtils.WEBSITE.MYNTRA);
                } else {
                    onDetailPage(false, ShoppingUtils.WEBSITE.TEMP);
                }
                return;
            } else if (!TextUtils.isEmpty(previousTitle) && previousURL.contains("snapdeal")) {
                onDetailPage(true, ShoppingUtils.WEBSITE.SNAPDEAL);
                return;
            } else if (!TextUtils.isEmpty(previousTitle) && previousURL.contains("paytm")) {
                onDetailPage(true, ShoppingUtils.WEBSITE.PAYTM);
                return;
            } /*else if (!TextUtils.isEmpty(previousTitle)) {
                onDetailPage(true, ShoppingUtils.WEBSITE.TEMP);
                return;
            } */ else {
                if (isURLBarVisible()) {
                    checkDataByURLBar();
                } else {
                    previousTitle = null;
                    // Please Scroll
                    mProduct = new Product();
                    mProduct.isNative = false;
                    onDetailPage(true, ShoppingUtils.WEBSITE.TEMP);
                }
            }
        } else {
            if (!TextUtils.isEmpty(previousURL) && previousURL.contains("flipkart")) {
                previousURL = "";
                previousTitle = null;
                if (isURLBarVisible()) {
                    checkDataByURLBar();
                } else {
                    onDetailPage(false, null);
                }
            } else if (!TextUtils.isEmpty(previousURL) && previousURL.contains("snapdeal")) {
                previousURL = "";
                previousTitle = null;
                onDetailPage(false, null);
            } else if (!TextUtils.isEmpty(previousURL) && previousURL.contains("paytm")) {
                previousURL = "";
                previousTitle = null;
                onDetailPage(false, null);
            } else if (!TextUtils.isEmpty(previousURL) && previousURL.contains("myntra")) {
                previousURL = "";
                previousTitle = null;
                if (isURLBarVisible()) {
                    checkDataByURLBar();
                } else {
                    onDetailPage(false, null);
                }
            }
        }
        checkDataByURLBar();
    }

    private boolean isURLBarVisible() {
        if (getRootInActiveWindow() == null) {
            return false;
        }

        List<AccessibilityNodeInfo> accessibilityNodeInfos = getRootInActiveWindow().findAccessibilityNodeInfosByViewId("com.android.chrome:id/url_bar");
        if (accessibilityNodeInfos != null && accessibilityNodeInfos.size() > 0) {
            AccessibilityNodeInfo accessibilityNodeInfo = accessibilityNodeInfos.get(0);
            if (accessibilityNodeInfo != null && ("android.widget.EditText").equals(accessibilityNodeInfo.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void checkDataByURLBar() {
        try {
            if (getRootInActiveWindow() == null) {
                return;
            }
            List<AccessibilityNodeInfo> accessibilityNodeInfos = getRootInActiveWindow().findAccessibilityNodeInfosByViewId("com.android.chrome:id/url_bar");
            if (accessibilityNodeInfos != null && accessibilityNodeInfos.size() > 0) {
                AccessibilityNodeInfo accessibilityNodeInfo = accessibilityNodeInfos.get(0);
                if (accessibilityNodeInfo != null && ("android.widget.EditText").equals(accessibilityNodeInfo.getClassName())) {
                    final String url = accessibilityNodeInfo.getText().toString();
                    final ShoppingUtils.WEBSITE website;
                    if (url.equals(previousURL) && !TextUtils.isEmpty(previousTitle)) {
                        website = ChromeParser.getWebSiteType(url);
                        onDetailPage(true, website);
                        return;
                    }
                    website = ChromeParser.getWebSiteType(url);
                    if (website == null) {
                        onDetailPage(false, null);
                        return;
                    }

                    // Flipkart, Amazon
                    if (url.contains("/p/") || url.contains("/dp/") || url.contains("/gp/") || url.contains("/buy") || url.contains("/product/") || url.contains("-pdp") || (url.contains(".htm")
                            && url.contains("jabong"))) {

                        if (!url.equals(currentProcessingURL)) {
                            currentProcessingURL = url;
                            ChromeParser.parseURL(url, new ChromeParser.OnProductPageListener() {
                                @Override
                                public void onProductPage(Product product) {
                                    currentProcessingURL = null;
                                    if (product != null && !TextUtils.isEmpty(product.productName)) {
                                        previousURL = url;
                                        // previousTitle = product.productName;
                                        mProduct = product;
                                        onDetailPage(true, website);
                                    } else {
                                        mProduct = null;
                                        previousTitle = null;
                                        onDetailPage(false, website);
                                    }
                                }
                            });
                        }
                    }
                }
            } else {
                if (!TextUtils.isEmpty(previousURL) && (previousURL.contains("flipkart") || previousURL.contains("myntra"))) {
                    onDetailPage(false, null);
                }
            }
        } catch (Exception e) {

        }
    }

    private ShoppingUtils.WEBSITE getWebSiteType(String url) {
        if (url.contains(".htm") && url.contains("jabong")) {
            return ShoppingUtils.WEBSITE.JABONG;
        }

        if (url.contains("/p/") && url.contains("flipkart")) {
            return ShoppingUtils.WEBSITE.FLIPKART;
        }
        if ((url.contains("/p/") || url.contains("/s/")) && url.contains("ajio")) {
            return ShoppingUtils.WEBSITE.AJIO;
        }

        if (url.contains("/dp/") || url.contains("/gp/")) {
            return ShoppingUtils.WEBSITE.AMAZON;
        }

        if (url.contains("/buy")) {
            return ShoppingUtils.WEBSITE.MYNTRA;
        }

        if (url.contains("/product/")) {
            return ShoppingUtils.WEBSITE.SNAPDEAL;
        }

        if (url.contains("-pdp") || url.contains("/p/")) {
            return ShoppingUtils.WEBSITE.PAYTM;
        }
        return null;
    }


    private void onDetailPage(boolean isDetailPage, ShoppingUtils.WEBSITE website) {

        if (isDetailPage) {
            if (mProduct != null && !TextUtils.isEmpty(mProduct.productName)) {
                if (!mProduct.productName.equals(previousTitle)) {
                    title = mProduct.productName;
                    previousTitle = title;
                    if (!TextUtils.isEmpty(title)) {
                        searchString = title + " site:amazon.in OR site:snapdeal.com OR site:flipkart.com OR site:paytm.com OR site:myntra.com";
                    }
                    Log.e("DETAIL PAGE", mProduct.toString());
                    saveProductToHistoryChrome(mProduct);
                }
                mProduct.website = website;
            } else {
                title = null;
                Log.e("DETAIL PAGE", "PLS SCROLLmmm");
            }
            if (mProduct == null) {
                mProduct = new Product();
                mProduct.productName = previousTitle;
            }
            dragDropWidget.showAssistant(mProduct);
        } else {
            previousTitle = null;
            title = null;
            mProduct = null;
            Log.e("DETAIL PAGE", "NOT A DETAIL PAGE");
            dragDropWidget.removeDragAndDropViews();
            title = searchString = price = brand = null;
            mProduct = null;
        }
    }

    /*private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!TextUtils.isEmpty(action) && action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
                if (reason != null) {
                    if (reason.equals(SYSTEM_DIALOG_REASON_HOME_KEY) || reason.equals(SYSTEM_DIALOG_REASON_RECENT_APPS)) {
                        dragDropWidget.removeDragAndDropViews();
                        if (dragDropWidget.getOverlayWindow() != null) {
                            dragDropWidget.getOverlayWindow().hide();
                        }
                    }
                }
            }
        }
    };*/

    public void saveProductToHistoryChrome(Product product) {
        if (product == null || product.equals(lastVisitedProduct)) {
            return;
        }
        lastVisitedProduct = product;
        SearchHistory searchHistory = new SearchHistory();
        searchHistory.setItem(product.productName);
        searchHistory.setBrand(product.productBrand);
        searchHistory.setProductUrl(product.productURL);
        searchHistory.setImageUrl(product.productImage);
        searchHistory.setShoppingSite(product.webSiteName);
        new ShoppingRepository(getApplication()).insertItem(searchHistory);

        AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.BUBBLE,
                AnalyticsConstants.Label.BUBBLE_SHOW + product.website.toString() + AnalyticsConstants.SEPERATOR + CHROME_BROWSER + AnalyticsConstants.SEPERATOR + product.productName);


        Bundle bundle = new Bundle();
        bundle.putString("Store", product.website.toString());
        bundle.putString("Title", product.productName);
        bundle.putString("Placeholder", CHROME_BROWSER);
        bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(product.productPrice));

        FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_SHOW, bundle);
    }

    public void saveProductToHistoryNative(Product product) {
        if (product == null || product.equals(lastVisitedProduct)) {
            return;
        }
        lastVisitedProduct = product;
        SearchHistory searchHistory = new SearchHistory();
        searchHistory.setItem(product.productName);
        searchHistory.setBrand(product.productBrand);
        searchHistory.setProductUrl(product.productURL);
        searchHistory.setImageUrl(product.productImage);
        searchHistory.setShoppingSite(product.webSiteName);
        new ShoppingRepository(getApplication()).insertItem(searchHistory);

        AnalyticsManager.getInstance().sendEvent(AnalyticsConstants.Category.PURCHASE_GOAL, AnalyticsConstants.Action.BUBBLE,
                AnalyticsConstants.Label.BUBBLE_SHOW + product.website.toString() + AnalyticsConstants.SEPERATOR + NATIVE + AnalyticsConstants.SEPERATOR + product.productName);


        Bundle bundle = new Bundle();
        bundle.putString("Store", product.website.toString());
        bundle.putString("Title", product.productName);
        bundle.putString("Placeholder", NATIVE);
        bundle.putInt("Price", (int) ChromeParser.getPriceInNumericFormat(product.productPrice));

        FirebaseAnalyticsManager.getInstance().sendEvent(BUBBLE_SHOW, bundle);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }
}