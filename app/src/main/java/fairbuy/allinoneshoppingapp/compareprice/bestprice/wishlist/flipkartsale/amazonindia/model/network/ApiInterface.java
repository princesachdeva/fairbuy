package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.network;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.GoogleSearch;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by aditya.amartya on 24/10/18
 */
public interface ApiInterface {
    @GET
    Call<GoogleSearch> getSearchResults(@Url String url);
}
