package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.worker;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by prince.sachdeva on 15/01/19.
 */

public class AccessibilityCheckWorker  {


/*    public AccessibilityCheckWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        boolean isServiceRunning = isMyServiceRunning(ShoppingAccessibilityService.class);
        if (!isServiceRunning) {
            getApplicationContext().startService(new Intent(getApplicationContext(), ShoppingAccessibilityService.class));
        }
        logApiRequestAndResponse(getApplicationContext(), "doWork", isMyServiceRunning(ShoppingAccessibilityService.class) + "AccessibilityCheckWorker");
        return Worker.Result.success();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }*/

    public static void logApiRequestAndResponse(Context mContext, String url, String type) {
        String data = "##### " + type + " URL : " + url + " ---> Time : " + convertMilliSecondsToTime(System.currentTimeMillis());
        Log.i("##### ", data);
        if (isSDCardSupported()) {
            logIntoFile(mContext, data);
        }
//        } else {
//            writeFileOnInternalStorage(mContext, data);
//        }
    }

    public static void writeFileOnInternalStorage(Context context, String sBody) {
        File file = new File(Environment.getExternalStorageDirectory(), "MyFiles");
        if (!file.exists()) {
            file.mkdir();
        }

        try {
            File gpxfile = new File(file, "api_details.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isSDCardSupported() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    private static void logIntoFile(Context context, String data) {
        File file = new File(Environment.getExternalStorageDirectory(), "api_details.txt");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream f = new FileOutputStream(file, true);
            PrintWriter pw = new PrintWriter(f);
            pw.println(data);
            pw.println();
            pw.println();
            pw.flush();
            pw.close();
            f.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String convertMilliSecondsToTime(Long milliSecs) {
        String dateFormat = "yyyy-MM-dd HH:mm:ss.SSS";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSecs);
        return simpleDateFormat.format(calendar.getTime());
    }
}
