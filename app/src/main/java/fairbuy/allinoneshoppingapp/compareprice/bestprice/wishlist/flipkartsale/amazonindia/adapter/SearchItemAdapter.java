package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.WebSearchFragment;

/**
 * Created by aditya.amartya on 03/10/18
 */
public class SearchItemAdapter extends FragmentStatePagerAdapter {
    private String searchKeyword;

    public SearchItemAdapter(FragmentManager fm, String searchKeyword) {
        super(fm);
        this.searchKeyword = searchKeyword;
    }

    public void updateSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new WebSearchFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(WebSearchFragment.BUNDLE_COMPANY, ShoppingUtils.WEBSITE.values()[position]);
        bundle.putString(WebSearchFragment.BUNDLE_SEARCH_KEYWORD, searchKeyword);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return ShoppingUtils.WEBSITE.dataValues().length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return ShoppingUtils.WEBSITE.values()[position].toString();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
