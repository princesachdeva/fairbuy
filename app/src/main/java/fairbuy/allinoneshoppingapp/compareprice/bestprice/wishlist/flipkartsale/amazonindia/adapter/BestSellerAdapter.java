package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;

/**
 * Created by prince.sachdeva on 19/01/19.
 */

public class BestSellerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private JSONArray mJSONArray;
    private View.OnClickListener mOnClickListener;

    public BestSellerAdapter(View.OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    public void updateData(JSONArray jsonArray) {
        int start = getItemCount();
        mJSONArray = jsonArray;
        notifyItemRangeChanged(start, mJSONArray.length());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_best_seller, viewGroup, false);
        binding.setVariable(BR.clickListener, mOnClickListener);
        return new BestSellerHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof BestSellerHolder) {
            JSONObject jsonObject = getItem(i);
            BestSellerHolder bestSellerHolder = (BestSellerHolder) viewHolder;
            bestSellerHolder.binding.setVariable(BR.jsonObject, jsonObject);
            bestSellerHolder.binding.setVariable(BR.url, jsonObject.optString("image"));
            bestSellerHolder.binding.setVariable(BR.title, jsonObject.optString("text"));
            bestSellerHolder.binding.setVariable(BR.productURL, jsonObject.optString("http"));
        }
    }

    private JSONObject getItem(int position) {
        if (mJSONArray != null) {
            try {
                return mJSONArray.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return new JSONObject();
    }

    @Override
    public int getItemCount() {
        return mJSONArray != null ? mJSONArray.length() > 25 ? 25 : mJSONArray.length() : 0;
    }

    public static class BestSellerHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BestSellerHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
