package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.converter.StatusConverter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsDao;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;

/**
 * Created by aditya.amartya on 28/09/18
 */

@Database(entities = {SearchHistory.class, SmsProductData.class, WishlistItem.class}, version = 1)
@TypeConverters({StatusConverter.class})
public abstract class ShoppingDatabase extends RoomDatabase {
    public abstract ShoppingDao getDao();

    public abstract SmsDao getSmsDao();

    public abstract WishlistDao getWishlistDao();

    private static volatile ShoppingDatabase instance;

    public static ShoppingDatabase getInstance(Application application) {
        ShoppingDatabase localInstance = instance;
        if (localInstance == null) {
            synchronized (ShoppingDatabase.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = Room.databaseBuilder(application, ShoppingDatabase.class, "shopping_database").build();
                }
            }
        }
        return localInstance;
    }
}
