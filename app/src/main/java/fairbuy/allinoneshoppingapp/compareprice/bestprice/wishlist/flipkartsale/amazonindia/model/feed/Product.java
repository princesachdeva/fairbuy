package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;

/**
 * Created by prince.sachdeva on 04/10/18.
 */

public class Product extends BaseModel implements Parcelable {
    public String productName;
    public String productImage;
    public String productPrice;
    public String productBrand;
    public String productFeatures;
    public String rating;
    public String ratedUsersCount;
    public String webSiteName;
    public String deducedCategory;
    public String deducedBrand;
    public String productURL;
    public String breadCrumb;
    public String productOrderId;
    public String productTrackingId;
    public boolean isWishlisted;
    public ShoppingUtils.WEBSITE website = ShoppingUtils.WEBSITE.TEMP;
    public boolean isNative;
    public String productOrigin;


    public Product() {

    }

    protected Product(Parcel in) {
        productName = in.readString();
        productImage = in.readString();
        productPrice = in.readString();
        productBrand = in.readString();
        productFeatures = in.readString();
        rating = in.readString();
        ratedUsersCount = in.readString();
        webSiteName = in.readString();
        deducedCategory = in.readString();
        deducedBrand = in.readString();
        productURL = in.readString();
        breadCrumb = in.readString();
        productOrderId = in.readString();
        productTrackingId = in.readString();
        isWishlisted = in.readByte() != 0;
        website = ShoppingUtils.WEBSITE.valueOf(in.readString());
        isNative = in.readByte() != 0;
        productOrigin = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productName);
        dest.writeString(productImage);
        dest.writeString(productPrice);
        dest.writeString(productBrand);
        dest.writeString(productFeatures);
        dest.writeString(rating);
        dest.writeString(ratedUsersCount);
        dest.writeString(webSiteName);
        dest.writeString(deducedCategory);
        dest.writeString(deducedBrand);
        dest.writeString(productURL);
        dest.writeString(breadCrumb);
        dest.writeString(productOrderId);
        dest.writeString(productTrackingId);
        dest.writeByte((byte) (isWishlisted ? 1 : 0));
        dest.writeString(website.name());
        dest.writeByte((byte) (isNative ? 1 : 0));
        dest.writeString(productOrigin);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public String toString() {
        return productName + "  " + breadCrumb + "   " + productPrice + "   " + productBrand + "   " + productImage + "  " + productURL;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true; //if both pointing towards same object on heap

        if (this.productName.equalsIgnoreCase(((Product) obj).productName)) {
            return true;
        } else {
            return false;
        }
    }

    public String getProductName() {
        if (!TextUtils.isEmpty(productName)) {
            productName = productName.replace(".", "").replace(",", "").trim();
        }
        return productName;
    }

  /*  public void appendOrderStatus(String status) {
        if (!TextUtils.isEmpty(productOrderStatus)) {
            productOrderStatus.append(" ");
        }
        productOrderStatus.append(status);
    }*/

    public int getAppIcon() {
        if (!TextUtils.isEmpty(productURL)) {
            if (productURL.contains("flipkart")) {
                return R.drawable.flipkart;
            } else if (productURL.contains("amazon")) {
                return R.drawable.amazon;
            } else if (productURL.contains("snapdeal")) {
                return R.drawable.snapdeal;
            } else if (productURL.contains("myntra")) {
                return R.drawable.myntra;
            } else if (productURL.contains("paytm")) {
                return R.drawable.paytm;
            }
        }
        return R.drawable.flipkart;
    }

    public int getAppIconByBrand() {
        if (website != null) {
            switch (website) {
                case PAYTM:
                    return R.drawable.paytm;
                case MYNTRA:
                    return R.drawable.myntra;
                case FLIPKART:
                    return R.drawable.flipkart;
                case SNAPDEAL:
                    return R.drawable.snapdeal;
                case AMAZON:
                    return R.drawable.amazon;
                case TEMP:
                    return 0;
                default:
                    return R.drawable.flipkart;
            }
        } else {
            return getAppIcon();
        }
    }

    public String getProductOrigin() {
        return productOrigin;
    }

    public void setProductOrigin(String productOrigin) {
        this.productOrigin = productOrigin;
    }
}
