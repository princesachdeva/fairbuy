package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by aditya.amartya on 22/11/18
 */
public class ShoppingDragWindow {
    private WindowManager windowManager;
    private WindowManager.LayoutParams layoutParams;

    private ShoppingDragLayout dragLayout;

    public ShoppingDragWindow(Context context, int layoutId, int... gravity) {
        layoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                Build.VERSION.SDK_INT < Build.VERSION_CODES.O ? WindowManager.LayoutParams.TYPE_SYSTEM_ALERT : WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        layoutParams.gravity = gravity.length >= 2 ? (gravity[0] | gravity[1]) : gravity[0];

        windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        dragLayout = new ShoppingDragLayout(context, this);
        inflater.inflate(layoutId, dragLayout, true);
    }

    public View getView() {
        return dragLayout;
    }

    public void show() {
        windowManager.addView(dragLayout, layoutParams);
    }

    public void hide() {
        if (dragLayout.getParent() != null)
            windowManager.removeView(dragLayout);
    }
}
