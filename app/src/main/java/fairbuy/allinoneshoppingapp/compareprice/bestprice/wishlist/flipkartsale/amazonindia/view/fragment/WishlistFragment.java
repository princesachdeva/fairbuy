package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.WishlistAdapter.VIEW_TYPE_EMPTY_LIST;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.WISHLIST_HOME;
import static fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants.Label.WISHLIST_NAV;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.BR;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.WishlistAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.dao.WishlistItem;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ActivityFairBuyBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentWishlistBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.AnalyticsConstants;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.model.feed.Product;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.parser.ChromeParser;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo.DbCallBack;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.util.Utils;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.FairBuyActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.listeners.OnViewAllClickListener;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.viewmodel.WishlistViewModel;


public class WishlistFragment extends BaseFragment {
    private WishlistAdapter wishlistAdapter;
    private int size;
    private ArrayList<Product> mProductsArrayList;
    private WishlistViewModel wishlistViewModel;
    private boolean isSnackBarVisible = false;
    private boolean isHome = true;
    private OnViewAllClickListener onViewAllClickListener;

    @Override
    protected int getResourceId() {
        return R.layout.fragment_wishlist;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FairBuyActivity) {
            onViewAllClickListener = ((FairBuyActivity) context);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(ARGS.IS_HOME)) {
            isHome = bundle.getBoolean(ARGS.IS_HOME);
        }

        if (isHome) {
            binding.setVariable(BR.showViewAll, true);
            binding.setVariable(BR.clickListener, mOnClickListener);
        }
        init();
        if (!isHome) {
            FirebaseAnalyticsManager.getInstance().setScreen(getActivity(), "Wishlist");
        }

    }

    private void init() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());

        RecyclerView recyclerView = ((FragmentWishlistBinding) binding).recylerViewWishList;

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        wishlistAdapter = new WishlistAdapter(mOnClickListener, isHome);
        recyclerView.setAdapter(wishlistAdapter);

        wishlistViewModel = ViewModelProviders.of(this).get(WishlistViewModel.class);
        if (Utils.isNetworkAvailable(getActivity())) {
            getLiveData();
        } else {
            if (isHome) {
                hideViewIfEmpty();
            } else {
                wishlistAdapter.setViewType(WishlistAdapter.VIEW_TYPE_NO_INTERNET);
                wishlistAdapter.notifyDataSetChanged();
            }
        }


        handleEmptyViewCase();
    }

    private void showHideHeaderView(boolean visibility) {
        if (isHome) {
            binding.setVariable(BR.showViewAll, visibility);
            binding.executePendingBindings();
        }
    }

    private void getLiveData() {
        final LiveData<List<WishlistItem>> liveData = isHome ? wishlistViewModel.getWishListLiveDataHome() : wishlistViewModel.getLiveData();
        liveData.observe(this, new Observer<List<WishlistItem>>() {
            @Override
            public void onChanged(@Nullable List<WishlistItem> wishlistItems) {
                liveData.removeObserver(this);
                if (wishlistItems != null && wishlistItems.size() > 0) {
                    prepareProductData((ArrayList<WishlistItem>) wishlistItems);
                }
            }
        });
    }

    private void prepareProductData(ArrayList<WishlistItem> wishlistItems) {
        if (wishlistItems == null || wishlistItems.size() == 0) {
            return;
        }

        size = wishlistItems.size();
        mProductsArrayList = new ArrayList<>(size);

        // Lazy Loading Handling in Java 8
        for (int i = 0; i < size; i++) {
            mProductsArrayList.add(new Product());
        }

        for (int i = size - 1; i >= 0; i--) {
            WishlistItem wishlistItem = wishlistItems.get(i);
            final int finalI = i;


            ChromeParser.parseURL(wishlistItem.getProductUrl(), new ChromeParser.OnProductPageListener() {
                @Override
                public void onProductPage(Product product) {
                    size--;
                    mProductsArrayList.remove(finalI);
                    if (product != null) {
                        mProductsArrayList.add(finalI, product);
                    } else {
                        mProductsArrayList.add(finalI, null);
                    }

                    if (size == 0) {
                        mProductsArrayList.removeAll(Collections.singleton(null));
                        if (mProductsArrayList.size() > 0) {
                            wishlistAdapter.updateData(mProductsArrayList);
                        } else {
                            wishlistAdapter.setViewType(VIEW_TYPE_EMPTY_LIST);
                            wishlistAdapter.notifyDataSetChanged();
                            hideViewIfEmpty();
                        }
                    }
                }
            });
        }
    }

    ChromeParser.OnProductPageListener mOnProductPageListener = new ChromeParser.OnProductPageListener() {
        @Override
        public void onProductPage(Product product) {
            size--;
            if (product != null) {
                mProductsArrayList.add(product);
            }

            if (size == 0) {
                wishlistAdapter.updateData(mProductsArrayList);

            }
        }
    };

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.buttonViewAll:
                    if (onViewAllClickListener != null) {
                        onViewAllClickListener.onViewAll(OnViewAllClickListener.VIEW_ALL_WISHLIST);
                    }
                    break;
                case R.id.cardItem:
                    if (view.getTag() != null && view.getTag() instanceof Product) {
                        showProgressBarForDelay();
                        Product product = (Product) view.getTag();
                        if (isHome) {
                            product.productOrigin = WISHLIST_HOME;
                        } else {
                            product.productOrigin = WISHLIST_NAV;
                        }
                        ShoppingUtils.launchProductPageInAppWishlist(view.getContext(), product);
                    }
                    break;
                case R.id.ivWishlist:
                    if (view.getTag() != null && view.getTag() instanceof Product) {
                        Product product = (Product) view.getTag();
                        if (!isSnackBarVisible) {
                            wishlistAdapter.removeItem(product);
                            showSnackBar(product);
                        }
                    }
                    break;
                case R.id.llTryAgain:
                    handleTryAgain();
                    break;
            }

        }
    };

    private void handleTryAgain() {
        getLiveData();
    }


    public void showSnackBar(final Product product) {
        Snackbar snackbar = Snackbar
                .make(getActivity().findViewById(R.id.coordinator), "Wishlist Item is deleted", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        wishlistAdapter.addDeletedItem(product);

                    }
                });

        snackbar.addCallback(new Snackbar.Callback() {

            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                if (getActivity() instanceof FairBuyActivity) {
                    ((ActivityFairBuyBinding) ((FairBuyActivity) getActivity()).binding).bottomNavigationView.setVisibility(View.VISIBLE);
                }
                isSnackBarVisible = false;
                if (event != DISMISS_EVENT_ACTION) {
                    wishlistViewModel.deleteWishListData(product.productURL, new DbCallBack() {
                        @Override
                        public void onQueryProcessed(boolean isSuccess) {
                            Log.e("TAG", isSuccess + "");
                        }
                    });
                }
            }

            @Override
            public void onShown(Snackbar sb) {
                super.onShown(sb);
                if (getActivity() instanceof FairBuyActivity) {
                    ((ActivityFairBuyBinding) ((FairBuyActivity) getActivity()).binding).bottomNavigationView.setVisibility(View.GONE);
                }
                isSnackBarVisible = true;
            }
        });

        snackbar.show();
    }

    private void handleEmptyViewCase() {
        wishlistViewModel.getCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null && integer == 0) {
                    wishlistAdapter.setViewType(VIEW_TYPE_EMPTY_LIST);
                    wishlistAdapter.notifyDataSetChanged();
                    hideViewIfEmpty();
                }
                wishlistViewModel.getCount().removeObserver(this);
            }
        });
    }

    private void hideViewIfEmpty() {
        if (isHome) {
            ((FragmentWishlistBinding) binding).rlContainer.setVisibility(View.GONE);
        }
    }

    public interface ARGS {
        String IS_HOME = "is_home";
    }
}
