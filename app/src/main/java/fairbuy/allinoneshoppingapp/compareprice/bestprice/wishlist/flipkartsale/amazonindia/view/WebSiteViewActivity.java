package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.MenuItem;

import com.google.firebase.FirebaseApp;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.ShoppingAssistantApplication;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.ActivityWebsiteViewBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.helper.ShoppingUtils.WEBSITE;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.manager.FirebaseAnalyticsManager;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseActivity;

/**
 * Created by prince.sachdeva on 04/09/18.
 */

public class WebSiteViewActivity extends BaseActivity implements WebSearchFragment.OnBackPressTaskCompltedListener {

    public static final String BUNDLE_COMPANY = "BUNDLE_COMPANY";
    public static final String IS_DIRECT_DETAIL_PAGE = "IS_DIRECT_DETAIL_PAGE";
    public static final String IS_DEMO = "IS_DEMO";
    public static final String URL = "URL";
    private WEBSITE site_type;
    private ActivityWebsiteViewBinding dataBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_website_view);
        if (savedInstanceState == null) {
            initView();
        }
        initActionBar();
    }

    private void initView() {
        Fragment fragment = new WebSearchFragment();
        Bundle bundle = new Bundle();
        site_type = (WEBSITE) getIntent().getSerializableExtra(BUNDLE_COMPANY);
        if (site_type == null) {
            String url = getIntent().getStringExtra(URL);
            site_type = WEBSITE.TEMP;
            site_type.setUrl(url);
        } else {
            if ("compare".equalsIgnoreCase(site_type.getSource())) {
                ShoppingAssistantApplication.showAskPermissionDialog = true;
            }
        }
        bundle.putSerializable(WebSearchFragment.BUNDLE_COMPANY, site_type);
        bundle.putBoolean(WebSearchFragment.IS_DIRECT_DETAIL_PAGE, getIntent().getBooleanExtra(IS_DIRECT_DETAIL_PAGE, false));
        bundle.putBoolean(WebSearchFragment.IS_DEMO, getIntent().getBooleanExtra(IS_DEMO, false));
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment, WebSearchFragment.class.getName()).commit();
        FirebaseAnalyticsManager.getInstance().setScreen(this, "Webview_store");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void initActionBar() {
        setSupportActionBar(dataBinding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && site_type != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(site_type.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fragment instanceof WebSearchFragment) {
            ((WebSearchFragment) fragment).onBackPressed();
        }
    }

    @Override
    public void onBackPressTaskCompleted() {
        super.onBackPressed();
    }

}
