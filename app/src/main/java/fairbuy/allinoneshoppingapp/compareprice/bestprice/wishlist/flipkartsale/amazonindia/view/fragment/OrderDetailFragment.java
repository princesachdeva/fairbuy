package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.adapter.OrderDetailAdapter;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.databinding.FragmentOrderDetailBinding;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.SmsProductData;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.sms.parser.dao.Status;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.activity.OrderDetailActivity;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView.BaseFragment;

/**
 * Created by prince.sachdeva on 09/12/18.
 */

public class OrderDetailFragment extends BaseFragment implements View.OnClickListener {

    public static OrderDetailFragment getInstance(SmsProductData smsProductData) {
        OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(OrderDetailActivity.ARGS.SMS_PRODUCT_DATA, smsProductData);
        orderDetailFragment.setArguments(bundle);
        return orderDetailFragment;
    }


    @Override
    protected int getResourceId() {
        return R.layout.fragment_order_detail;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        if (getArguments() == null || getArguments().getSerializable(OrderDetailActivity.ARGS.SMS_PRODUCT_DATA) == null) {
            getActivity().finish();
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());

        RecyclerView recylerViewMyOrders = ((FragmentOrderDetailBinding) binding).recylerViewMyOrders;
        recylerViewMyOrders.setLayoutManager(mLayoutManager);
        recylerViewMyOrders.setItemAnimator(new DefaultItemAnimator());

        SmsProductData smsProductData = (SmsProductData) getArguments().getSerializable(OrderDetailActivity.ARGS.SMS_PRODUCT_DATA);
        OrderDetailAdapter orderDetailAdapter = new OrderDetailAdapter(this);
        orderDetailAdapter.updateData(smsProductData);

        recylerViewMyOrders.setAdapter(orderDetailAdapter);
    }

    public void showDeatiledMessage(String message) {

        if (TextUtils.isEmpty(message)) {
            return;
        }

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity());
        View sheetView = getActivity().getLayoutInflater().inflate(R.layout.order_detail_message, null);
        bottomSheetDialog.setContentView(sheetView);

        TextView textView = (TextView) sheetView.findViewById(R.id.tvMessage);
        textView.setText(message);

        sheetView.findViewById(R.id.ivCross).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });
        bottomSheetDialog.show();
    }

    @Override
    public void onClick(View view) {
        Object object = view.getTag();
        if (object instanceof Status) {
            showDeatiledMessage(((Status) object).getMessage());

           /* Intent intent = new Intent(getContext(), AppUpdateActivity.class);
            startActivity(intent);
            getActivity().finishAffinity();*/
        }
    }
}
