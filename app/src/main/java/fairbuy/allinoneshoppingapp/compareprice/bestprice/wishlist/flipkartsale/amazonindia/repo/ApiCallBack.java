package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.repo;

/**
 * Created by aditya.amartya on 24/10/18
 */
public interface ApiCallBack<T> {
    void onSuccess(T data);

    void onFail(Throwable throwable);
}
