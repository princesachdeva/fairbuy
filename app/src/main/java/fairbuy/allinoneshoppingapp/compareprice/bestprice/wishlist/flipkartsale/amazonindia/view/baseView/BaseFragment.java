package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.baseView;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by aditya.amartya on 05/12/18
 */
public abstract class BaseFragment extends Fragment {
    protected ViewDataBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getResourceId(), container, false);
        return binding.getRoot();
    }

    protected abstract int getResourceId();

    /*
    Customize back press handling for fragments. Default:true. If true popup backstack
     */
    public boolean onBackPressed() {
        return true;
    }

    public void setActionBar(String title) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(title);
    }

    public void showProgressBarForDelay() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showProgressBarForDelay();
        }
    }
}
