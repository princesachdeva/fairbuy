package fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.widget;

import android.app.Application;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.R;
import fairbuy.allinoneshoppingapp.compareprice.bestprice.wishlist.flipkartsale.amazonindia.view.customView.OverlayOptionsView;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by aditya.amartya on 26/12/18
 */
public class FabUtil {
    private Application context;
    private OnFabClickListener onFabClickListener;
    private ViewGroup layoutSrpActions;
    private int fabItemParentId;
    private View overlayOptionsView;

    /**
     * @param resource           - layout resource, which we have to inflate
     * @param onFabClickListener - listener to provide Callback for click actions on inflated layout
     * @param fabItemParentId    - resource id of parent of current child items
     * @param fabCloseId         - resource id of close button
     */
    public FabUtil(Application context, int resource, OnFabClickListener onFabClickListener, int fabItemParentId, int fabCloseId) {
        this.context = context;
        this.onFabClickListener = onFabClickListener;
        this.fabItemParentId = fabItemParentId;
        inflateView(resource, fabItemParentId, fabCloseId);
    }

    public void changeWishlistIcon(boolean isWishlisted, int wishlistId) {
        if (overlayOptionsView != null) {
            ImageView fabWishlist = (ImageView) overlayOptionsView.findViewById(wishlistId);
            fabWishlist.setImageResource(isWishlisted ? R.drawable.pop_up_wishlisted : R.drawable.pop_up_wishlist);
        }
    }

    private void inflateView(int resource, int fabItemParentId, int fabCloseId) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        animation.setInterpolator(new OvershootInterpolator());

        WindowManager.LayoutParams layoutParams;

        layoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                Build.VERSION.SDK_INT < Build.VERSION_CODES.O ? WindowManager.LayoutParams.TYPE_SYSTEM_ALERT : WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);

        //layoutParams.gravity = Gravity.END | Gravity.CENTER_VERTICAL;
        //layoutParams.alpha = 0.5f;

        WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);

        LayoutInflater inflater = LayoutInflater.from(context);
        //overlayOptionsView = inflater.inflate(resource, null);
        OverlayOptionsView view = new OverlayOptionsView(context, resource);
        this.overlayOptionsView = view.getView();


        layoutSrpActions = (ViewGroup) this.overlayOptionsView.findViewById(fabItemParentId);
        ImageView fabClose = (ImageView) this.overlayOptionsView.findViewById(fabCloseId);

        FabItemClickListener fabItemClickListener = new FabItemClickListener();

        //fabClose.setOnClickListener(fabItemClickListener);

        //layoutSrpActions.setId(fabItemParentId);
        layoutSrpActions.setOnClickListener(fabItemClickListener);
        layoutSrpActions.setAnimation(animation);

        /*for (int i = 0; i < layoutSrpActions.getChildCount(); i++) {
            View tempView = layoutSrpActions.getChildAt(i);
            if (tempView instanceof ImageView || tempView instanceof LinearLayout) {
                tempView.setOnClickListener(fabItemClickListener);
            }
        }*/

        for (int i = 0; i < layoutSrpActions.getChildCount(); i++) {
            View tempView = layoutSrpActions.getChildAt(i);
            if (tempView instanceof LinearLayout) {
                LinearLayout imageView = (LinearLayout) tempView;
                imageView.setOnClickListener(fabItemClickListener);
            }
        }
        view.showView(layoutParams);
        //windowManager.addView(overlayOptionsView, layoutParams);
    }

    public interface OnFabClickListener {
        void onFabClick(View view);
    }

    private class FabItemClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id != fabItemParentId) {
                onFabClickListener.onFabClick(v);
            }
        }
    }

    public void removeFabItems() {
        Animation upDownAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_down);
        layoutSrpActions.setAnimation(upDownAnimation);
        //((ViewGroup) parent).removeView(layoutSrpActions);
        if (overlayOptionsView != null && overlayOptionsView.getParent() != null) {
            WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
            windowManager.removeView(overlayOptionsView);
            overlayOptionsView = null;
        }
    }
}
